<!--*******************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*-->
<!--**
* @author ET Marlabs
* @date 30-Nov-2015
* @description This page is Sales Order main page
*-->
<apex:page showHeader="false" sidebar="false" docType="html-5.0" standardStylesheets="false" applyHtmlTag="true" controller="SalesOrderController">
<html ng-app="App.SalesOrderMobile" ng-cloak="true">
    <head>

        <title>{!HTMLENCODE($Label.Sales_Order)}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta charset="UTF-8"/>
        <apex:stylesheet value="{!URLFOR($Resource.ExternalLib,'AngularMaterial1.0.6/css/angular-material.min.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.SalesOrderMobile, '/css/salesOrderMobile.css')}" />

	</head> 
<body ng-controller="SalesOrderMobileController" >
    
    <div align="center">
		<span class="md-headline">{!HTMLENCODE($Label.Sales_Order)}</span>
		<md-divider class="headlineDivider"></md-divider>

	</div>
	<form name="salesOrderForm">
	<md-content layout-padding="" class="scroll">
	

    	<md-datepicker ng-model="salesOrder.date" ng-disabled="isopen" ng-required="true"></md-datepicker>

		<md-autocomplete    
		  md-input-name="buyerField"
		  md-selected-item="salesOrder.account"
		  md-search-text="searchAccount"
		  md-selected-item-change="fetchShippingAddress(item)"
		  md-items="item in searchAccounts(searchAccount)" 
		  md-item-text="item.Name"
		  md-min-length="0"
		  placeholder="{!HTMLENCODE($ObjectType.SandDV1__Sales_Order__c.Fields.SandDV1__Buyer__c.Label)}"
		  ng-disabled="isDisabled" 
		  md-floating-label="{!HTMLENCODE($ObjectType.SandDV1__Sales_Order__c.Fields.SandDV1__Buyer__c.Label)}"
		  ng-required="true">

			<md-item-template>
				<span md-highlight-text="searchAccount" md-highlight-flags="^i">
			  		{{item.Name}}
			  	</span>
			</md-item-template>
			<md-not-found>
				{!HTMLENCODE($Label.No_buyer_matching)} "{{searchAccount}}" {!HTMLENCODE($Label.were)} {!HTMLENCODE($Label.found)}.
			</md-not-found>
			<div ng-messages="salesOrderForm.buyerField.$error" ng-if="salesOrderForm.buyerField.$touched">
	            <div ng-message="required">Buyer is required.</div>
	     
	        </div>
		</md-autocomplete>

	    <md-input-container>
	        <label >{!HTMLENCODE($Label.Ship_To)}</label>
	        <md-select ng-model="salesOrder.shipTo" ng-required="true" ng-disabled="isopen" name="shipTo">				
	        <md-option ng-repeat="ship in shippingList" 
	        	ng-value="ship" 
	        	ng-selected="{{ ship.Name === salesOrder.shipTo.Name}}">
					{{ship.Name}}
				</md-option>
	        </md-select>
	        <div ng-messages="salesOrderForm.salesOrder.shipTo.$error" ng-if="salesOrderForm.salesOrder.shipTo.$touched">
	          <div ng-message="required">{!HTMLENCODE($Label.Required_Message)}</div>
	        </div>
      	</md-input-container> 

		<md-autocomplete      
		  md-input-name="pricebookField"
		  md-selected-item="salesOrder.selectedPricebook"
		  md-search-text="searchPricebook"
		  md-selected-item-change="selectedPricebookChange(item)"
		  md-items="item in searchPricebooks(searchPricebook)" 
		  md-item-text="item.Name"
		  md-min-length="0"
		  ng-disabled="isopen"
		  md-floating-label="{!HTMLENCODE($ObjectType.SandDV1__Sales_Order__c.Fields.SandDV1__Price_Book__c.Label)}"
		  ng-required="true">

			<md-item-template>
			  <span md-highlight-text="searchPricebook" md-highlight-flags="^i">{{item.Name}}</span>
			</md-item-template>
			<md-not-found>
			  {!HTMLENCODE($Label.No_pricebook_matching)} "{{searchPricebook}}" {!HTMLENCODE($Label.were)} {!HTMLENCODE($Label.found)}.
			</md-not-found>
			<div ng-messages="salesOrderForm.pricebookField.$error" ng-if="salesOrderForm.pricebookField.$touched">
	            <div ng-message="required">PriceBook is required.</div>
	     
	        </div>
		</md-autocomplete>

	    <md-input-container >
			<label >{!HTMLENCODE($ObjectType.SandDV1__Sales_Order__c.Fields.SandDV1__PO_Reference__c.Label)}</label>
			<input ng-model="salesOrder.poNumber" type="text" ng-disabled="isopen" name="poNumber"/>

	    </md-input-container>

	    <br/>
	    <div ng-if="checkPriceBook()">
		    <span class="md-title">{!HTMLENCODE($Label.Products)}</span>   
		    <md-divider></md-divider>
		    <br/>
		    <md-list>
		    
		    <md-list-item>
			    <div flex="50">
			    	<span class="md-subhead">
			    		{!HTMLENCODE($ObjectType.Product2.Fields.Name.Label)}
			    	</span>
			    </div>
		        <div flex="20" ng-if="!isopen">
		        	<span class="md-subhead">
		        		{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Quantity__c.Label)}
		        	</span>
		        </div>
		        <div flex="15" ng-if="isopen">
			        <span class="md-subhead">
			        	{!HTMLENCODE($Label.Old_Quantity)}
			        </span>
		        </div>
		        <div flex="15" ng-if="isopen">
			        <span class="md-subhead">
			        	{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__New_Quantity__c.Label)}
			        </span>
		        </div>
		        <div flex="20" ng-if="isopen">
			        <span class="md-subhead">
			        	{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Net_Price__c.Label)}
			        </span>
		        </div>
		        <div flex="30" ng-if="!isopen">
			        <span class="md-subhead">
			        	{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Net_Price__c.Label)}
			        </span>
		        </div>
	        </md-list-item>

	        <md-list-item ng-repeat="soli in salesOrderLIList"
	        md-swipe-right="onSwipeRight(soli,$index)" 
	        ng-click="editSoli($index)" 
	        md-ink-ripple="#F7F6F6">
	        	
		        <div flex="50"><span >{{soli.SandDV1__Part__r.Name}}</span> </div>
		        <div flex="20" ng-if="!isopen"><span >{{soli.SandDV1__Quantity__c}}</span> </div>
		        <div flex="15" ng-if="isopen"><span >{{soli.SandDV1__Quantity__c}}</span> </div>
		        <div flex="15" ng-if="isopen"><span >{{soli.SandDV1__New_Quantity__c}}</span> </div>
		        <div flex="20" ng-if="isopen"><span >{{soli.SandDV1__Net_Price__c}}</span> </div>
		        <div flex="30" ng-if="!isopen"><span >{{soli.SandDV1__Net_Price__c}}</span> </div>
	        	<md-divider></md-divider>
			</md-list-item>
			
			
			<md-divider ng-if="salesOrderLIList.length > 0"></md-divider>
	    	<md-list-item >

			    <div flex="80">
			    	
			    	<table >
			    		<tr>
			    			<td><span class="md-subhead">{!HTMLENCODE($Label.Total_Discount)}</span></td>
			    			<td><span class="md-subhead">{{discountAmount}}</span></td>
			    		</tr>
			    		<tr>
			    			<td><span class="md-subhead">{!HTMLENCODE($Label.Total_Order_Amount)}</span></td>
			    			<td><span class="md-subhead">{{grandTotal}} </span></td>
			    		</tr>
			    	</table>
			    </div>
		        
		        <div flex="20">

	    			<md-button class="md-fab" aria-label="Eat cake" ng-click="addSOLI()" ng-if="!isopen">
			            <md-icon md-svg-src="{!URLFOR($Resource.ExternalLib,'AngularMaterial1.0.6/icons/ic_add_black_24px.svg')}">
			            </md-icon>
			            
			        </md-button>

	    		</div>

	        </md-list-item>

	    	</md-list>
		</div>
	</md-content>
  	<div class="pageFooter" >

  		<div class="bottom-sheet-demo inset" layout="row" layout-align="center" >
	        
	        <md-button flex="30" class="md-primary md-raised" ng-click="salesOrderForm.$valid && saveAsDraft()" ng-if="!isopen" type="submit">
	        	{!HTMLENCODE($Label.Draft)}
	        </md-button>
	        <md-button flex="30" class="md-primary md-raised" ng-click="salesOrderForm.$valid && submit()" type="submit">
	        	{!HTMLENCODE($Label.Save)}
	        </md-button>
	        <md-button flex="30" class="md-primary md-raised" ng-click="cancel()">
	        	{!HTMLENCODE($Label.Cancel)}
	        </md-button>
		</div>
	</div>
	</form>

	<script type="text/ng-template" id="productDialogNewTemplate.html">
 
        <md-dialog aria-label="List dialog" class="md-padding productDialog">
           <div align="center"><span class="md-title">{!HTMLENCODE($Label.Add_Product)}</span></div>
           <md-divider></md-divider>
           <md-dialog-content>
           		<form name="productNewForm">
            	<md-list>
            		<div class="errorMsg" ng-if="errorMsg != ''">
            			<span class="md-caption">{{errorMsg}}</span>
            		</div>
        			<md-autocomplete
        				ng-required="true"
              		  md-input-name="productCodeField"
		              md-selected-item="dialogSoli.selectedProduct"
		              md-search-text="dialogSoli.searchProductCode"
		              md-selected-item-change="selectedProductChange(item)"
		              md-items="item in searchProductsByCode(dialogSoli.searchProductCode)" 
		              md-item-text="item.ProductCode"
		              md-min-length="0"
		              md-floating-label="{!HTMLENCODE($ObjectType.Product2.Fields.ProductCode.Label)}"
		              class="soliAutoComplete"
		              ng-disabled="isopen"
		              >
			            <md-item-template>
			              <span md-highlight-text="dialogSoli.searchProductCode" md-highlight-flags="^i">{{item.ProductCode}}</span>
			            </md-item-template>
			            <md-not-found>
			              {!HTMLENCODE($Label.No_product_matching)} "{{dialogSoli.searchProductCode}}" {!HTMLENCODE($Label.were)} {!HTMLENCODE($Label.found)}.
			            </md-not-found>
			            <div ng-messages="productNewForm.productCodeField.$error" ng-if="productNewForm.productCodeField.$touched">
				            <div ng-message="required">Product code is required.</div>
				     
				        </div>
			        </md-autocomplete>

			        <md-autocomplete
              		  ng-required="true"
              		  md-input-name="productNameField"
		              md-selected-item="dialogSoli.selectedProduct"
		              md-search-text="dialogSoli.searchProduct"
		              md-selected-item-change="selectedProductChange(item)"
		              md-items="item in searchProducts(dialogSoli.searchProduct)" 
		              md-item-text="item.Name"
		              md-min-length="0"
		              md-floating-label="{!HTMLENCODE($ObjectType.Product2.Fields.Name.Label)}"
		              class="soliAutoComplete"
		              ng-disabled="isopen"
		              >
			            <md-item-template>
			              <span md-highlight-text="dialogSoli.searchProduct" md-highlight-flags="^i">{{item.Name}}</span>
			            </md-item-template>
			            <md-not-found>
			              {!HTMLENCODE($Label.No_product_matching)} "{{dialogSoli.searchProduct}}" {!HTMLENCODE($Label.were)} {!HTMLENCODE($Label.found)}.
			            </md-not-found>
			            <div ng-messages="productNewForm.productCodeField.$error" ng-if="productNewForm.productNameField.$touched">
				            <div ng-message="required">Product Name is required.</div>
				     
				        </div>
			        </md-autocomplete>

 					<md-input-container >
				      <label>{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Unit_Price__c.Label)}</label>
				      <input ng-model="dialogSoli.SandDV1__Unit_Price__c" type="number" disabled="true"/>
				    </md-input-container>

				    
        			<md-input-container >
						<label>{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Quantity__c.Label)}</label>
						<input ng-model="dialogSoli.SandDV1__Quantity__c" type="number" ng-required="true" name="quantity" min="1"/>
						<div ng-messages="productNewForm.quantity.$error">
						  <div ng-message="required">Quantity is required.</div>
						  <div ng-message="min">
				            Minimum quantity is 1.
				          </div>
						</div>
				    </md-input-container>

				    
				    <md-input-container >
				      <label>{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Discount__c.Label)}</label>
				      <input ng-model="dialogSoli.SandDV1__Discount__c" type="number" />
				    </md-input-container>
            		

            		<table class="soliTable">
						<tr class="textAlignCenter">
							<td flex="50">
								{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Sales_Price__c.Label)}
							</td>
							<td flex="50" >
								{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Net_Price__c.Label)}
							</td>
						</tr>
						<tr class="textAlignCenter">
							<td flex="50">
								{{dialogSoli.SandDV1__Sales_Price__c}}
							</td>
							<td flex="50">
								{{dialogSoli.SandDV1__Net_Price__c}}
							</td>
						</tr>
					</table>
            		<div class="bottom-sheet-demo inset" layout="row" layout-align="center" >
            			<md-button flex="50" class="md-primary md-raised" ng-click="productNewForm.$valid && addProduct()" type="submit">
            				{!HTMLENCODE($Label.Add)}
            			</md-button>
				        <md-button flex="50" class="md-primary md-raised" ng-click="closeDialog()">
				        	{!HTMLENCODE($Label.Cancel)}
				        </md-button>
				      
					</div>

            	</md-list>
            	</form>
           </md-dialog-content>
         </md-dialog>
    </script>


    <script type="text/ng-template" id="productDialogEditTemplate.html">
 
        <md-dialog aria-label="List dialog" class="md-padding productDialog">
           <div align="center"><span class="md-title">{!HTMLENCODE($Label.Edit_Product)}</span></div>
           <md-divider></md-divider>
           <md-dialog-content>
            	<md-list>
            		
            		<form name="productEditForm" >
 					<md-input-container >
				     	<label>
				     		{!HTMLENCODE($ObjectType.Product2.Fields.Name.Label)}
				     	</label>
				      	<input ng-model="dialogSoli.SandDV1__Part__r.Name" type="text" disabled="true"/>
				    </md-input-container>

            		<div class="errorMsg" ng-if="errorMsg != ''">
            			<span class="md-caption">{{errorMsg}}</span>
            		</div>
  		
 					<md-input-container >
					    <label>
					    	{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Unit_Price__c.Label)}
					    </label>
					    <input ng-model="dialogSoli.SandDV1__Unit_Price__c" type="number" disabled="true"/>
				    </md-input-container>
	    
        			<md-input-container >
				      	<label ng-if="!isopen">
				      		{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Quantity__c.Label)}
				      	</label>
				      	<label ng-if="isopen">
				      		{!HTMLENCODE($Label.Old_Quantity)}
				      	</label>
				     	<input ng-model="dialogSoli.SandDV1__Quantity__c" type="number" ng-required="true" ng-disabled="isopen"  />
				    </md-input-container>

				    <md-input-container ng-if="isopen">
						<label>
							{!HTMLENCODE($Label.Fulfilled_Quantity)}
						</label>
						<input ng-model="dialogSoli.SandDV1__Fulfilled_Quantity__c" type="number" minlength="1" ng-disabled="isopen" />
				    </md-input-container>

				    <md-input-container ng-if="isopen">
				      	<label>
				      		{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__New_Quantity__c.Label)}
				      	</label>
				      	<input ng-model="dialogSoli.SandDV1__New_Quantity__c" name="newQuantity"  type="number" required="true"  />
					  	<div ng-messages="productEditForm.newQuantity.$error" >
							<div ng-message="required" ng-if="dialogSoli.newQuantityReq">
						    	{!HTMLENCODE($Label.Required_Message)}
						  	</div>
						  	<div ng-message="greater" ng-if="dialogSoli.newQuantityError">
						    	{!HTMLENCODE($Label.New_quantity_greater_than_fulfilled_quantity)}
						  	</div>
					 
						</div>

				    </md-input-container>

				    <md-input-container >
				      	<label>
				      		{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Discount__c.Label)}
				      	</label>
				      	<input ng-model="dialogSoli.SandDV1__Discount__c" type="number" ng-required="true" ng-disabled="isopen" />
				    </md-input-container>
            		

            		<table class="soliTable">
						<tr class="textAlignCenter">
							<td flex="50">
								{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Sales_Price__c.Label)}
							</td>
							<td flex="50" >
								{!HTMLENCODE($ObjectType.SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Net_Price__c.Label)}
							</td>
						</tr>
						<tr class="textAlignCenter">
							<td flex="50">
								{{dialogSoli.SandDV1__Sales_Price__c}}
							</td>
							<td flex="50">
								{{dialogSoli.SandDV1__Net_Price__c}}
							</td>
						</tr>
					</table>
            		<div class="bottom-sheet-demo inset" layout="row" layout-align="center" >
				        
				        <md-button flex="50" class="md-primary md-raised" ng-click="productEditForm.$valid && editProduct()" type="submit">
				        	{!HTMLENCODE($Label.Update)}
				        </md-button>
				        <md-button flex="50" class="md-primary md-raised" ng-click="closeDialog()">
				        	{!HTMLENCODE($Label.Cancel)}
				        </md-button>
					</div>
					</form>
            	</md-list>
           </md-dialog-content>
         </md-dialog>
    </script>
	<!--Loading section  -->
	<c:Loading_Component />

	<script type="text/javascript">
		//This is bridging for passing Custom Labels to the JS and can not be moved to any 
        //static resource file.
        window.$Label = window.$Label || {};
        window.$Constant = window.$Constant || {};
        $Constant.accId = '{!JSENCODE($CurrentPage.parameters.accId)}';
        $Constant.newPage = '{!JSENCODE($CurrentPage.parameters.new)}';  
        $Constant.editPage = '{!JSENCODE($CurrentPage.parameters.edit)}'; 
        $Constant.salesOrderId = '{!JSENCODE($CurrentPage.parameters.sId)}';
    	$Constant.fetchAllAccounts = '{!JSENCODE($RemoteAction.SalesOrderController.fetchAllAccounts)}';
    	$Constant.fetchAccountsList = '{!JSENCODE($RemoteAction.SalesOrderController.fetchAccountsList)}';
    	$Constant.fetchShippingList = '{!JSENCODE($RemoteAction.SalesOrderController.fetchShippingList)}';
    	$Constant.fetchPriceBooks = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBooks)}';
    	$Constant.fetchPriceBookEntries = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBookEntries)}';
        $Constant.getLimitedPriceBookEntries = '{!JSENCODE($RemoteAction.SalesOrderController.getLimitedPriceBookEntries)}';
    	$Constant.saveSalesOrder = '{!JSENCODE($RemoteAction.SalesOrderController.saveSalesOrder)}';
    	$Constant.fetchSalesOrderRecords = '{!JSENCODE($RemoteAction.SalesOrderController.fetchSalesOrderRecords)}';
    	$Constant.fetchPriceBookEntriesSoli = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBookEntriesSoli)}';
    	$Constant.fetchAccount = '{!JSENCODE($RemoteAction.SalesOrderController.fetchAccount)}';
	</script>
    <!-- load angular and various other angular services via static resource -->
    <c:Angular_Js_Resource_Component />

    <!--load underscore js library via static resource-->
    <script src="{!URLFOR($Resource.ExternalLib, '/Underscore1.8.3/underscore-min.js')}" />
    <script type="text/javascript" src="/canvas/sdk/js/publisher.js"/>

    <script src="{!URLFOR($Resource.SalesOrderMobile, '/js/salesOrderMobile.js')}" />
    <script src="{!URLFOR($Resource.SalesOrderMobile, '/js/salesOrderMobileController.js')}" />
    <script src="{!URLFOR($Resource.SalesOrderMobile, '/js/salesOrderMobileFactory.js')}" />

</body>

</html>
</apex:page>