/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 28-July-2016
* @description This class for DeliveryOrderCancelPage
*/

public with sharing class DeliveryOrderCancelController {

    public String orderCancelled {get;set;}
    public String deliveryId {get;set;}
    
    public DeliveryOrderCancelController(ApexPages.StandardController stdController) {
     
     deliveryId = stdController.getId();
     
    }
    public void cancelDeliveryOrder(){
        
        List<SandDV1__Delivery_Order__c> deliveryOrderList = DeliveryOrderService.fetchDeliveryOrderRecord(deliveryId);
         if(!deliveryOrderList.isEmpty()){
             if(deliveryOrderList[0].SandDV1__Status__c == 'Open'){

                 DeliveryOrderService.changeDeliveryOrderStatus(deliveryOrderList[0],'Cancelled');
                 orderCancelled = 'true'; 
             }
             else {
                 
                 orderCancelled = 'false';
             }
         }
    }
}