/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 03-Nov-2015
* @description This class is for Customer360View Page.
*/
public with sharing class Customer360ViewController {

   
    /*******************************************************************************************************
    * @description : method to retrive Account related objects based on account Id
    * @param : accId - account Id
    * @return AccountWrrapper
    */
    
    @RemoteAction
    public static Customer360ViewDomain.CustomerWrapper fetchAllDetails(String accId,String visitId){

        Integer limitRecords = ConfigurationService.LimitRecords; 
        List<SandDV1__Complaint__c> complaintList;
        List<Task> openActivityList;
        List<SandDV1__Visit__c> visitList;

        Customer360ViewDomain.CustomerWrapper cusWrap = new Customer360ViewDomain.CustomerWrapper();
    	
        cusWrap.acc = fetchAccount(accId);
                       
        cusWrap.contactList = ServiceFacade.fetchContacts(accId,limitRecords);

        complaintList = ServiceFacade.fetchComplaints(accId);

        openActivityList = ServiceFacade.fetchTasks(accId);
                                   
        cusWrap.visitList = ServiceFacade.fetchVisits(accId,limitRecords);

        cusWrap.salesOrderList = ServiceFacade.fetchSalesOrders(accId,limitRecords);

        cusWrap.deliveryOrderList = ServiceFacade.fetchDeliveryOrders(accId,limitRecords);

        visitList = ServiceFacade.fetchVisitsById(visitId);

        if(!visitList.isEmpty()){

            cusWrap = Customer360ViewService.assignCheckInCheckOut(visitList,cusWrap);
        }


        if(!complaintList.isEmpty()){

            cusWrap = Customer360ViewService.assignComplaintValues(complaintList,cusWrap);
        }

        if(!openActivityList.isEmpty()){

            cusWrap = Customer360ViewService.assignActivitieValues(openActivityList,cusWrap);
        }
        
    	return cusWrap;
    }


    public static Account fetchAccount(String accId){

        List<Account> accList = ServiceFacade.fetchAccountById(accId);

        if(!accList.isEmpty()) return accList[0];

        else return null;
    }


    /*******************************************************************************************************
    * @description : method to update a visit object
    * @param : visit - visit object
    * @return Visit object
    */
    
    @RemoteAction
    public static SandDV1__Visit__c visitCheckInOut(SandDV1__Visit__c visit){

        return Customer360ViewService.updateVisit(visit);
    }
}