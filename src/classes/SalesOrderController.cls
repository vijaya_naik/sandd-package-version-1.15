/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 05-Oct-2015
* @description This class is for SalesOrderPage.
*/

public with sharing class SalesOrderController {

    public class salesOrderException extends Exception {}

    /*******************************************************************************************************
    * @description method to retrive accounts based on provided string
    * @param accName - account name to query
    * @return List of account . 
    */
    
    @RemoteAction
    public static List<Account> fetchAccountsList(String accName){

        return ServiceFacade.fetchAccounts(accName);
    }

    /*******************************************************************************************************
    * @description method to retrive accounts 
    * @param 
    * @return List of account . 
    */
    
    @RemoteAction
    public static List<Account> fetchAllAccounts(){

        return ServiceFacade.fetchAccounts();
    }

    /*******************************************************************************************************
    * @description : method to retrive shipping based on account Id and query string
    * @param : accId - account Id,shipName - ship name to query
    * @return List of shipping address . 
    */
    
    @RemoteAction
    public static List<SandDV1__Shipping_Address__c> fetchShippingList(String accId){


        return ServiceFacade.fetchShippingByAccId(accId);  

    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook id and Prouct name/code
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    @RemoteAction
    public static List<PricebookEntry> fetchPriceBookEntries(String priceBookId,String productName,String productCode){


        if(productName != null){

            return ServiceFacade.fetchPriceBookEntriesByProductName(priceBookId,productName);
        }
        else {

            return ServiceFacade.fetchPriceBookEntriesByProductCode(priceBookId,productCode);
        }
    

    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    @RemoteAction
    public static List<PricebookEntry> getPriceBookEntries(String priceBookId){


        return ServiceFacade.fetchPriceBookEntries(priceBookId);
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on List of product Ids
    * @param : productIdList - List of product Ids, pricebookId - Id of the pricebook
    * @return Map of PricebookEntries. 
    */
    
    @RemoteAction
    public static List<PricebookEntry> fetchPriceBookEntriesSoli(List<String> productIdList,String priceBookId){


        return ServiceFacade.fetchPriceBookEntriesByProductId(productIdList,priceBookId);

    }


     /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook name
    * @param : priceBookName - priceBookName name to query
    * @return List of Pricebook2. 
    */
    
    @RemoteAction
    public static List<Pricebook2> fetchPriceBooks(String priceBookName){

        return ServiceFacade.fetchPriceBooks(priceBookName);
    }


    /*******************************************************************************************************
    * @description : Method to save Sales order and sales order line item
    * @param :  salesOrder- Sales order that should be saved. salesOrderLI - Sales order line item to insert, 
                deleteSalesOrderLI - salesOrderLI - Sales order line item to delete
    * @return String 
    */
    
    @RemoteAction
    public static String saveSalesOrder(SandDV1__Sales_Order__c salesOrder,List<SandDV1__Sales_Order_Line_Item__c> salesOrderLI,
                                            List<SandDV1__Sales_Order_Line_Item__c> deleteSalesOrderLI,Boolean isDraft){
     
       return  SalesOrderService.saveSalesOrder(salesOrder,salesOrderLI,deleteSalesOrderLI,isDraft);
    }
    
    /*******************************************************************************************************
    * @description : Method to fetch Sales order and sales order line items
    * @param : salesOrderId- Id of Sales order
    * @return SalesOrderWrapper
    */  
    @RemoteAction
    public static SalesOrderDomain.SalesOrderWrapper fetchSalesOrderRecords(String salesOrderId){

        return SalesOrderService.fetchSalesOrder(salesOrderId);
    }
    
    /*******************************************************************************************************
    * @description method to retrive account based on Id
    * @param accId - account Id to query
    * @return account object. 
    */
    
    @RemoteAction
    public static Account fetchAccount(String accId){

        List<Account> acc = ServiceFacade.fetchAccountById(accId);

        if(!acc.isEmpty()){

            return acc[0];
        }
        else {

            return null;
        }
    }

    /*******************************************************************************************************
    * @description method to delete salesorder line item based on Id
    * @param soLineItemId - sales order line item Id to query
    * @return account object. 
    */
    
    @RemoteAction
    public static void deleteSOLineItem(String soLineItemId){

        List<SandDV1__Sales_Order_Line_Item__c> soLI = ServiceFacade.getSalesOrderLineItems(soLineItemId);

        if(!soLI.isEmpty()){
            SalesOrderService.deleteSOLineItem(soLI[0]);
        }
        
    }

    /*******************************************************************************************************
    * @description :Method to check Order creation based on account Id
    * @param accountId - Account Id
    * @return Wrapper.
    */
    
    @RemoteAction
    public static SalesOrderDomain.CreditBlockOrderWrapper checkOrderCreation(String accountId){

        SalesOrderDomain.CreditBlockOrderWrapper cbWrap = new SalesOrderDomain.CreditBlockOrderWrapper();

        List<Account> acc = ServiceFacade.fetchAccountById(accountId);
        cbWrap.creditLimit = acc[0].SandDV1__Credit_Limit__c;
        cbWrap.blockOrderCreation = acc[0].SandDV1__Block_Order_Creation__c;
        return cbWrap;
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id. Limited records are retrived.
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    @RemoteAction
    public static List<PricebookEntry> getLimitedPriceBookEntries(String priceBookId){


        return ServiceFacade.fetchLimitedPriceBookEntries(priceBookId);
    }


}