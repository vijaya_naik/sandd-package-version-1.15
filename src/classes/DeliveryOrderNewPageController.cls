/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class DeliveryOrderNewPageController {
    public DeliveryOrderNewPageController (ApexPages.StandardController controller) {

    }

    public DeliveryOrderNewPageController () {

    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Sales Order List from Sales Order Id.
    * @param :- Id.
    * @return :- list<SandDV1__Sales_Order__c>.
    */
    @RemoteAction
    public static list<SandDV1__Sales_Order__c> getSalesOrder(Id salesOrderID) {
         return ServiceFacade.fetchSalesOrderRecord(salesOrderID);
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch SalesOrder Record in wrapper from Sales Order Id.
    * @param :- Id.
    * @return :- DeliveryOrderDomain.salesOrderWrapper. 
    */
    @RemoteAction
    public static DeliveryOrderDomain.salesOrderWrapper fetchSalesOrder(Id salesOrderID) {
        DeliveryOrderDomain.salesOrderWrapper wrap = new DeliveryOrderDomain.salesOrderWrapper();
        List<SandDV1__Sales_Order_Line_Item__c> orderLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
        
        for(SandDV1__Sales_Order__c order : ServiceFacade.fetchSalesOrderRecord(salesOrderID)) {
            wrap.salesOrderDate = string.valueof(order.SandDV1__Date__c);
            if(order.SandDV1__Shipping_Address_Name__c == null) {
                order.SandDV1__Shipping_Address_Name__c = '';
            }
            if(order.SandDV1__Shipping_Street__c == null) {
                order.SandDV1__Shipping_Street__c = '';
            }
            if(order.SandDV1__Shipping_City__c == null) {
                order.SandDV1__Shipping_City__c = '';
            }
            if(order.SandDV1__Shipping_Country__c == null) {
                order.SandDV1__Shipping_Country__c = '';
            }
            if(order.SandDV1__Shipping_State_Province__c == null) {
                order.SandDV1__Shipping_State_Province__c = '';
            }
            if(order.SandDV1__Shipping_Zip_Postal_Code__c == null) {
                order.SandDV1__Shipping_Zip_Postal_Code__c = '';
            }
            wrap.salesAddress = order.SandDV1__Shipping_Address_Name__c + ' ' + order.SandDV1__Shipping_Street__c + ' ' + order.SandDV1__Shipping_City__c + ' ' +
                    order.SandDV1__Shipping_Country__c + ' ' +order.SandDV1__Shipping_State_Province__c + ' ' + order.SandDV1__Shipping_Zip_Postal_Code__c;
            wrap.salesOrderObj = order;
        }
        
        for(SandDV1__Sales_Order_Line_Item__c orderline : ServiceFacade.fetchSalesOrderLineItemRecordsGreaterQuantity(salesOrderID)) {
                (orderLineItemList).add(orderline);
        }
        wrap.orderLI = orderLineItemList;       
        return wrap;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SalesOrder Record in wrapper for dialog box.
    * @param :- List<SandDV1__Sales_Order_Line_Item__c>,List<SandDV1__Sales_Order_Line_Item__c>.
    * @return :- DeliveryOrderDomain.salesOrderWrapper. 
    */
    @RemoteAction 
    public static DeliveryOrderDomain.salesOrderWrapper fetchDialogOrder(List<SandDV1__Sales_Order_Line_Item__c> mainSalesOrderWrap,
                        List<SandDV1__Sales_Order_Line_Item__c> allSalesOrderWrap) {
        Set<Id> salesOrderId = new Set<Id>();
        List<SandDV1__Sales_Order_Line_Item__c> orderLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
        DeliveryOrderDomain.salesOrderWrapper wrap = new DeliveryOrderDomain.salesOrderWrapper();
        for(SandDV1__Sales_Order_Line_Item__c all : allSalesOrderWrap) {
            for(SandDV1__Sales_Order_Line_Item__c main : mainSalesOrderWrap) {
                salesOrderId.add(main.Id);
            }

            if(!salesOrderId.contains(all.Id)) {
                (orderLineItemList).add(all);
            }
        }
                wrap.orderLI = orderLineItemList;       
        return wrap;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch DeliveryOrder Record in wrapper from Delivery Order Id.
    * @param :- Id.
    * @return :- DeliveryOrderDomain.deliveryOrderWrapper. 
    */
    @RemoteAction
    public static DeliveryOrderDomain.deliveryOrderWrapper fetchDeliveryOrder(Id deliveryOrderId) {
        DeliveryOrderDomain.deliveryOrderWrapper wrap = new DeliveryOrderDomain.deliveryOrderWrapper();
        List<SandDV1__Delivery_Order_Line_Item__c> deliveryLineItemList = new List<SandDV1__Delivery_Order_Line_Item__c>();
        List<SandDV1__Sales_Order_Line_Item__c> salesLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
        Id salesOrderID = null;
        for(SandDV1__Delivery_Order__c order : DeliveryOrderService.fetchDeliveryOrderRecord(deliveryOrderId)) {
            wrap.deliveryOrderDate = string.valueof(order.SandDV1__Date__c);
            order.SandDV1__Shipping_Address_Name__c = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_Address_Name__c);
            order.SandDV1__Shipping_Street__c       = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_Street__c);
            order.SandDV1__Shipping_City__c = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_City__c);
            order.SandDV1__Shipping_Country__c = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_Country__c);
            order.SandDV1__Shipping_State_Province__c = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_State_Province__c);
            //order.SandDV1__Shipping_Zip_Postal_Code__c = DeliveryOrderService.modifiyString(order.SandDV1__Shipping_Zip_Postal_Code__c);
            if(order.SandDV1__Shipping_Address_Name__c == null) {
                order.SandDV1__Shipping_Address_Name__c = '';
            }
            if(order.SandDV1__Shipping_Street__c == null) {
                order.SandDV1__Shipping_Street__c = '';
            }
            if(order.SandDV1__Shipping_City__c == null) {
                order.SandDV1__Shipping_City__c = '';
            }
            if(order.SandDV1__Shipping_Country__c == null) {
                order.SandDV1__Shipping_Country__c = '';
            }
            if(order.SandDV1__Shipping_State_Province__c == null) {
                order.SandDV1__Shipping_State_Province__c = '';
            }
            if(order.SandDV1__Shipping_Zip_Postal_Code__c == null) {
                order.SandDV1__Shipping_Zip_Postal_Code__c = '';
            }
            wrap.deliveryAddress = order.SandDV1__Shipping_Address_Name__c + ' ' + order.SandDV1__Shipping_Street__c + ' ' + order.SandDV1__Shipping_City__c + ' ' +
                    order.SandDV1__Shipping_Country__c + ' ' +order.SandDV1__Shipping_State_Province__c + ' ' + string.valueof(order.SandDV1__Shipping_Zip_Postal_Code__c);
        
            wrap.deliverOrderObj = order;
            salesOrderID = order.SandDV1__Sales_Order__c;
        }

        for(SandDV1__Delivery_Order_Line_Item__c orderline : ServiceFacade.fetchDeliveryOrderLineItemRecords(deliveryOrderId)) {
                (deliveryLineItemList).add(orderline);
        }

        for(SandDV1__Sales_Order_Line_Item__c orderline : ServiceFacade.fetchSalesOrderLineItemRecords(salesOrderID)) {
                (salesLineItemList).add(orderline);
        }
        wrap.deliveryOrderLI = deliveryLineItemList;    
        wrap.salesOrderLI = salesLineItemList;          
        return wrap;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Delivery Order Record in wrapper for dialog box.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c>,List<SandDV1__Sales_Order_Line_Item__c>..
    * @return :- DeliveryOrderDomain.salesOrderWrapper. 
    */
    @RemoteAction 
    public static DeliveryOrderDomain.deliveryOrderWrapper fetchDeliveryDialogOrderList(List<SandDV1__Delivery_Order_Line_Item__c> mainDeliveryOrderWrap,
                                                                List<SandDV1__Sales_Order_Line_Item__c> allSalesOrderWrap) {
        Set<Id> deliveryOrderId = new Set<Id>();
        List<SandDV1__Sales_Order_Line_Item__c> salesLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
        List<SandDV1__Delivery_Order_Line_Item__c> orderLineItemList = new List<SandDV1__Delivery_Order_Line_Item__c>();
        DeliveryOrderDomain.deliveryOrderWrapper wrap = new DeliveryOrderDomain.deliveryOrderWrapper();
        Id deliveryId = null;
        for(SandDV1__Sales_Order_Line_Item__c all : allSalesOrderWrap) {
            if(all.SandDV1__Unfulfilled_Quantity__c > 0){
                for(SandDV1__Delivery_Order_Line_Item__c main : mainDeliveryOrderWrap) {
                    deliveryId = main.SandDV1__Delivery_Order__c;
                    deliveryOrderId.add(main.SandDV1__System_SalesOrderLine_Item__c);
                }

                if(!deliveryOrderId.contains(all.Id)) {
                    (salesLineItemList).add(all);
                }
            }
        }
            wrap.salesOrderLI = salesLineItemList;  
               
        return wrap;
    }

    /*******************************************************************************************************
    * @description :- Method to Save Delivery Order and Delivery Order Line Item.
    * @param :- SandDV1__Delivery_Order__c,List<SandDV1__Delivery_Order_Line_Item__c>,List<SandDV1__Delivery_Order_Line_Item__c>.
    * @return :- String.
    */
    @RemoteAction
    public static String saveDeliveryRecords(SandDV1__Delivery_Order__c orderObj,List<SandDV1__Delivery_Order_Line_Item__c> orderLineItemList,
                                                List<SandDV1__Delivery_Order_Line_Item__c> deleteOrderList) {
       return DeliveryOrderService.saveDeliveryRecords(orderObj,orderLineItemList,deleteOrderList);
    }
    
}