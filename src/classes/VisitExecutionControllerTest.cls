/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Sep 2015
* @description This class if for providing testdata to VisitExecutionController test class(VisitPlanTest).
*/

@isTest(seeAllData=false)
private class VisitExecutionControllerTest {
    
    @testSetup
    private static void VisitTestData() {
        
        Account acc = VisitPlanTestData.testAccount(1);
        insert acc;
        SandDV1__Period__c period = VisitPlanTestData.testPeriod(true, Date.newInstance(2015, 1, 1), Date.newInstance(2015, 1, 31));
        insert period;
        SandDV1__Visit_Plan__c visitPlan = VisitPlanTestData.testVisitPlan(period.Id, userInfo.getUserId());
        insert visitPlan;
        List<SandDV1__Visit__c> visitList;
        SandDV1__Visit__c visit1  = VisitPlanTestData.testVisit(acc.Id, visitPlan.Id);
        visit1.SandDV1__Visit_Plan_Date__c = Date.newInstance(2015, 1, 8);
        visit1.SandDV1__Status__c = 'Approved';
        SandDV1__Visit__c visit2  = VisitPlanTestData.testVisit(acc.Id, visitPlan.Id);
        visit2.SandDV1__Visit_Plan_Date__c = Date.newInstance(2015, 1, 15);
        visit2.SandDV1__Status__c = 'Approved';
        SandDV1__Visit__c visit3  = VisitPlanTestData.testVisit(acc.Id, visitPlan.Id);
        visit3.SandDV1__Visit_Plan_Date__c = Date.newInstance(2015, 1, 22);
        visit3.SandDV1__Status__c = 'Approved';
        SandDV1__Visit__c visit4  = VisitPlanTestData.testVisit(acc.Id, visitPlan.Id);
        visit4.SandDV1__Visit_Plan_Date__c = Date.newInstance(2015, 1, 29);
        visit4.SandDV1__Status__c = 'Approved';
        visitList = new List<SandDV1__Visit__c>{visit1, visit2, visit3, visit4};
        insert visitList;
        
        Integer visitCount = [select count() from SandDV1__Visit__c];
        system.assertEquals(visitCount, visitList.size());
    }
    
    private static testMethod void retreivePlannedVisitsTest() {
        
        VisitExecutionController ve = new VisitExecutionController();
        for(SandDV1__Visit__c visit : VisitExecutionController.getParticularDayVisits(22, 1, 2015)){
            system.assertEquals(visit.SandDV1__Visit_Plan_Date__c, Date.newInstance(2015, 1, 22));
        }
    }
}