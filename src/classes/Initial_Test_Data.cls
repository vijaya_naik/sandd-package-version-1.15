/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 31-Aug-2015
* @description This class is for getting the initial test data 
*/
@isTest
public class Initial_Test_Data {

  
  /*******************************************************************************************************
  * @description Method will return initial test data of User. 
  * @param LName-Last name, FName-First Name, profileName-Profile Name
  * @return User Object. 
  */
  
  public static User createUser(String LName ,String FName,String profileName,String roleId){
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName]; 
        User userData = new User(Alias = 'standt1', 
                                  Email= FName+LName+'@TestUser.com', 
                                  EmailEncodingKey='UTF-8', 
                                  FirstName = FName,
                                  LastName = LName, 
                                  LanguageLocaleKey='en_US', 
                                  LocaleSidKey='en_AU', 
                                  ProfileId = p.Id, 
                                  TimeZoneSidKey='Australia/Sydney', 
                                  UserName= FName+LName+'@TestUser.com',
                                  UserRoleId = roleId
                                  );
        return userData; 
    }


  /*******************************************************************************************************
  * @description Method will return initial test data of UserRole.
  * @param rolename -Role Name, parentId - Id of Parent Role
  * @return UserRole Object
  */

    public static UserRole createRole(String roleName,String parentId){

      UserRole r = new UserRole(
                  name = roleName,
                  ParentRoleId = parentId
                  );

      return r;
    }

     



    /*******************************************************************************************************
    * @description Method will return initial test data of Period
    * @param periodName - Period Name, masterId - Master period Id, noOfDays - number of days of period
    * @return Period Object 
    */
    
    public static SandDV1__Period__c createPeriod(String periodName,String masterId,Integer noOfDays){
        
        SandDV1__Period__c periodData = new SandDV1__Period__c(Name = periodName, 
                                  SandDV1__Active__c= true, 
                                  SandDV1__Periodicity__c='Weekly', 
                                  SandDV1__Start_Date__c = date.today(),
                                  SandDV1__End_Date__c = date.today().addDays(noOfDays),
                                  SandDV1__Master_Period__c = masterId
                                  );
        return periodData; 
    }


    /*******************************************************************************************************
    * @description Method will return initial test data of Target
    * @param status - Status of the target, assignedToId - User Id, periodId - Period Id 
    * @return Target Object 
    */
    
    public static SandDV1__Target__c createTarget(String status,String assignedToId,String periodId){
        
        SandDV1__Target__c targetData = new SandDV1__Target__c(SandDV1__Active__c= true, 
                                  SandDV1__Aspect__c='Revenue', 
                                  SandDV1__Status__c = status,
                                  SandDV1__Assigned_To__c = assignedToId,
                                  SandDV1__Period__c = periodId,
                                  SandDV1__Value__c = 100
                                  );
        return targetData; 
    }


    /*******************************************************************************************************
    * @description Method will return initial test data of Target Line Item
    * @param targetId - Target Id , periodId - Period Id
    * @return Target Line Item Object 
    */
    
    public static SandDV1__Target_Line_Item__c createTargetLI(String targetId,String periodId){
        
        SandDV1__Target_Line_Item__c targetLIData = new SandDV1__Target_Line_Item__c( 
                                  SandDV1__Aspect__c='Revenue', 
                                  SandDV1__Target__c = targetId,
                                  SandDV1__Period__c = periodId,
                                  SandDV1__Value__c = 100
                                  );
        return targetLIData; 
    }

    /*******************************************************************************************************
    * @description Method will return initial test data of Account
    * @param accName - Account Name
    * @return Account Object
    */
    
    public static Account createAccount(String accName,Integer creditLimit, Boolean blockOrderCreation){
        
        Account accData = new Account( 
                                  Name=accName, 
                                  SandDV1__Credit_Limit__c = creditLimit,
                                  SandDV1__Block_Order_Creation__c = blockOrderCreation
                                  );
        return accData; 
    }


    /*******************************************************************************************************
    * @description Method will return initial test data of Shipping Address 
    * @param accName - Account Name
    * @return Shipping Address object 
    */
    
    public static SandDV1__Shipping_Address__c createShippingAddress(String shipName,String accId){
        
        SandDV1__Shipping_Address__c shipData = new SandDV1__Shipping_Address__c( 
                                  Name=shipName, 
                                  SandDV1__Account__c = accId,
                                  SandDV1__Shipping_City__c = 'Test City',
                                  SandDV1__Shipping_Country__c = 'Test country',
                                  SandDV1__Shipping_State_Province__c = 'Test state',
                                  SandDV1__Shipping_Street__c = 'Test street',
                                  SandDV1__Shipping_Zip_Postal_Code__c = '500000'
                                  );
        return shipData; 
    }

    //
    /*******************************************************************************************************
    * @description Method will return initial test data of Product
    * @param proName - Product Name
    * @return Product2 object 
    */
    public static Product2 createProduct(String proName,String productCode) {

      Product2 newPro = new Product2(

              Name = proName,
              IsActive=true,
              ProductCode = productCode
              );

      return newPro;

    } 
    

    /*******************************************************************************************************
    * @description Method will return initial test data of PricebookEntry
    * @param price - Pricebook Id, prod - Product Id
    * @return PricebookEntry object 
    */
    public static PricebookEntry createPriceBookEntry(Id price,Id prod) {

      PricebookEntry newPriceE = new PricebookEntry(

            Pricebook2Id = price,

            UnitPrice = 0.0,

            IsActive = true,

            Product2Id = prod);

 
      return newPriceE;

    }   


    /*******************************************************************************************************
    * @description Method will return initial test data of Pricebook
    * @param pricebookName - Pricebook Name
    * @return Pricebook2 object 
    */
    public static Pricebook2 createPriceBook(String pricebookName) {

        Pricebook2 newPriceBook = new Pricebook2(

            Description = 'Test Pricebook',
            IsActive = true,
            Name = pricebookName
            );

        return newPriceBook;

    }   


    /*******************************************************************************************************
    * @description Method will return initial test data of SandDV1__Sales_Order__c
    * @param 
    * @return SandDV1__Sales_Order__c object 
    */
    public static SandDV1__Sales_Order__c createSalesOrder(String buyerId,String status,String priceBookId) {

        SandDV1__Sales_Order__c newSalesOrder = new SandDV1__Sales_Order__c(

            SandDV1__Buyer__c = buyerId,
            SandDV1__Date__c = date.today(),
            SandDV1__PO_Reference__c = 'Test Ref',
            SandDV1__Status__c = status,
            SandDV1__Price_Book__c = priceBookId,
            SandDV1__Remarks__c = 'Test'
            );

        return newSalesOrder;

    }   


    /*******************************************************************************************************
    * @description Method will return initial test data of Sales Order Line Item
    * @param 
    * @return Sales Order Line Item object 
    */
    public static SandDV1__Sales_Order_Line_Item__c createSalesOrderLI(String salesOrderId,String partId,String pricebookId) {

        SandDV1__Sales_Order_Line_Item__c newSLI = new SandDV1__Sales_Order_Line_Item__c(

            SandDV1__Sales_Order__c = salesOrderId,
            SandDV1__Part__c = partId,
            SandDV1__Price_Book__c = pricebookId,
            SandDV1__Quantity__c = 10,
            SandDV1__Discount__c = 10,
            SandDV1__Unit_Price__c = 100,
            SandDV1__Net_Price__c = 200,
            SandDV1__Sales_Price__c = 250
            );

        return newSLI;

    }  

    /*******************************************************************************************************
    * @description Method will return initial test data of Visit
    * @param 
    * @return Visit object 
    */
    public static SandDV1__Visit__c createVisit(String visitPlanId,String accId) {

        SandDV1__Visit__c newObj = new SandDV1__Visit__c(

            SandDV1__Visit_Plan__c = visitPlanId,
            SandDV1__Account__c = accId

            );

        return newObj;

    }  

    /*******************************************************************************************************
    * @description Method will return initial test data of Visit Plan
    * @param 
    * @return Visit Plan object 
    */
    public static SandDV1__Visit_Plan__c createVisitPlan(String periodId,String uId) {

        SandDV1__Visit_Plan__c newObj = new SandDV1__Visit_Plan__c(

            SandDV1__Period__c = periodId,
            SandDV1__User__c = uId

            );

        return newObj;

    }  

    /*******************************************************************************************************
    * @description Method will return initial test data of Complaint
    * @param 
    * @return Complaint object 
    */
    public static SandDV1__Complaint__c createComplaint(String accId,String priority) {

        SandDV1__Complaint__c newObj = new SandDV1__Complaint__c(

            SandDV1__Account__c = accId,
            SandDV1__Priority__c = priority,
            SandDV1__Status__c = 'Escalated'

            );

        return newObj;

    }  

    /*******************************************************************************************************
    * @description Method will return initial test data of Task
    * @param 
    * @return Task object 
    */
    public static Task createTask(String accId,String priority,String userId,Integer noOfDays) {

        Task newObj = new Task(

            WhatId = accId,
            Priority = priority,
            Status = 'Not Started',
            OwnerId  = userId,
            Subject = 'test',
            ActivityDate = date.today().addDays(noOfDays)
            );

        return newObj;

    }  


    /*******************************************************************************************************
    * @description Method will return initial test data of Expense__c
    * @param 
    * @return Expense__c object 
    */
    public static SandDV1__Expense__c createExpense(String name,String submittedBy,String status) {

        SandDV1__Expense__c newExpense = new SandDV1__Expense__c(

            Name = name,
            SandDV1__Date__c = date.today(),
            SandDV1__Submitted_By__c = submittedBy,
            SandDV1__Status__c = status

            );

        return newExpense;

    }   

}