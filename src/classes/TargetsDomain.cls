/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 24-Nov-2015
* @description This is a Domain class to store wrapper class
*/
public with sharing class TargetsDomain {
	

	/*******************************************************************************************************
    * @description :Wrapper class to store Target, Child Targets, Periods and their Target Line 
                   Item Info with some other flags
    * @param 
    * @return 
    */
    public class TargetInfoWrapper {
        public SandDV1__Target__c                    targetObj {get; set;}
        public List<TargetInfoWrapper>     	childTargetsWrapperList {get; set;}
        public List<PeriodTLIWrapper>   	periodTliWrapList {get;set;}
        public Boolean hasParent {get; set;}
        public Boolean hasSubordinates {get; set;}
        public Boolean isAssigned  {get; set;}
        public Boolean isRevised   {get; set;}
        public Boolean isDraft     {get; set;}
        public Boolean isAccepted  {get; set;}
        public Boolean isSuspended  {get; set;}
        public User userInstance {get; set;}

        
        public TargetInfoWrapper() {
            this.targetObj                  = new SandDV1__Target__c();
            this.childTargetsWrapperList    = new List<TargetInfoWrapper>();
            this.periodTliWrapList          = new List<PeriodTLIWrapper>();
            this.hasParent                  = false;
            this.hasSubordinates            = false;
            this.isAssigned                 = false;
            this.isRevised                  = false;
            this.isDraft                    = true;
            this.isAccepted                 = false;
            this.isSuspended                = false;
            this.userInstance 				= new User();
        }
    }


    /*******************************************************************************************************
    * @description :Wrapper class to store Target Line Item with Period name
    * @param <param name> .
    * @return <return type> . 
    */

    public class PeriodTLIWrapper {

        public String                       periodName{get;set;}
        public String                       periodEndDate{get;set;}
        public String                       periodStartDate{get;set;}
        public SandDV1__Target_Line_Item__c          tliObj{get;set;}
        public PeriodTLIWrapper(){
            this.periodName = '';
            this.periodEndDate = '';
            this.periodStartDate = '';
            this.tliObj = new SandDV1__Target_Line_Item__c();
        }
    }


    /*******************************************************************************************************
    * @description :Wrapper class to store Target Id after Save with Return Message
    * @param <param name> .
    * @return <return type> . 
    */
    public class TargetMessageInfo {
         
         public String     targetID{get;set;}
         public String     targetReturnMessage{get;set;}

    }
}