/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a Service class for salesorder..
*/
public with sharing class SalesOrderService {

    public static SandDV1__Sales_Order__c getSalesOrder(String salesOrderId){
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [SELECT Id, Name, SandDV1__Buyer__c,SandDV1__System_No_of_Sales_Order__c 
                FROM SandDV1__Sales_Order__c WHERE Id = :salesOrderId];
    }

    /*******************************************************************************************************
    * @description : Method to save Sales order and sales order line item
    * @param :  salesOrder- Sales order that should be saved. salesOrderLI - Sales order line item to insert, 
                deleteSalesOrderLI - salesOrderLI - Sales order line item to delete
    * @return String 
    */
    
    public static String saveSalesOrder(SandDV1__Sales_Order__c salesOrder,List<SandDV1__Sales_Order_Line_Item__c> salesOrderLI,
                                            List<SandDV1__Sales_Order_Line_Item__c> deleteSalesOrderLI,Boolean isDraft){

        Set<Id> soliIds = new Set<Id>();
        String salesOrderId;
        Savepoint sp = Database.setSavepoint();
        try {
            if(salesOrder != null){

                if(isDraft){
                    salesOrder.SandDV1__Status__c = ConstantsClass.getSalesOrderStatusValues().get('isDraft');
                }
                else {
                    salesOrder.SandDV1__Status__c = ConstantsClass.getSalesOrderStatusValues().get('isOpen');
                }
                
                List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility.fetchSalesOrderFields();
                SecurityUtils.checkInsert(SandDV1__Sales_Order__c.SObjectType,salesOrderFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Sales_Order__c.SObjectType,salesOrderFieldsList);
                Database.UpsertResult sr = Database.upsert(salesOrder,false);
                if(sr.isSuccess()){
                    if(!salesOrderLI.isempty()) {
                        salesOrderId = sr.getId();
                        for(SandDV1__Sales_Order_Line_Item__c so:salesOrderLI){

                            if(so.SandDV1__Sales_Order__c == null){
                                so.SandDV1__Sales_Order__c = sr.getId();
                            }
                        }

                        List<Schema.SObjectField> salesOLIFieldsList = FieldAccessibilityUtility.fetchSalesOrderLineItemFields();
                        SecurityUtils.checkInsert(SandDV1__Sales_Order_Line_Item__c.SObjectType,salesOLIFieldsList);
                        SecurityUtils.checkUpdate(SandDV1__Sales_Order_Line_Item__c.SObjectType,salesOLIFieldsList);
                        Upsert salesOrderLI;
                    }
                    if(!deleteSalesOrderLI.isempty()) {
                        SecurityUtils.checkObjectIsDeletable(SandDV1__Sales_Order_Line_Item__c.SObjectType);
                        delete deleteSalesOrderLI;
                    }
                }
                else {
                    
                    throw new CustomException('Error while inserting Sales Order');
                }
                return salesOrderId;
            }
            else{

                throw new CustomException('Error while inserting Sales Order');
            }
        }
        catch(Exception e) {

            Database.rollback(sp);
            throw new CustomException(e.getMessage());
        }
            
    }

    /*******************************************************************************************************
    * @description : Method to fetch Sales order and sales order line items
    * @param : salesOrderId - Id of Sales order
    * @return List<SandDV1__Sales_Order__c>
    */

    public static List<SandDV1__Sales_Order__c> fetchSalesOrderAndLineItem(String salesOrderId){
        
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields2();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [SELECT Id,SandDV1__Buyer__c,SandDV1__Buyer__r.Name,SandDV1__Order_Amount__c,SandDV1__Discount__c,SandDV1__Total_Line_Item_Amount__c,SandDV1__Total_Line_Item_Discount__c,SandDV1__Date__c,SandDV1__PO_Reference__c,SandDV1__Price_Book__c,SandDV1__Status__c,
            SandDV1__Price_Book__r.Name,SandDV1__Remarks__c,SandDV1__Shipping_Address_Name__c,SandDV1__Shipping_City__c,SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,
            SandDV1__Shipping_Street__c,SandDV1__Shipping_Zip_Postal_Code__c,SandDV1__System_No_of_Sales_Order__c ,(SELECT Id,SandDV1__Sales_Order__c,SandDV1__Discount__c,SandDV1__Net_Price__c,SandDV1__Part__c,SandDV1__Part__r.Name,
            SandDV1__Part__r.ProductCode,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Sales_Price__c,SandDV1__Type__c,SandDV1__Unit_Price__c,SandDV1__UOM__c,
            SandDV1__System_Related_to_SalesOrderLineItem__c ,SandDV1__Sales_Order_Item_Benefit__c,SandDV1__Fulfilled_Quantity__c 
            FROM SandDV1__Sales_Order_Line_Items__r) FROM SandDV1__Sales_Order__c WHERE Id=:salesOrderId];
    } 

    /*******************************************************************************************************
    * @description : Method to fetch Sales order and sales order line items
    * @param : salesOrderId- Id of Sales order
    * @return Sales order 
    */
    
    public static SalesOrderDomain.SalesOrderWrapper fetchSalesOrder(String salesOrderId){

        if(salesOrderId != null){
            
            List<SandDV1__Sales_Order__c> salesOrder = fetchSalesOrderAndLineItem(salesOrderId);
            
            if(!salesOrder.isEmpty()){

                map<Id,List<SandDV1__Sales_Order_Line_Item__c>> SalesBenefitMap = new map<Id,List<SandDV1__Sales_Order_Line_Item__c>>();
                map<Id,List<SandDV1__Sales_Order_Line_Item__c>> SalesOrderLineItemMap = new map<Id,List<SandDV1__Sales_Order_Line_Item__c>>();
                map<Id,SandDV1__Sales_Order__c> salesOrderMap = new map<Id,SandDV1__Sales_Order__c>();

                for(SandDV1__Sales_Order__c sales : salesOrder) {
                    salesOrderMap.put(sales.Id,sales);
                    for(SandDV1__Sales_Order_Line_Item__c salesLineItem : sales.SandDV1__Sales_Order_Line_Items__r) {
          
                        if(salesLineItem.SandDV1__Sales_Order_Item_Benefit__c == null) {
                            if(SalesOrderLineItemMap.containskey(sales.Id)) {
                                SalesOrderLineItemMap.get(sales.Id).add(salesLineItem);
                            }
                            else {
                                List<SandDV1__Sales_Order_Line_Item__c> salesOrderLIList = new List<SandDV1__Sales_Order_Line_Item__c>();
                                salesOrderLIList.add(salesLineItem);
                                SalesOrderLineItemMap.put(sales.Id,salesOrderLIList);
                            }
                        }
                        else {
                            if(SalesBenefitMap.containskey(salesLineItem.SandDV1__System_Related_to_SalesOrderLineItem__c)) {
                                SalesBenefitMap.get(salesLineItem.SandDV1__System_Related_to_SalesOrderLineItem__c).add(salesLineItem);
                            }
                            else {
                                List<SandDV1__Sales_Order_Line_Item__c> benefitSalesList = new List<SandDV1__Sales_Order_Line_Item__c>();
                                benefitSalesList.add(salesLineItem);
                                SalesBenefitMap.put(salesLineItem.SandDV1__System_Related_to_SalesOrderLineItem__c,benefitSalesList);
                            }
                        }
                    }
                }

                SalesOrderDomain.SalesOrderWrapper salesWrap = new SalesOrderDomain.SalesOrderWrapper ();
                for(String salesOrderIID :SalesOrderLineItemMap.keyset()) {
                    salesWrap.salesOrder = salesOrderMap.get(salesOrderID);
                    
                    
                    for(SandDV1__Sales_Order_Line_Item__c orderLine :SalesOrderLineItemMap.get(salesOrderID)) {
                        SalesOrderDomain.OrderLineItemWrapper orderWrap = new SalesOrderDomain.OrderLineItemWrapper(); 
                   
                        orderWrap.salesOrderLineItem = orderLine;
                        if(SalesBenefitMap.containskey(orderLine.Id)) {
                            orderWrap.salesBenefitLineItemList = SalesBenefitMap.get(orderLine.Id);  
                        }
                        salesWrap.orderLineItemWrapList.add(orderWrap); 
                    }                   
                }
                return salesWrap;
            }
            else {

                return null;
            }
        }
        else {

            return null;
        }
    }


    /*******************************************************************************************************
    * @description method to delete salesorder line item
    * @param soLI - Sales order line item
    * @return void
    */
    
    @RemoteAction
    public static void deleteSOLineItem(SandDV1__Sales_Order_Line_Item__c soLI){
        try{

            SecurityUtils.checkObjectIsDeletable(SandDV1__Sales_Order_Line_Item__c.SObjectType);
            delete soLI;
        }
        catch(Exception e) {

            throw new CustomException(e.getMessage());
        }
        
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SalesOrder List from Sales Order Id.
    * @param :- Id.
    * @return :- List<SandDV1__Sales_Order__c>. 
    */
    public static List<SandDV1__Sales_Order__c> fetchSalesOrderRecord(Id salesOrderId) {
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields3();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [Select Id,SandDV1__Buyer__c,SandDV1__Buyer__r.Name,SandDV1__Date__c,SandDV1__Approval_Status__c,SandDV1__No_of_Parts__c,SandDV1__PO_Reference__c,SandDV1__PO_Status__c,SandDV1__Scheme_Amount__c,
                SandDV1__Status__c,SandDV1__Discount__c,SandDV1__Total_Line_Item_Amount__c,SandDV1__Order_Amount__c,SandDV1__Total_Line_Item_Discount__c,Name,SandDV1__Shipping_City__c,SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,SandDV1__Shipping_Street__c,
                SandDV1__Shipping_Zip_Postal_Code__c,SandDV1__Shipping_Address_Name__c,SandDV1__System_No_of_Sales_Order__c  from SandDV1__Sales_Order__c where Id =: salesOrderId];
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SalesOrder List from Account Id.
    * @param :- accId-Account Id, limitRecords-Number of records that needs to be retrived
    * @return :- List<SandDV1__Sales_Order__c>. 
    */
    public static List<SandDV1__Sales_Order__c> fetchSalesOrderList(String accId,Integer limitRecords) {
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields4();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [SELECT SandDV1__Buyer__c,SandDV1__Date__c,Id,Name,SandDV1__Discount__c,SandDV1__Total_Line_Item_Amount__c,SandDV1__Order_Amount__c,SandDV1__Total_Line_Item_Discount__c,SandDV1__Status__c,SandDV1__System_Date__c,
                                    SandDV1__System_No_of_Sales_Order__c FROM SandDV1__Sales_Order__c 
                                    WHERE SandDV1__Buyer__c = :accId ORDER BY SandDV1__Date__c DESC NULLS FIRST LIMIT :limitRecords]; 
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order__c List from Sales Order Id Set. 
    * @param :- set of Id.
    * @return :- List<SandDV1__Sales_Order__c>. 
    */
    public static List<SandDV1__Sales_Order__c> fetchSalesOrderRec(Set<Id> salesOrderId) {
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields3();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [Select Id,SandDV1__Buyer__c,SandDV1__Buyer__r.Name,SandDV1__Date__c,SandDV1__Approval_Status__c,SandDV1__No_of_Parts__c,SandDV1__PO_Reference__c,SandDV1__PO_Status__c,SandDV1__Scheme_Amount__c,SandDV1__Status__c,SandDV1__Discount__c,SandDV1__Total_Line_Item_Amount__c,SandDV1__Order_Amount__c,SandDV1__Total_Line_Item_Discount__c,Name,
                SandDV1__Shipping_City__c,SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,SandDV1__Shipping_Street__c,SandDV1__Shipping_Zip_Postal_Code__c,
                SandDV1__Shipping_Address_Name__c,SandDV1__System_No_of_Sales_Order__c  from SandDV1__Sales_Order__c where Id In: salesOrderId];
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order__c and its line item.This is limited to some fields.
    * @param :- set of Id.
    * @return :- SandDV1__Sales_Order__c 
    */
    public static SandDV1__Sales_Order__c fetchSalesOrderAndLineItemById(String salesOrderId) {
        List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility2.fetchSalesOrderFields5();
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldsList);
        return [SELECT Id,SandDV1__Buyer__c,SandDV1__Date__c,SandDV1__Price_Book__c,SandDV1__Shipping_Address_Name__c,SandDV1__Shipping_City__c,
            SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,SandDV1__Shipping_Street__c,SandDV1__Shipping_Zip_Postal_Code__c,SandDV1__System_No_of_Sales_Order__c ,
            (SELECT SandDV1__Part__c,SandDV1__Price_Book__c,SandDV1__Quantity__c,SandDV1__Type__c,SandDV1__Unit_Price__c,SandDV1__UOM__c FROM SandDV1__Sales_Order_Line_Items__r 
            WHERE SandDV1__Type__c != 'FOC') FROM SandDV1__Sales_Order__c WHERE Id=:salesOrderId];
    }


    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order__c and its line item.This is limited to some fields.
    * @param :- set of Id.
    * @return :- SandDV1__Sales_Order__c 
    */

    public static PageReference cloneSalesOrder(String salesOrderId){

        List<SandDV1__Sales_Order_Line_Item__c> saleOrderItmList = new List<SandDV1__Sales_Order_Line_Item__c>();
        SandDV1__Sales_Order__c saleOrderToInsert = new SandDV1__Sales_Order__c();
        PageReference pageRef;
        Savepoint sp = Database.setSavepoint();
        try{
    
            SandDV1__Sales_Order__c saleOrder = fetchSalesOrderAndLineItemById(salesOrderId);
            saleOrderToInsert = saleOrder.clone(false);
            saleOrderItmList = saleOrder.SandDV1__Sales_Order_Line_Items__r.deepClone(false);
            saleOrderToInsert.SandDV1__Status__c = 'Draft';
            List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility.fetchSalesOrderFields3();
            SecurityUtils.checkInsert(SandDV1__Sales_Order__c.SObjectType,salesOrderFieldsList);
            Database.SaveResult sr = Database.insert(saleOrderToInsert,false);

            if(sr.isSuccess()){

                pageRef = new PageReference('/'+sr.getId());
                if(!saleOrderItmList.isEmpty()){
                    for(SandDV1__Sales_Order_Line_Item__c so:saleOrderItmList){

                        so.SandDV1__Sales_Order__c = sr.getId();
                    }

                    List<Schema.SObjectField> salesOLIFieldsList = FieldAccessibilityUtility.fetchSalesOrderLineItemFields();
                    SecurityUtils.checkInsert(SandDV1__Sales_Order_Line_Item__c.SObjectType,salesOLIFieldsList);
                    insert saleOrderItmList;
                    return pageRef.setRedirect(true);
                }
                else {
        
                    return pageRef.setRedirect(true);

                }
                
            }
            else {

                pageRef = new PageReference('/'+salesOrderId);
                return pageRef.setRedirect(true);
                
            }
        }
        catch(Exception e) {

            Database.rollback(sp);
            throw new CustomException(e.getMessage());
        }
        
    }

}