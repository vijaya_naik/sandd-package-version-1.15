/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class DeliveryOrderService {
    
    /*******************************************************************************************************
    * @description :- Method to fetch DeliveryOrder List from DeliveryOrder Id.
    * @param :- Id.
    * @return :- List<SandDV1__Delivery_Order__c>. 
    */
    public static List<SandDV1__Delivery_Order__c> fetchDeliveryOrderRecord(Id deliveryOrderID) {
        List<Schema.SObjectField> deliveryOrderFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderFields2();
        SecurityUtils.checkRead(SandDV1__Delivery_Order__c.SObjectType, deliveryOrderFieldsList); 
        return [Select Id,SandDV1__Buyer__c,SandDV1__Buyer__r.Name,SandDV1__Date__c,SandDV1__Sales_Order__c,SandDV1__No_of_Parts__c,SandDV1__PO_Reference__c,SandDV1__PO_Status__c,SandDV1__Status__c,SandDV1__Total_Amount__c,Name,
                SandDV1__Shipping_City__c,SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,SandDV1__Shipping_Street__c,SandDV1__Shipping_Zip_Postal_Code__c,
                SandDV1__Shipping_Address_Name__c from SandDV1__Delivery_Order__c where Id =: deliveryOrderID];
    }

     /*******************************************************************************************************
    * @description :- Method to make string from null to blank. 
    * @param :- String.
    * @return :- String. 
    */
    public static string modifiyString(String textStriing) {
        if(textStriing == null){
            textStriing = '';
        }
        return textStriing;
    }

     /*******************************************************************************************************
    * @description :- Method to Save Delivery Order and Delivery Order Line Item.
    * @param :- SandDV1__Delivery_Order__c,List<SandDV1__Delivery_Order_Line_Item__c>,List<SandDV1__Delivery_Order_Line_Item__c>.
    * @return :- String.
    */
    public static String saveDeliveryRecords(SandDV1__Delivery_Order__c orderObj,List<SandDV1__Delivery_Order_Line_Item__c> orderLineItemList,
                                                List<SandDV1__Delivery_Order_Line_Item__c> deleteOrderList) {
        String salesOrderID = '';
        Savepoint sp = Database.setSavepoint();
        try { 
            if(orderObj != null) {
                salesOrderID = orderObj.SandDV1__Sales_Order__c;
                //Before Upsert
                List<Schema.SObjectField> deliveryFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderFields();
                SecurityUtils.checkInsert(SandDV1__Delivery_Order__c.SObjectType,deliveryFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Delivery_Order__c.SObjectType,deliveryFieldsList);
                upsert orderObj;
            }
            if(!orderLineItemList.isEmpty()) {
                for(SandDV1__Delivery_Order_Line_Item__c doli :orderLineItemList){
                    if(doli.SandDV1__Delivery_Order__c == null) {
                        doli.SandDV1__Delivery_Order__c = orderObj.Id;
                    }
                }
                List<Schema.SObjectField> deliveryOLIFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderLineItemFields();
                SecurityUtils.checkInsert(SandDV1__Delivery_Order_Line_Item__c.SObjectType,deliveryOLIFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Delivery_Order_Line_Item__c.SObjectType,deliveryOLIFieldsList);
                upsert orderLineItemList;
            }
            if(!deleteOrderList.isEmpty()) {
                //Before Delete
            SecurityUtils.checkObjectIsDeletable(SandDV1__Delivery_Order_Line_Item__c.SObjectType);
                delete deleteOrderList;
            }
            return salesOrderID;
        }
        catch(Exception e) {
            Database.rollback(sp);
            
            throw new CustomException(e.getMessage());
           // return null;
        }
    }

    /*******************************************************************************************************
    * @description .Method to fetch Delivery Order
    * @param <param name> .accId - Account Id
    * @return <return type> .List of Delivery Order object
    */

    public static List<SandDV1__Delivery_Order__c> fetchDeliveryOrders(String accId,Integer limitRecords){
        List<Schema.SObjectField> deliveryOrderFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderFields3();
        SecurityUtils.checkRead(SandDV1__Delivery_Order__c.SObjectType, deliveryOrderFieldsList);   
        return [SELECT SandDV1__Buyer__c,SandDV1__Date__c,Id,Name,SandDV1__Status__c,SandDV1__Total_Amount__c,SandDV1__System_Date__c,SandDV1__Sales_Order__r.Name  FROM SandDV1__Delivery_Order__c 
                  WHERE SandDV1__Buyer__c = :accId ORDER BY SandDV1__Date__c DESC NULLS FIRST LIMIT :limitRecords];      
    }

    
    /*******************************************************************************************************
    * @description :- Method to change Delivery Order status.
    * @param :- SandDV1__Delivery_Order__c.
    * @return :- String.
    */
    public static String changeDeliveryOrderStatus(SandDV1__Delivery_Order__c orderObj,String status){
        
        Savepoint sp = Database.setSavepoint();
        try { 

            orderObj.SandDV1__Status__c = status;
            List<Schema.SObjectField> deliveryFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderFields();
            deliveryFieldsList.add(SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Status__c);
            SecurityUtils.checkUpdate(SandDV1__Delivery_Order__c.SObjectType,deliveryFieldsList);
            update orderObj;
            return orderObj.Id;
            
        }catch(Exception e) {
            Database.rollback(sp);
            
            throw new CustomException(e.getMessage());
        }
    }
}