/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Test class of SalesOrderNewOverrideController
*/

@isTest(seeAllData = false)
private class SalesOrderEditOverrideControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static SandDV1__Sales_Order__c testSalesOrder;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.AssertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        System.runAs(sysAdmin){
            
            testSalesOrder = Initial_Test_Data.createSalesOrder(null,'Draft',null);
            insert testSalesOrder;


        }
    }
    @isTest static void test_method_one() {
        
        init();

        System.runAs(testUser){
            Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(testSalesOrder);
            ApexPages.currentPage().getParameters().put('retURL','/'+testSalesOrder.Id);
            ApexPages.currentPage().getParameters().put('id',testSalesOrder.Id);
            SalesOrderEditOverrideController trc = new SalesOrderEditOverrideController(sc); 
            System.AssertEquals(trc.soStatus, testSalesOrder.SandDV1__Status__c);
            Test.stopTest();  
        } 
    }
    
}