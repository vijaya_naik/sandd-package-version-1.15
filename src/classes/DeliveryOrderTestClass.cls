/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/*
Test Class is written to cover Delivery Order and Line Item Classes,triggers and trigger related with Sales Order.
*/
@isTest(seeAllData = false)
private class DeliveryOrderTestClass {

    

    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    static Account accountObj;
    static Product2 productObj;
    static Pricebook2 pricebookObj;
    static SandDV1__Sales_Order__c salesOrderObj;
    static SandDV1__Delivery_Order__c deliveryObj;


    static List<SandDV1__Sales_Order_Line_Item__c> salesOrderLIList = new List<SandDV1__Sales_Order_Line_Item__c>();
    static List<SandDV1__Delivery_Order_Line_Item__c> deliveryOrderLiList = new List<SandDV1__Delivery_Order_Line_Item__c>();

    static void init() {
        //create dummy Account
        accountobj = InitializeTest.createAccount();
        insert accountobj;
        System.assertequals(accountobj.Name,'testaccount');
        //create dummy product
        productObj = InitializeTest.createProduct();
        insert productObj;
        //create dummy price book
        pricebookObj = InitializeTest.createPricebook();
        insert pricebookObj;
        //create dummy sales order 
        salesOrderObj = InitializeTest.createSalesOrder(accountobj.Id,'Open');
        insert salesOrderObj;
        System.assertequals(salesOrderObj.SandDV1__Status__c,'Open');

        // create multiple sales order line item under sales order 
        for(integer i=0;i<10;i++){
            SandDV1__Sales_Order_Line_Item__c salesOrderLIObj = InitializeTest.createSalesOrderLineItem(salesOrderObj.Id,productObj.Id,0,pricebookObj.Id,10,i);
            salesOrderLIList.add(salesOrderLIObj);
        }
        insert salesOrderLIList;

        //create dummy delivery order 
        deliveryObj = InitializeTest.createDeliveryOrder(accountobj.Id,salesOrderObj.Id,'Open');
        insert deliveryObj;
        System.assertequals(deliveryObj.SandDV1__Status__c,'Open');

        //create dummy delivery order line item 
        for(integer i=0;i<10;i++){
            SandDV1__Delivery_Order_Line_Item__c deliveryOLIObj = InitializeTest.createDeliveryOrderLineItem(deliveryObj.Id,productObj.Id,pricebookObj.Id,salesOrderLIList[i].Id,2,i);
            deliveryOrderLiList.add(deliveryOLIObj);
        }
        insert deliveryOrderLiList; 
    }

    @isTest static void DeliveryTriggersTest() {

        init();
        List<SandDV1__Delivery_Order_Line_Item__c> deletedeliveryOLIList = new List<SandDV1__Delivery_Order_Line_Item__c>();
        List<SandDV1__Delivery_Order_Line_Item__c> updateDeliveryList = new List<SandDV1__Delivery_Order_Line_Item__c>();
        for(integer i=0;i<deliveryOrderLiList.size();i++){
            if(i==1){
                deletedeliveryOLIList.add(deliveryOrderLiList[i]);
            }
            if(i==5){
                deliveryOrderLiList[i].SandDV1__Quantity__c = 10;
                updateDeliveryList.add(deliveryOrderLiList[i]);
            }
            if(i<5 && i != 1){
                deliveryOrderLiList[i].SandDV1__Quantity__c = 6;
                updateDeliveryList.add(deliveryOrderLiList[i]);
            }
            if(i == 6){
                deliveryOrderLiList[i].SandDV1__Quantity__c = 1;
                updateDeliveryList.add(deliveryOrderLiList[i]);
            }
        }
        delete deletedeliveryOLIList;
        update updateDeliveryList;
        delete deliveryObj;
    }

    @isTest static void DeliveryOrderNewPageControllerTest() {

        init();
        List<SandDV1__Delivery_Order_Line_Item__c> deletedeliveryOLIList = new List<SandDV1__Delivery_Order_Line_Item__c>();
        List<SandDV1__Sales_Order_Line_Item__c> mainSalesOrderWrapList = new List<SandDV1__Sales_Order_Line_Item__c>();
        for(integer i=0;i<salesOrderLIList.size();i++){
            if(i<5){
                mainSalesOrderWrapList.add(salesOrderLIList[i]);
            }
            
        }
        DeliveryOrderNewPageController deliveryController = new DeliveryOrderNewPageController();
        DeliveryOrderNewPageController.getSalesOrder(salesOrderObj.Id);
        DeliveryOrderNewPageController.fetchSalesOrder(salesOrderObj.Id);
        DeliveryOrderNewPageController.fetchDialogOrder(mainSalesOrderWrapList,salesOrderLIList);
        DeliveryOrderNewPageController.fetchDeliveryOrder(deliveryObj.Id);
        DeliveryOrderNewPageController.fetchDeliveryDialogOrderList(deliveryOrderLiList,salesOrderLIList);
        SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
        try {
            DeliveryOrderNewPageController.saveDeliveryRecords(deliveryObj,deliveryOrderLiList,deletedeliveryOLIList);
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    @isTest static void SalesOrderTriggerTest() {

        init();
        List<SandDV1__Sales_Order_Line_Item__c> deleteSalesOLIList = new List<SandDV1__Sales_Order_Line_Item__c>();
        for(integer i=0;i<salesOrderLIList.size();i++) {
            if(i<4) {
                deleteSalesOLIList.add(salesOrderLIList[i]);
            }
        }
        try {
            delete deleteSalesOLIList;
        }
        catch(Exception e) {

        }
        try {
            delete salesOrderObj;
        }
        catch(Exception e) {}

    }
    
     @isTest static void DeliveryOrderCancelControllerTest() {

        init();
        ApexPages.StandardController sc = new ApexPages.StandardController(deliveryObj);
        DeliveryOrderCancelController deliveryCtrl= new DeliveryOrderCancelController(sc);
        deliveryCtrl.cancelDeliveryOrder();
        //This is to cover else part
        deliveryCtrl.cancelDeliveryOrder();
     }
     
     @isTest static void DeliveryOrderCloseControllerTest() {

        init();
        ApexPages.StandardController sc = new ApexPages.StandardController(deliveryObj);
        DeliveryOrderCloseController deliveryCtrl= new DeliveryOrderCloseController(sc);
        deliveryCtrl.closeDeliveryOrder();
        //This is to cover else part
        deliveryCtrl.closeDeliveryOrder();
     }
}