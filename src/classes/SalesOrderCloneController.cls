/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author ET Marlabs
* @date 21-Dec-2015
* @description This is a controller for salesOrderClonePage
*/
global with sharing class SalesOrderCloneController {

	public String salesOrderId;

	public SalesOrderCloneController (ApexPages.StandardController controller) {

		salesOrderId = controller.getId();
    }

    public PageReference cloneSalesOrder(){

		return SalesOrderService.cloneSalesOrder(salesOrderId); 
    }
}