/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SalesorderLineItemTriggerHandler {
    
    
    private static SalesorderLineItemTriggerHandler instance;

    
    public static SalesorderLineItemTriggerHandler getInstance() {
        if (instance == null) {
            instance = new SalesorderLineItemTriggerHandler();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public void onBeforeUpdate(List<SandDV1__Sales_Order_Line_Item__c> newobjects,Map<Id,SandDV1__Sales_Order_Line_Item__c> oldMap) {
        
        for(SandDV1__Sales_Order_Line_Item__c soli : newobjects) {
            if(soli.SandDV1__Quantity__c  == null) {
                soli.SandDV1__Quantity__c  = 0;
            }
            if(soli.SandDV1__Discount__c  == null) {
                soli.SandDV1__Discount__c  = 0;
            }
    
            if(soli.SandDV1__Discount_Percentage__c != oldMap.get(soli.Id).SandDV1__Discount_Percentage__c && soli.SandDV1__Discount_Percentage__c != null) {
               if(soli.SandDV1__Discount_Percentage__c != oldMap.get(soli.Id).SandDV1__Discount_Percentage__c && soli.SandDV1__Discount_Percentage__c < oldMap.get(soli.Id).SandDV1__Discount_Percentage__c) {
                   soli.SandDV1__Discount__c =   soli.SandDV1__Discount__c - ((oldMap.get(soli.Id).SandDV1__Discount_Percentage__c*soli.SandDV1__Unit_Price__c )/100);
               }
                   soli.SandDV1__Discount__c =   soli.SandDV1__Discount__c + ((soli.SandDV1__Discount_Percentage__c*soli.SandDV1__Unit_Price__c )/100);
            }

            if(soli.SandDV1__Quantity__c != oldMap.get(soli.Id).SandDV1__Quantity__c || soli.SandDV1__Discount__c != oldMap.get(soli.Id).SandDV1__Discount__c) {
                soli.SandDV1__Sales_Price__c = soli.SandDV1__Unit_Price__c - soli.SandDV1__Discount__c;
                soli.SandDV1__Net_Price__c = (soli.SandDV1__Sales_Price__c) * soli.SandDV1__Quantity__c; 
               
            }
        }
            
           
    }



    /*******************************************************************************************************
    * @description :- on After Update of Sales Order Line Item extract SalesOrder ID and then all Line Items under 
        that OrderID and then calculate Unfilled Quantity and Quantity of line items to fetch Order Status.
    * @param :- List<SandDV1__Sales_Order_Line_Item__c> newobjects,Map<Id,SandDV1__Sales_Order_Line_Item__c> oldMap .
    * @return :- void. 
    */
    
    public void onAfterUpdate(List<SandDV1__Sales_Order_Line_Item__c> newobjects,Map<Id,SandDV1__Sales_Order_Line_Item__c> oldMap) {
        //Set<Id> salesOrderLineItemSet = new Set<Id>();
        Set<Id> salesOrderIdSet = new Set<Id>();
        Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> salesOrderMap = new Map<Id,List<SandDV1__Sales_Order_Line_Item__c>>();

        for(SandDV1__Sales_Order_Line_Item__c soli : newobjects) {
            if(soli.SandDV1__System_Sales_Order_Status__c != System.Label.SalesOrderDraftStatus){
                if(soli.SandDV1__Unfulfilled_Quantity__c != oldMap.get(soli.Id).SandDV1__Unfulfilled_Quantity__c) {
                    salesOrderIdSet.add(soli.SandDV1__Sales_Order__c);
                }
            }
           /* if(soli.SandDV1__Quantity__c != oldMap.get(soli.Id).SandDV1__Quantity__c) {
                salesOrderLineItemSet.add(soli.Id);   
            }*/
        }

        salesOrderMap = fetchSalesOrderLineItems(salesOrderIdSet);

        if(!salesOrderMap.isEmpty()) {
            updateSalesOrder(salesOrderMap);
        }
       
    }

    /*******************************************************************************************************
    * @description :- Method to fetch all SalesOrderLineItems deom Sales Order ID and put it in a Map.
    * @param :- Set<Id> salesOrderIdSet.
    * @return :- Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> salesOrderMap . 
    */

    public Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> fetchSalesOrderLineItems(Set<Id> salesOrderIdSet) {
        
        Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> salesOrderMap = new Map<Id,List<SandDV1__Sales_Order_Line_Item__c>>();

        for(SandDV1__Sales_Order_Line_Item__c soli :ServiceFacade.fetchSOLIRecords(salesOrderIdSet)){
            if(salesOrderMap.containskey(soli.SandDV1__Sales_Order__c)) {
                salesOrderMap.get(soli.SandDV1__Sales_Order__c).add(soli);
            }
            else {
                List<SandDV1__Sales_Order_Line_Item__c> sOLIList = new List<SandDV1__Sales_Order_Line_Item__c>();
                sOLIList.add(soli);
                salesOrderMap.put(soli.SandDV1__Sales_Order__c,sOLIList);
            }
        }
        return salesOrderMap;
    }

    /*******************************************************************************************************
    * @description :- Method to update Sales Order Status using Map of salesorder Id and list of Salesorder Line Items.
    * @param :- Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> salesOrderMap.
    * @return :- void . 
    */
    
    public void updateSalesOrder(Map<Id,List<SandDV1__Sales_Order_Line_Item__c>> salesOrderMap) {

        List<SandDV1__Sales_Order__c> salesOrderList = ServiceFacade.fetchSalesOrderRec(salesOrderMap.keyset());

        for(SandDV1__Sales_Order__c sales : salesOrderList) {
            decimal totalUnfilledQuna = 0;
            decimal totalQunantity = 0;
            for(SandDV1__Sales_Order_Line_Item__c so : salesOrderMap.get(sales.Id)) {
                totalUnfilledQuna = totalUnfilledQuna + so.SandDV1__Unfulfilled_Quantity__c;
                totalQunantity = totalQunantity + so.SandDV1__Quantity__c;
            }
            if(totalUnfilledQuna == 0) {
                sales.SandDV1__Status__c =  System.Label.SalesOrderProcessedStatus; 
            }
            else if(totalUnfilledQuna == totalQunantity) {
                sales.SandDV1__Status__c = System.Label.SalesOrderOpenStatus; 
            }
            else if(totalUnfilledQuna != totalQunantity && totalUnfilledQuna > 0) {
                sales.SandDV1__Status__c =  System.Label.SalesOrderPartiallyProcessedStatus; 
            }
        }

        if(!salesOrderList.isEmpty()) {
            List<Schema.SObjectField> salesOrderFieldsList = FieldAccessibilityUtility.fetchSalesOrderFields2();
            SecurityUtils.checkUpdate(SandDV1__Sales_Order__c.SObjectType,salesOrderFieldsList);
            update salesOrderList;
        }
    }

    /*******************************************************************************************************
    * @description :-method to restrict user's to delete SandDV1__Sales_Order_Line_Item__c records. 
    * @param :- List<SandDV1__Sales_Order_Line_Item__c>.
    * @return :- void.
    */
    //
    public void onDelete(List<SandDV1__Sales_Order_Line_Item__c> salesOrderLIList) {
        Set<String> statusSet = ConfigurationService.getStatusSet('SandDV1__Sales_Order__c','Allow_Delete');
        Set<Id> salesOrderLIIdSet = new Set<Id>();
        String ProfileId = UserInfo.getProfileId(); 
         
        
        for(SandDV1__Sales_Order_Line_Item__c salesOrderLineItem : salesOrderLIList) { 
            if(!statusSet.contains(salesOrderLineItem.SandDV1__System_Sales_Order_Status__c)){
                salesOrderLineItem.adderror(+system.Label.Can_tDeleteRecordMessage);
            }
            else {

                salesOrderLIIdSet.add(salesOrderLineItem.Id);
            }

        }
        if(!salesOrderLIIdSet.isEmpty()) {
            ServiceFacade.deleteSalesBenefit(salesOrderLIIdSet);
        }
    }

    
    
}