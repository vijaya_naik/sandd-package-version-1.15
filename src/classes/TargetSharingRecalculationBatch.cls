/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan (ET Marlabs)
* @date july 2015 
* @description This batch class is used to recalculate target sharing.
*/

global with sharing class TargetSharingRecalculationBatch implements Database.Batchable<sObject> {
    
    
    global TargetSharingRecalculationBatch() {}
    
    //fetch all target whose assigned to != null and status != draft. for traget sharing recalculation 
    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<Schema.SObjectField> targetFieldsList = new List<Schema.SObjectField>{
            SandDV1__Target__c.SObjectType.fields.SandDV1__Assigned_To__c,
            SandDV1__Target__c.SObjectType.fields.SandDV1__Status__c
        };
        SecurityUtils.checkRead(SandDV1__Target__c.SObjectType, targetFieldsList);
        return Database.getQueryLocator([SELECT Id,SandDV1__Assigned_To__c,SandDV1__Status__c FROM SandDV1__Target__c 
                                          where SandDV1__Assigned_To__c != null AND SandDV1__Status__c !=: Label.Draft]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Map<ID,SandDV1__Target__c> targetMap = new Map<ID,SandDV1__Target__c>((List<SandDV1__Target__c>) scope);
        List<Target__Share> newTargetShareList = new List<Target__Share>();

        try {
          
            List<Target__Share> oldTargetSharingList = [SELECT Id,ParentId FROM Target__Share WHERE ParentId IN :targetMap.keySet()
                              AND RowCause = :Schema.Target__Share.RowCause.SandDV1__Target_Assignment_Access__c];

            for(SandDV1__Target__c target : targetMap.values()) {
                Target__Share newtargetshare = new Target__Share();
                newtargetshare.ParentId = target.Id;
                newtargetshare.UserOrGroupId = target.SandDV1__Assigned_To__c;
                newtargetshare.AccessLevel = 'Read';
                newtargetshare.RowCause = Schema.Target__Share.RowCause.SandDV1__Target_Assignment_Access__c;
                newTargetShareList.add(newtargetshare);
            }

            if(newTargetShareList.size()>0) {
                if(oldTargetSharingList.size()>0) {
                    //SecurityUtils.checkObjectIsDeletable(Target__Share.SObjectType);
                    delete oldTargetSharingList;
                }
                
                List<Schema.SObjectField> targetShareFieldsList = FieldAccessibilityUtility.fetchTargetShareFields();
                //SecurityUtils.checkInsert(Target__Share.SObjectType,targetShareFieldsList);
                insert newTargetShareList;
            }
        }
        catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
}