/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author Ankita Trehan(ET Marlabs)
* @date   8/21/2015
* @description This class is used as a handler for UpdateRoothPath trigger.
*/

public with sharing class BucketTriggerHandler {
    
    /*******************************************************************************************************
    * @description :-  method is used to Activate the bucket (checkbox and recordtype) on insert of bucket.
    * @param :- List<SandDV1__Bucket__c> 
    * @return :- void.
    */
    public static void onBeforeInsert(List<SandDV1__Bucket__c> newObjects) {
        Id activeRecordTypeID = BucketService.fetchRecordTypeID('Active','SandDV1__Bucket__c');
        for(SandDV1__Bucket__c b : newObjects) {
            b.SandDV1__Active__c = true;
            if(activeRecordTypeID != null) {
                b.RecordTypeId = activeRecordTypeID;
            }
        }
    }
    
    /*******************************************************************************************************
    * @description :- method on after insert of bucket call update Bucket method to update bucket and create group 
            Before performing any operation check bucket hieararchy locked or not.If locked show message otherwise 
            perform operation.
    * @param :- List<SandDV1__Bucket__c>.
    * @return :- void.
    */
    public static void onAfterInsert(List<SandDV1__Bucket__c> newObjects) {
        //try{
            List<SandDV1__Bucket__c> bucketList = new List<SandDV1__Bucket__c>();
            //check bucket hierarchy locked or not. 
            bucketList = checkHierarchyBlock(newObjects);
            if(!bucketList.isempty()) {
               updateBucket(bucketList,'insert');
            }
        /*}
        catch(Exception e){
            for (SandDV1__Bucket__c newbucket : newObjects) {
                newbucket.addError(+System.Label.TriggerError +':-' +e);
            }
        }*/
    }
    
    /*******************************************************************************************************
    * @description :- Call insert group method,insert root id in bucket and update bucket with root path.
    * @param :- List<SandDV1__Bucket__c> bucketlist,String triggeraction. // triggeraction is string specifing insert or update.
    * @return :- void.
    */
    public static void updateBucket(List<SandDV1__Bucket__c> bucketlist,String triggeraction) {
        Set<Id> parentIds = new Set<Id>();
        Set<Id> bucketIds = new Set<Id>();
        List<SandDV1__Bucket__c> updatebucketList = new List<SandDV1__Bucket__c>();

        for(SandDV1__Bucket__c bucket : bucketlist) {
            parentIds.add(bucket.SandDV1__Parent__c);  //set having parent bucket id of current bucket.
            bucketIds.add(bucket.Id); //set having current bucket id.
        }
        
        //parentBuckets map have parent bucket id and record as a value.
        Map<Id,SandDV1__Bucket__c> parentBuckets = new Map<Id,SandDV1__Bucket__c>(BucketService.fetchBucketRecord(parentIds));

        //according to parent id fetch SandDV1__System_Route_Path__c of current bucket.
        for(SandDV1__Bucket__c updatedBucket : BucketService.fetchBucketRecord(bucketIds)) {
            if(updatedBucket.SandDV1__Parent__c != null) {
                updatedBucket.SandDV1__System_Route_Path__c = (((parentBuckets.get(updatedBucket.SandDV1__Parent__c).SandDV1__System_Route_Path__c)!= null)? parentBuckets.get(updatedBucket.SandDV1__Parent__c).SandDV1__System_Route_Path__c: '')  + '/' + updatedBucket.Id;
            }
            else {
                updatedBucket.SandDV1__System_Route_Path__c = '/' + updatedBucket.ID;
            }
            updatebucketList.add(updatedBucket);
        }  

        //if triggeraction = insert then only insert group for that bucket.
        if(triggeraction == 'insert') {
            updatebucketList = insertGroup(updatebucketList);
        }

        //in current bucket store root id according to parent bucket id.
        updatebucketList = bucketRootID(updatebucketList);
        
        if(!updatebucketList.isEmpty()) {
            List<SandDV1__Bucket__c> futureBucketList = new List<SandDV1__Bucket__c>();
            //before calling future method blcok bucket hiearrchy from top level bucket.
            futureBucketList = BucketService.blockBucketHierarchy(updatebucketList,true);
            /*if current bucket is itself a top level bucket (bucket id = system root id) then above 
              method will return bucketlist with system future call = true and we will update bucket with other fields(system root id,system route path etc.)
              if current bucket is not a top level bucket(bucket id != system root id) then above method will return empty list.
              and it will update top level bucket with system future call = true.*/
            if(!futureBucketList.isEmpty()) {
                updatebucketList = futureBucketList;
            }
        }

        //fetch current user session id for http call.
        String sessionID = UserInfo.getSessionId();

        //future method,fetch bucket members and then insert group members in group.
        createfuturegroupmembers(bucketIds,sessionID);
        ConstantsClass.Is_Bucket_Update = false;
        
        List<Schema.SObjectField> bucketFieldsList = FieldAccessibilityUtility.fetchBucketFields();
        SecurityUtils.checkUpdate(SandDV1__Bucket__c.SObjectType, bucketFieldsList);
        update updatebucketList ;
    }
    
    /*******************************************************************************************************
    * @description :- method to insert group and put group id in current bucket.
    * @param :- List<SandDV1__Bucket__c>.
    * @return :- List<SandDV1__Bucket__c>.
    */
    public static List<SandDV1__Bucket__c> insertGroup(List<SandDV1__Bucket__c> newObjects) {
        List<Group> groupList = new List<Group>();
        Map<String,Group> groupMap = new Map<String,Group>();

        for(SandDV1__Bucket__c b : newObjects) {
            Group newgrp = new Group();
            newgrp.Type = 'Regular';
            newgrp.Name = 'Bucketing'+'-'+b.Name;
            newgrp.DeveloperName = b.Id;
            groupList.add(newgrp);
        }

        if(!groupList.isempty()) {
            List<Schema.SObjectField> groupFieldsList = FieldAccessibilityUtility.fetchGroupFields();
            SecurityUtils.checkInsert(Group.SObjectType,groupFieldsList);
            insert groupList;
        }
        
        for(Group g : groupList) {
            groupMap.put(g.DeveloperName,g);
        }
        
        for(SandDV1__Bucket__c b : newObjects) {
            if(!groupMap.isempty()) {
                b.SandDV1__System_Group_ID__c = groupMap.get(b.Id).Id;
            }
        }

        return newObjects;
    }

    /*******************************************************************************************************
    * @description :- method to add parent group members in current group using future call. 
    * @param :- Set<Id> //set of bucket id.
    * @return :- void.
    */
    @Future(callout=true)
    public static void createfuturegroupmembers(Set<Id> bucketId,String sessionID) {
        try {
            String rootBucketID = null;
            if(!bucketID.isEmpty()) {
                rootBucketID = BucketService.hierarchyBucketID(bucketID);
            }
            if(rootBucketID != null) {
                //unlock bucket hierarchy 
               BucketService.callHttpmethodtoUpdateBucket(rootBucketID,sessionID,false);
            }

            //addParentGroupMembers method fetch bucket members and return group member list to insert.
            List<GroupMember> groupMemberList  = BucketService.addParentGroupMembers(bucketId);
            if(!groupMemberList.isempty()) {
                List<Schema.SObjectField> groupMemberFieldsList = FieldAccessibilityUtility.fetchGroupMemeberFields();
                SecurityUtils.checkInsert(GroupMember.SObjectType, groupMemberFieldsList);
                insert groupMemberList;
            }
        }
        catch(Exception e) {
            throw new CustomException(e);
        }
    }

    /*******************************************************************************************************
    * @description :- method to put root id and level index in bucket according to parent bucket.
    * @param :- List<SandDV1__Bucket__c>.
    * @return :- List<SandDV1__Bucket__c>.
    */
    public static List<SandDV1__Bucket__c> bucketRootID(List<SandDV1__Bucket__c> updateBucketList) {
        for(SandDV1__Bucket__c b : updateBucketList) {
            if(b.SandDV1__Parent__c == null) {
                b.SandDV1__System_RootId__c = b.Id;
                b.SandDV1__System_Level_Index__c = 0;
            }

            if(b.SandDV1__Parent__c != null && b.SandDV1__Parent__r.SandDV1__System_RootId__c != null) {
                b.SandDV1__System_RootId__c = b.SandDV1__Parent__r.SandDV1__System_RootId__c;
                if(b.SandDV1__Parent__r.SandDV1__System_Level_Index__c != null) {
                    b.SandDV1__System_Level_Index__c = b.SandDV1__Parent__r.SandDV1__System_Level_Index__c + 1;
                }
            }
        }

        return updateBucketList;
    }

    /*******************************************************************************************************
    * @description :- on update of bucket if user update parent id then according to new parent change complete below hierarchy. 
    * @param :- Map<Id,SandDV1__Bucket__c>,Map<Id,SandDV1__Bucket__c> //new map and old map.
    * @return :- void.
    */
    public static void onUpdate(Map<Id,SandDV1__Bucket__c> newBucketMap,Map<Id,SandDV1__Bucket__c> oldBucketMap,List<SandDV1__Bucket__c> bucketList) {
        Set<Id> bucketID = new Set<Id>();
        Map<Id,Id> accessbucketIDMap = new Map<Id,Id>();

        try {
            for(SandDV1__Bucket__c b : BucketService.fetchBucketRecord(newBucketMap.keyset())) {
                //check here if parent id is changed.
                if(b.SandDV1__Parent__c != oldBucketMap.get(b.Id).SandDV1__Parent__c) {
                    bucketID.add(b.Id);
                }
                if(b.SandDV1__Access_Level__c != oldBucketMap.get(b.Id).SandDV1__Access_Level__c) {
                    accessbucketIDMap.put(b.Id,b.SandDV1__System_Group_ID__c);
                }
            }

            if(!bucketID.isEmpty()) {
              //on change of parent id bucket hierarchy will change.
              BucketService.changeBucketHierarchy(bucketID);
            } 

            if(!accessbucketIDMap.isEmpty()) {
                /*on change of access level fetch all objetc bucket records.delete sharing records and create new sharing
                    update sharing record id in object bucket record.
                */
                List<SandDV1__System_Object_Bucket__c> objectBucketList = BucketService.fetchObjectBucket(accessbucketIDMap.keyset());
                RecordSharing.deleteShare(objectBucketList);
                BucketSharingHandler.onInsert(objectBucketList,false);
            }
        }
        catch(Exception e) {
            for (SandDV1__Bucket__c newbucket : bucketList) {
                newbucket.addError(+System.Label.TriggerError +':-' +e);
            }
        }    
    }

    /*******************************************************************************************************
    * @description :-method to restrict user's to delete bucket records. 
    * @param :- List<SandDV1__Bucket__c>.
    * @return :- void.
    */
    //
    public static void onDelete(List<SandDV1__Bucket__c> bucketList) {
        for(SandDV1__Bucket__c bucket : bucketList) {
            bucket.adderror(+system.Label.Can_tDeleteRecordMessage);
        }
    }

    /*******************************************************************************************************
    * @description:- method is used to check bucket hierarchy is locked or not.
        on insert or update of bucket check bucket at top level have system future call = true or false.
        If system future call is true then means bucket hierachy is locked.
    * @param <param name>:- List<SandDV1__Bucket__c>.
    * @return <return type> :-List<SandDV1__Bucket__c>. 
    */
    public static List<SandDV1__Bucket__c> checkHierarchyBlock(List<SandDV1__Bucket__c> bucketList) {
        Set<Id> bucketRootIdSet = new Set<Id>();
        Set<Id> parentBucketSet = new Set<Id>();
        Map<Id,Boolean> bucketMap = new Map<Id,Boolean>();
        List<SandDV1__Bucket__c> newBucketList = new List<SandDV1__Bucket__c>();
        
        try {
            /*from current bucket list fetch parent bucket id. 
              (here we can't fetch direct system root id becoz it will come after insert in bucket.)
            */
            for(SandDV1__Bucket__c bucket :bucketList) {
                parentBucketSet.add(bucket.SandDV1__Parent__c);
            }

            //from parent bucket id fetch system root id (becoz system root id will be top level id)
            for(SandDV1__Bucket__c bucket :BucketService.fetchBucketRecord(parentBucketSet)) {
                bucketRootIdSet.add(bucket.SandDV1__System_RootId__c);
            }

            //from system root id fetch bucket records and put in map
            for(SandDV1__Bucket__c bucket :BucketService.fetchBucketRecord(bucketRootIdSet)) {
                bucketMap.put(bucket.Id,bucket.SandDV1__System_Future_Call__c);
            }

            // from current bucket list check with the help of BucketMap whether bucket hierarchy is locked or not
            for(SandDV1__Bucket__c bucket :bucketList) {
                if(bucketMap.containskey(bucket.SandDV1__Parent__c) && bucketMap.get(bucket.SandDV1__Parent__c) == false) {
                   newBucketList.add(bucket); 
                }
                else if(bucketMap.containskey(bucket.SandDV1__Parent__c) && bucketMap.get(bucket.SandDV1__Parent__c) == true) {
                    bucket.addError(+System.Label.Error_Message_For_Future_Call);
                }
                else {
                     newBucketList.add(bucket);
                }
            }
            return newBucketList;
        }
        catch(Exception e) {
            newBucketList.clear();
            for (SandDV1__Bucket__c newb : bucketList) {
                newb.addError(+System.Label.TriggerError +':-' +e);
            }
            return newBucketList;
        }
    }
}