/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 07-Oct-2015
* @description This is a Test class of TargetsController
*/
@isTest(seeAllData = false)
private class TargetRedirectControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;
        System.assertEquals(sysAdmin.LastName,'Super');

    }
    @isTest static void test_method_one() {
        
        init();

        System.runAs(sysAdmin){
            Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(sysAdmin);
            TargetRedirectController trc = new TargetRedirectController(sc); 
            trc.redirect();
            Test.stopTest(); 
        }
    }
    
}