/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/21/2015..
* @description This class is used to deactive bucket member using custom button.
*/

public with sharing class BucketMemberDeactivationController {
    
  public String bucketMemberId {get;set;}
  public String bucketId {get;set;}
  public String systemFutureCall {get;set;}
  public String bucketMemberActive {get;set;}

  public BucketMemberDeactivationController(ApexPages.StandardController stdController) {
     
      bucketMemberId = stdController.getId();
      Set<Id> userBucketIDSet = new Set<Id>();
      userBucketIDSet.add(bucketMemberId);
      List<SandDV1__User_Bucket_Member__c> bucketMemberList = BucketService.fetchUserBucketRecord(userBucketIDSet);
      List<SandDV1__Bucket__c> bucketRootList;
      
      if(!bucketMemberList.isEmpty()){
      
          bucketId = bucketMemberList[0].SandDV1__Bucket__c;
          bucketMemberActive = bucketMemberList[0].SandDV1__System_Active__c ? 'true' : 'false';
          if(bucketMemberList[0].SandDV1__Bucket__r.SandDV1__System_Record_Id__c != bucketMemberList[0].SandDV1__Bucket__r.SandDV1__System_RootId__c){
          
              bucketRootList = BucketService.fetchBucketById(bucketId);
              systemFutureCall = bucketRootList[0].SandDV1__System_Future_Call__c ? 'true' : 'false';
          }
          else {
              systemFutureCall = bucketMemberList[0].SandDV1__Bucket__r.SandDV1__System_Future_Call__c ? 'true' : 'false';
          }
          
      }
  }
  
  
  /*******************************************************************************************************
  * @description :- method is used to call deactivation method from bucket service class to deactivate bucket member.
  * @param :- null.
  * @return :- null. 
  */
  @RemoteAction
  public static void deactivateBucketMember(String bucketMemberId) {

      BucketService.deactiveBucketMember(bucketMemberId);
  }

}