/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a service Facade
*/


public with sharing class ServiceFacade {
    
    
    public static Boolean isConditionApplicable(SandDV1__Scheme_Condition__c schemeCondition, SandDV1__Sales_Order__c salesOrder, 
                SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,Product2 productObj,Account accountObj){
        return SchemeConditionService.isConditionApplicable(schemeCondition, salesOrder, salesOrderLineItem,productObj,accountObj);
    } 


    public static Map<String, SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItemsMap(Set<String> salesOrderLineItemIdSet){
        return SalesOrderLineItemService.getSalesOrderLineItemsMap(salesOrderLineItemIdSet);
    }


    public static Map<String, SandDV1__Scheme_Benefit__c> getSchemeBenefitsMap(Set<String> schemeBenefitsIdSet){
        return SchemeBenefitService.getSchemeBenefitsMap(schemeBenefitsIdSet);
    }


    public static SandDV1__Sales_Order_Line_Item__c getSalesOrderLineItem(SandDV1__Scheme_Benefit__c schemeBenefit, String salesOrderItemBenefitId, String salesOrderId){
        return SalesOrderLineItemService.getSalesOrderLineItem(schemeBenefit, salesOrderItemBenefitId, salesOrderId);
    }


    public static List<SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItemForBenefits(Set<String> salesOrderItemBenefitIdSet){
        return SalesOrderLineItemService.getSalesOrderLineItemForBenefits(salesOrderItemBenefitIdSet);
    }

    public static List<SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItems(String salesOrderId){

        return SalesOrderLineItemService.getSalesOrderLineItems(salesOrderId);
    }

/*---------------------------- Sales Order --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description .Method to fetch Sales Order
    * @param <param name> .accId - Account Id , limitRecords-Number of records that needs to be retrived
    * @return <return type> .List of Sales Order object
    */
    
    public static List<SandDV1__Sales_Order__c> fetchSalesOrders(String accId,Integer limitRecords){

        return SalesOrderService.fetchSalesOrderList(accId, limitRecords);
    }

/*---------------------------- Period --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/


    /*******************************************************************************************************
    * @description .Method to fetch Periods based on a string
    * @param <param name> startWith
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriods(String startWith){

 
        return PeriodService.fetchPeriods(startWith);
    }


    /*******************************************************************************************************
    * @description .Method to fetch Periods
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriods(){

        return PeriodService.fetchPeriods(); 
    }


    /*******************************************************************************************************
    * @description .Method to Periods using master Id
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriodsByMasterId(String masterId){

        return PeriodService.fetchPeriodsByMasterId(masterId); 
    }


    /*******************************************************************************************************
    * @description .Method to put periods into map
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static Map<Id,SandDV1__Period__c> periodListToMap(String periodId){

        return PeriodService.periodListToMap(periodId); 
    }

/*---------------------------- Account --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description method to retrive accounts based on provided string
    * @param accName - account name to query
    * @return List of account . 
    */
    
    public static List<Account> fetchAccounts(String accName){

        return AccountService.fetchAccounts(accName);
    }

    /*******************************************************************************************************
    * @description method to retrive account based on Id
    * @param accId - account Id to query
    * @return List of accounts 
    */
    
    public static List<Account> fetchAccountById(String accountId){

        return AccountService.fetchAccountById(accountId);
    }


    /*******************************************************************************************************
    * @description method to retrive accounts 
    * @param 
    * @return List of account . 
    */
    
    public static List<Account> fetchAccounts(){

        return AccountService.fetchAccounts();
    }

/*---------------------------- Shipping Address --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description : method to retrive shipping based on account Id and query string
    * @param : accId - account Id,shipName - ship name to query
    * @return List of shipping address . 
    */
   

    public static List<SandDV1__Shipping_Address__c> fetchShippingByAccId(String accId){


        return ShippingAddressService.fetchShippingByAccId(accId); 

    }

/*---------------------------- Pricebook Entry --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/
    
    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id
    * @param : priceBookId - priceBook Id to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntries(String priceBookId){

        return PricebookEntryService.fetchPriceBookEntries(priceBookId);  
        
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id.Limited records are retrived.
    * @param : priceBookId - priceBook Id to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchLimitedPriceBookEntries(String priceBookId){

        return PricebookEntryService.fetchLimitedPriceBookEntries(priceBookId);  
        
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id and Product name
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductName(String priceBookId,String productName){

        return PricebookEntryService.fetchPriceBookEntriesByProductName(priceBookId,productName);  
        
    }

    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id and Product code
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductCode(String priceBookId,String productCode){

        return PricebookEntryService.fetchPriceBookEntriesByProductCode(priceBookId,productCode);
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id
    * @param : productIdList - List of product Ids, pricebookId - Id of the pricebook
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductId(List<String> productIdList,String priceBookId){

        return PricebookEntryService.fetchPriceBookEntriesByProductId(productIdList,priceBookId);
    }


/*---------------------------- Pricebook --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook name
    * @param : priceBookName - priceBookName name to query
    * @return List of Pricebook2. 
    */
    
    public static List<Pricebook2> fetchPriceBooks(String priceBookName){

        return Pricebook2Service.fetchPriceBooks(priceBookName);
    }
    
     /* -----Scheme-----*/
    public static List<SandDV1__Scheme__c> fetchAllSchemeRecords(Set<Id> schemeIdSet){
        return SchemeService.fetchAllSchemeRecords(schemeIdSet);
    }
     /* -----Scheme Condition -----*/
    public static List<SandDV1__Scheme_Condition__c> fetchSchemeConditionRec(String schemeID){
        return SchemeConditionService.fetchSchemeConditionRec(schemeID);
    }
    
    /* -----Scheme Benefit Range -----*/
    public static List<SandDV1__Scheme_Benefit_Range__c> fetchSchemeBenefitRange(String schemeID){
        return SchemeBenefitRangeService.fetchSchemeBenefitRange(schemeID);
    }
    
    /* -----Scheme Benefit -----*/
    public static List<SandDV1__Scheme_Benefit__c> fetchSchemeBenefit(Set<Id> benfitRangeId){
        return SchemeBenefitService.fetchSchemeBenefit(benfitRangeId);
    }
    
    /* -- Sales Order for Delivery Order --*/
    public static List<SandDV1__Sales_Order__c> fetchSalesOrderRecord(Id salesOrderId) {
        return SalesOrderService.fetchSalesOrderRecord (salesOrderId);
    }
    public static List<SandDV1__Sales_Order__c> fetchSalesOrderRec(Set<Id> salesOrderId) {
        return SalesOrderService.fetchSalesOrderRec(salesOrderId);
    }
    
    /* -- Sales OrderLine Item  for Delivery Order --*/
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOrderLineItemRecordsGreaterQuantity(Id salesOrderId) {
        return SalesOrderLineItemService.fetchSalesOrderLineItemRecordsGreaterQuantity(salesOrderId);
    }
    
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchOrderLineItemRecords(Set<Id> salesOrderIdSet){
        return SalesOrderLineItemService.fetchOrderLineItemRecords(salesOrderIdSet);
    }
    
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOrderLineItemRecords(Id salesOrderId) {
        return SalesOrderLineItemService.fetchSalesOrderLineItemRecords(salesOrderId);
    }
    
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSOLIRecords(Set<Id> salesOrderID) {
        return SalesOrderLineItemService.fetchSOLIRecords(salesOrderID);
    }
    
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOLIRecords(Set<Id> salesOrderLineItemID) {
        return SalesOrderLineItemService.fetchSalesOLIRecords(salesOrderLineItemID);
    }
    
    /* --  Delivery Order Line Item  for Delivery Order --*/
    public static List<SandDV1__Delivery_Order_Line_Item__c> fetchDeliveryOrderLineItemRecords(Id deliveryOrderID) {
        return DeliveryOrderLineItemService.fetchDeliveryOrderLineItemRecords(deliveryOrderID);
    }
    
    public static List<SandDV1__Delivery_Order_Line_Item__c> fetchDeliveryOLIRecords(Set<Id> deliveryOrderID) {
        return DeliveryOrderLineItemService.fetchDeliveryOLIRecords(deliveryOrderID);
    }
    
     /* --  SalesOrderLine Item benefit  --*/ 
    public static List<SandDV1__Sales_Order_Item_Benefit__c> fetchSalesOrderItemBenefits(Set<Id> salesOrderLIID) {
        return SalesOrderItemBenefitService.fetchSalesOrderItemBenefits(salesOrderLIID);
    }
    
    public static void deleteSalesBenefit(Set<Id> salesOrderLIIdSet) {
        SalesOrderItemBenefitService.deleteSalesBenefit(salesOrderLIIdSet);
    }
    
    public static List<SandDV1__Sales_Order_Item_Benefit__c> fetchSalesOrderItemBenefitRecords(Set<Id> schemeIdSet) {
        return SalesOrderItemBenefitService.fetchSalesOrderItemBenefitRecords(schemeIdSet);
    }
    
    /* --- Product2 ---*/
    public static List<Product2> fetchProductList(String prodName){
        return Product2Service.fetchProductList(prodName);
    }
    
    /* --- SandDV1__OnBoardingDocumentSetting__c ---*/
    public static List<SandDV1__OnBoardingDocumentSetting__c> fetchOnBoardingDocSetting(){
        return ConfigurationService.fetchOnBoardingDocSetting();
    }
    
    /* --- OnBoardingStatusColor__c---*/
    public static List<SandDV1__OnBoardingStatusColor__c> fetchOnBoardingStatusColor(){
        return ConfigurationService.fetchOnBoardingStatusColor();
    }
    
    /* --- OnBoarding_Document__c---*/
    public static List<SandDV1__OnBoarding_Document__c> fetchAccountDocs(Set<Id> accountID){
        return OnBoardingService.fetchAccountDocs(accountID);
    }

/*---------------------------- Delivery Order --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description .Method to fetch Delivery Order
    * @param <param name> .accId - Account Id
    * @return <return type> .List of Delivery Order object
    */
  
    public static List<SandDV1__Delivery_Order__c> fetchDeliveryOrders(String accId,Integer limitRecords){

        return DeliveryOrderService.fetchDeliveryOrders(accId,limitRecords);     
    }

/*---------------------------- Contact --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    public static List<Contact> fetchContacts(String accId,Integer limitRecords){

        return ContactService.fetchContacts(accId,limitRecords);
    }

/*---------------------------- Task --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description .Method to fetch Task
    * @param <param name> .accId - Account Id
    * @return <return type> .List of Task object
    */
  
    public static List<Task> fetchTasks(String accId){

        return TaskService.fetchTasks(accId);
    }


/*---------------------------- Complaint --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description .Method to fetch complaints
    * @param <param name> .accId - Account Id
    * @return <return type> .List of Complaint object
    */
  
    public static List<SandDV1__Complaint__c> fetchComplaints(String accId){

        return ComplaintService.fetchComplaints(accId);
    }

/*---------------------------- Visit --------------------------------------------*/
/*------------------------------*****----------------------------------------------*/

    /*******************************************************************************************************
    * @description .Method to fetch Visit
    * @param <param name> .accId - Account Id
    * @return <return type> .List of Visit object
    */

    public static List<SandDV1__Visit__c> fetchVisits(String accId,Integer limitRecords){

        return VisitService.fetchVisits(accId, limitRecords);  
    }

    /*******************************************************************************************************
    * @description .Method to fetch Visit
    * @param <param name> .visitId - Visit Id
    * @return <return type> .List of Visit object
    */

    public static List<SandDV1__Visit__c> fetchVisitsById(String visitId){

        return VisitService.fetchVisitsById(visitId);
    }

    /*******************************************************************************************************
    * @description .Method to fetch Visits
    * @param <param name> .visitDate - query based on the visit date
    * @return <return type> .List of Visit object
    */

    public static List<SandDV1__Visit__c> fetchVisits(Date visitDate){

        return VisitService.fetchVisits(visitDate);  
    }
    
    /* --- SystemObjectBucketService ---*/
    public static List<SandDV1__System_Object_Bucket__c> fetchBucketRecords(Set<Id> bucketIdSet) {
        return SystemObjectBucketService.fetchBucketRecords(bucketIdSet);
    }
}