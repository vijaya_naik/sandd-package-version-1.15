/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeDomain {
    public class LineItemWrapper{
        public String productId;
        public String productCode;
        public String productCategory;
        public Integer lineItemQuantity; 
        public String lineItemId;
    }
    
    
    public class OrderWrapper{
        public String accountId;
        public String terittory;
        public Decimal orderAmount;
    }
    
    
    public class ApplicableSchemeWrapper{
        public Boolean isApplicableForLineItem;
        public String schemeId;
        public String lineItemId;
        public String productId;
        public Integer lineItemQuantity;
    }


    public class ApplicableSchemeBenefitWrapper{
        public String lineItemId;
        public String lineItemName;
        public String productId;
        public String productName;
        public List<SchemeBenefitWrapper> schemeBenefitList = new List<SchemeBenefitWrapper>();
    }


    public class SchemeBenefitWrapper{
        public Boolean isSelected;
        public SandDV1__Scheme_Benefit__c schemeBenefit = new SandDV1__Scheme_Benefit__c();
    }


    public class ApplicableSchemesWrapper{
        public SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        public SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        //modified for Product and Account
        public Product2 productObj = new Product2();
        public Account accountObj = new Account();
        //end
        public List<SandDV1__Scheme__c> schemesList = new List<SandDV1__Scheme__c>();
    }


    public class SchemeBenefitRangeWrapper{
        public SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        public SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        public List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList = new List<SandDV1__Scheme_Benefit_Range__c>();
    }


    public class SchemeBenefitsWrapperV1{
        public SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        public SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        public List<SandDV1__Scheme_Benefit__c> schemeBenefitsList = new List<SandDV1__Scheme_Benefit__c>();
    }
    

    public class SchemeBenefitsWrapperV2{
        public SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        public SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        public List<BenefitWrapper> schemeBenefitsList = new List<BenefitWrapper>();
    }


    public class BenefitWrapper {
        public SandDV1__Scheme_Benefit__c schemeBenefit = new SandDV1__Scheme_Benefit__c();
        public Boolean isSelected = false;
        public Boolean isSelectedOld = false;
    }
    
    public class schemeWrapperAll {
        public schemeWrapper schemeRecord {get;set;}
        public SandDV1__Scheme_Condition__c sCriteriaRecord {get;set;}
        public List<SandDV1__Scheme_Condition__c> sAssignmentRecord {get;set;}
        public List<benfitRangeWrapper> sBenefitRangeRecord {get;set;}
        public List<benfitWrapper> sBenefitRecord {get;set;}

        public schemeWrapperAll() {
            this.schemeRecord               = new schemeWrapper();
            this.sCriteriaRecord            = new SandDV1__Scheme_Condition__c();
            this.sAssignmentRecord          = new List<SandDV1__Scheme_Condition__c>();
            this.sBenefitRangeRecord        = new List<benfitRangeWrapper>();
            this.sBenefitRecord             = new List<benfitWrapper>();
        }
    }

    public class benfitRangeWrapper {
        public String min;
        public String max;
        public String range;
        public String uniqueId;
        public String recordId;
    }

    public class benfitWrapper {
        public String productId;
        public String productName;
        public String range;
        public String uniqueId;
        public Decimal value;
        public String selectedSCType;
        public String selectedLineItem;
        public String recordId;
    }

    public class schemeWrapper {
        public SandDV1__Scheme__c schemeObj;
        public String schemeType;
        public String validFromDate;
        public String expireOnDate;
        public boolean IsActive;
    }

    public class criteriaValueWrapper {
        public String valueName;
        public String valueId;
        public String datatype;
    }
    
    public class criteriaWrapper {
        public String schemeType;
        public String objectApi;
        public String fieldApi;
        public String returnType;
        public String criteriavalue;
        public Boolean IsAssignment;
        public String activeApi;
        public String activeApiValue ;
        public Boolean isPickList;
    }
    
    /*******************************************************************************************************
    * @description :Wrapper class to store Scheme Id after Save with Return Message
    * @param <param name> .
    * @return <return type> . 
    */
    public class SchemeMessageInfo {      
       public String  schemeID{get;set;}
       public String  schemeReturnMessage{get;set;}
    }
    
    /*******************************************************************************************************
    * @description :Wrapper class to store Scheme Id after Save with Return Message
    * @param <param name> .
    * @return <return type> . 
    */
    public class ApplySchemeMessageInfo {      
       public String  applySchemeID{get;set;}
       public String  applySchemeReturnMessage{get;set;}
    }
}