/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class FieldAccessibilityUtility2 {
    
    public FieldAccessibilityUtility2() {}
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Condition__c object fields which are using while upsert of SandDV1__Scheme_Condition__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeConditionFields() {
        
        List<Schema.SObjectField> schemeConditionFieldsList = new List<Schema.SObjectField>();
        schemeConditionFieldsList.addAll(FieldAccessibilityUtility.fetchSchemeConditionFields());
        schemeConditionFieldsList.add(SandDV1__Scheme_Condition__c.SObjectType.fields.Name);
        return schemeConditionFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Benefit__c object fields which are using while upsert of SandDV1__Scheme_Benefit__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeBenefitields() {
        
        List<Schema.SObjectField> schemeBenefitFieldsList = new List<Schema.SObjectField>();
        schemeBenefitFieldsList.addAll(FieldAccessibilityUtility.fetchSchemeBenefitields());
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.Name);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__r.SandDV1__Min__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__r.SandDV1__Max__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Product__r.Name);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__r.SandDV1__Min__c);
        return schemeBenefitFieldsList;
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Benefit__c object fields which are using while upsert of SandDV1__Scheme_Benefit__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeBenefitields2() {
        
        List<Schema.SObjectField> schemeBenefitFieldsList = new List<Schema.SObjectField>();
        schemeBenefitFieldsList.addAll(FieldAccessibilityUtility.fetchSchemeBenefitields());
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.Name);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Product__r.Name);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__System_Product_Name__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Range__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Scheme_Name__c);
        return schemeBenefitFieldsList;
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Benefit__c object fields which are using while upsert of SandDV1__Scheme_Benefit__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeBenefitields3() {
        
        List<Schema.SObjectField> schemeBenefitFieldsList = new List<Schema.SObjectField>();
        schemeBenefitFieldsList.addAll(FieldAccessibilityUtility2.fetchSchemeBenefitields2());
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Scheme_Benefit_Range__r.SandDV1__Scheme__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Level__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Product__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Type__c);
        schemeBenefitFieldsList.add(SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Value__c);
        return schemeBenefitFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of Account object fields which are using while upsert of Account record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchAccountFields() {
        List<Schema.SObjectField> accountFieldsList = new List<Schema.SObjectField>();
        accountFieldsList.add(Account.SObjectType.fields.SandDV1__Credit_Limit__c);
        accountFieldsList.add(Account.SObjectType.fields.SandDV1__Block_Order_Creation__c);
        return accountFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of SandDV1__Sales_Order__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderFields() {
        List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>();
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.Name);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_No_of_Sales_Order__c );
        return salesOrderFieldsList;
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Benefit_Range__c object fields which are using while upsert of SandDV1__Scheme_Benefit_Range__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeBenefitRangeFields() {
        
        List<Schema.SObjectField> schemeBenefitRangeFieldsList = new List<Schema.SObjectField>();
        schemeBenefitRangeFieldsList.addAll(FieldAccessibilityUtility.fetchSchemeBenefitRangeFields());
        schemeBenefitRangeFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.Name);
        schemeBenefitRangeFieldsList.add(SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Scheme__c);            
        return schemeBenefitRangeFieldsList;
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Scheme_Benefit_Range__c object fields which are using while upsert of SandDV1__Scheme_Benefit_Range__c record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeFields2() {
        
        List<Schema.SObjectField> schemeFieldsList = new List<Schema.SObjectField>{
        SandDV1__Scheme__c.SObjectType.fields.Name, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Scheme_Name__c,
        SandDV1__Scheme__c.SObjectType.fields.SandDV1__Description__c,
        SandDV1__Scheme__c.SObjectType.fields.SandDV1__Type__c, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Valid_From__c, 
        SandDV1__Scheme__c.SObjectType.fields.SandDV1__Exclusive_Scheme__c, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Expires_On__c,
        SandDV1__Scheme__c.SObjectType.fields.SandDV1__Budget_Limit__c, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Status__c};          
       return schemeFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of SandDV1__Sales_Order__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderFields2() {
        List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>();
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Order_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Discount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Amount__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Discount__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Date__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__PO_Reference__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Price_Book__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Status__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Remarks__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Address_Name__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_City__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Country__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_State_Province__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Street__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Zip_Postal_Code__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_No_of_Sales_Order__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Order__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Discount__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Net_Price__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Part__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__System_Related_to_SalesOrderLineItem__c  );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Order_Item_Benefit__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Fulfilled_Quantity__c  );
        return salesOrderFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of SandDV1__Sales_Order__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderFields3() {
        List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>();
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Date__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Approval_Status__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__No_of_Parts__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__PO_Reference__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__PO_Status__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Scheme_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Status__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Discount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Order_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Discount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_City__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Country__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_State_Province__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Street__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Zip_Postal_Code__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Address_Name__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_No_of_Sales_Order__c );
        return salesOrderFieldsList;
    }
    
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of SandDV1__Sales_Order__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderFields4() {
        List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>();
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Date__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Discount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Order_Amount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Total_Line_Item_Discount__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Status__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_Date__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_No_of_Sales_Order__c);
        return salesOrderFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of SandDV1__Sales_Order__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderFields5() {
        List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>();
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Date__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Price_Book__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Address_Name__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_City__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Country__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_State_Province__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Street__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Zip_Postal_Code__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__System_No_of_Sales_Order__c);
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Part__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Price_Book__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Quantity__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Type__c );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Unit_Price__c  );
        salesOrderFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__UOM__c );
        return salesOrderFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of Scheme object fields which are using while upsert of Scheme  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSchemeFields() {
        List<Schema.SObjectField> schemeFieldsList = new List<Schema.SObjectField>{
            SandDV1__Scheme__c.SObjectType.fields.Name, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Status__c, SandDV1__Scheme__c.SObjectType.fields.SandDV1__Has_Primary_Condition__c, 
                SandDV1__Scheme_Condition__c.SObjectType.fields.Name, SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__FieldApi__c,
                SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__ObjectApi__c, SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__System_Value__c,
                SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__Parameter__c, SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__DataType__c,
                SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__Scheme__c, SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__Value__c};
                    return schemeFieldsList;
    }
    
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order_Line_Item__c object fields which are using while upsert of SandDV1__Sales_Order_Line_Item__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderLineItem() {
        List<Schema.SObjectField> salesOrderLinetItemFieldsList = new List<Schema.SObjectField>();
        salesOrderLinetItemFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Order__c);
        salesOrderLinetItemFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Quantity__c);
        salesOrderLinetItemFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__New_Quantity__c);
        salesOrderLinetItemFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Price__c);
        salesOrderLinetItemFieldsList.add(SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Net_Price__c );
        return salesOrderLinetItemFieldsList;
    }
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Expense__c object fields which are using while upsert of SandDV1__Expense__c  record.
It is used in Scheme Service Class.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchExpenseFields() {
        List<Schema.SObjectField> expenseFieldsList = new List<Schema.SObjectField>();
        expenseFieldsList.add(SandDV1__Expense__c.SObjectType.fields.Name);
        expenseFieldsList.add(SandDV1__Expense__c.SObjectType.fields.SandDV1__Currency__c);
        expenseFieldsList.add(SandDV1__Expense__c.SObjectType.fields.SandDV1__Date__c);
        expenseFieldsList.add(SandDV1__Expense__c.SObjectType.fields.SandDV1__Status__c);
        return expenseFieldsList;
    }
    
    
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Expense_Line_Item__c object fields which are using while upsert of SandDV1__Expense_Line_Item__c  record.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchExpenseLineItemFields() {
        List<Schema.SObjectField> expenseLineItemFieldsList = new List<Schema.SObjectField>();
        expenseLineItemFieldsList.add(SandDV1__Expense_Line_Item__c.SObjectType.fields.Name);
        expenseLineItemFieldsList.add(SandDV1__Expense_Line_Item__c.SObjectType.fields.SandDV1__Amount__c);
        expenseLineItemFieldsList.add(SandDV1__Expense_Line_Item__c.SObjectType.fields.SandDV1__Date__c);
        expenseLineItemFieldsList.add(SandDV1__Expense_Line_Item__c.SObjectType.fields.SandDV1__Exchange_Rate__c);
        expenseLineItemFieldsList.add(SandDV1__Expense_Line_Item__c.SObjectType.fields.SandDV1__Has_Bills__c);
        return expenseLineItemFieldsList;
    }
    
	
/*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order_Item_Benefit__c object fields which are using while upsert of SandDV1__Sales_Order_Item_Benefit__c  record.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderItemBenefitFields() {
    
        List<Schema.SObjectField> salesOrderItemBenefitFieldList = new List<Schema.SObjectField>{
            SandDV1__Sales_Order_Item_Benefit__c.sobjectType.fields.SandDV1__Sales_Order_Line_Item__c, 
            SandDV1__Sales_Order_Item_Benefit__c.sobjectType.fields.SandDV1__Scheme_Benefit__c,
            SandDV1__Sales_Order_Item_Benefit__c.sobjectType.fields.SandDV1__Order_Item_Benefit_Combination__c};
       return salesOrderItemBenefitFieldList;   
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order_Item__c object fields which are using while upsert of SandDV1__Sales_Order_Item__c  record.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderItemFields() {
    
        List<Schema.SObjectField> salesOrderItemFieldList = new List<Schema.SObjectField>{
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.Name, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Part__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__System_Product_Name__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Quantity__c, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Unit_Price__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Sales_Price__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Sales_Order__c};
       return salesOrderItemFieldList;   
    }
    
    /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order_Item__c object fields which are using while upsert of SandDV1__Sales_Order_Item__c  record.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderItemFields3() {
    
        List<Schema.SObjectField> salesOrderItemFieldList = new List<Schema.SObjectField>{
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__System_Product_Code__c, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Part__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Type__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Net_Price__c, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Sales_Price__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Part__r.Name,
            Product2.sobjectType.fields.SandDV1__Part__r.ProductCode,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Fulfilled_Quantity__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Unfulfilled_Quantity__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Price_Book__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Price_Book__r.Name,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Quantity__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Sales_Order__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Unit_Price__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__UOM__c};
       return salesOrderItemFieldList;   
    }
    
     /*******************************************************************************************************
* @description :- Method to return list of SandDV1__Sales_Order_Item__c object fields which are using while upsert of SandDV1__Sales_Order_Item__c  record.
* @param :- null
* @return :- List<Schema.SObjectField>.
*/
    public static List<Schema.SObjectField> fetchSalesOrderItemFields2() {
    
        List<Schema.SObjectField> salesOrderItemFieldList = new List<Schema.SObjectField>{
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.Name, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Part__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__System_Product_Name__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Quantity__c, 
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Discount__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Discount_Percentage__c,
            SandDV1__Sales_Order_Line_Item__c.sobjectType.fields.SandDV1__Sales_Order__c};
       return salesOrderItemFieldList;   
    }
}