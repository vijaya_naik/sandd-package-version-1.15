/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Test Class for AccountTriggerHelper.
*/

@isTest(seeAllData = false)
private class AccountTriggerHelperTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Account testAccount;
    static SandDV1__BlockOrderCreation__c blockOrderCreation;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;


    }

    @isTest static void test_method_one() {
        
        init();
        
        System.runAs(testUser){
            Test.startTest();

                blockOrderCreation = new SandDV1__BlockOrderCreation__c();
                blockOrderCreation.Name = 'BlockOrderCreation';
                blockOrderCreation.SandDV1__Value__c = true;
                insert blockOrderCreation;
                System.assertEquals(blockOrderCreation.SandDV1__Value__c,true);

                testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
                insert testAccount;

            Test.stopTest();
        }
    }
    
    
}