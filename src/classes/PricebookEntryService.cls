/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a service Facade
*/
public with sharing class PricebookEntryService {
  
  /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook name
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductName(String priceBookId,String productName){

        List<Schema.SObjectField> pricebookEntryFieldsList = FieldAccessibilityUtility.fetchPricebookEntryFields();
        SecurityUtils.checkRead(PricebookEntry.SObjectType, pricebookEntryFieldsList);
        String startsWith = productName + '%';

        return [SELECT Id,Name,Pricebook2Id,Pricebook2.Name,Product2Id,Product2.Name,Product2.SandDV1__UOM__c,ProductCode,UnitPrice 
        FROM PricebookEntry WHERE Pricebook2Id =:priceBookId AND Product2.Name LIKE :startsWith ORDER BY Product2.Name];  
        
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook code
    * @param : priceBookId - priceBook Id to query, productName - Product Name to query, productCode - Product Code to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductCode(String priceBookId,String productCode){

        List<Schema.SObjectField> pricebookEntryFieldsList = FieldAccessibilityUtility.fetchPricebookEntryFields();
        SecurityUtils.checkRead(PricebookEntry.SObjectType, pricebookEntryFieldsList);
        String startsWith = productCode + '%';

        return [SELECT Id,Name,Pricebook2Id,Pricebook2.Name,Product2Id,Product2.Name,Product2.SandDV1__UOM__c,ProductCode,UnitPrice  
        FROM PricebookEntry WHERE Pricebook2Id =:priceBookId AND ProductCode LIKE :startsWith ORDER BY ProductCode]; 

    }

    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook id
    * @param : productIdList - List of product Ids, pricebookId - Id of the pricebook
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntriesByProductId(List<String> productIdList,String priceBookId){
        
        List<Schema.SObjectField> pricebookEntryFieldsList = FieldAccessibilityUtility.fetchPricebookEntryFields();
        SecurityUtils.checkRead(PricebookEntry.SObjectType, pricebookEntryFieldsList); 
        return [SELECT Id,Name,Pricebook2Id,Pricebook2.Name,Product2Id,Product2.Name,Product2.SandDV1__UOM__c,ProductCode,UnitPrice 
        FROM PricebookEntry WHERE Pricebook2Id =:priceBookId AND Product2Id IN :productIdList]; 
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id
    * @param : priceBookId - priceBook Id to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchPriceBookEntries(String priceBookId){
        List<Schema.SObjectField> pricebookEntryFieldsList = FieldAccessibilityUtility.fetchPricebookEntryFields();
        SecurityUtils.checkRead(PricebookEntry.SObjectType, pricebookEntryFieldsList);
        return [SELECT Id,Name,Pricebook2Id,Pricebook2.Name,Product2Id,Product2.Name,Product2.SandDV1__UOM__c,ProductCode,UnitPrice 
        FROM PricebookEntry WHERE Pricebook2Id =:priceBookId ORDER BY Product2.Name];  
        
    }


    /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook Id.Limited records are retrive
    * @param : priceBookId - priceBook Id to query
    * @return List of PricebookEntry. 
    */
    
    public static List<PricebookEntry> fetchLimitedPriceBookEntries(String priceBookId){
        List<Schema.SObjectField> pricebookEntryFieldsList = FieldAccessibilityUtility.fetchPricebookEntryFields();
        SecurityUtils.checkRead(PricebookEntry.SObjectType, pricebookEntryFieldsList);
        return [SELECT Id,Name,Pricebook2Id,Pricebook2.Name,Product2Id,Product2.Name,Product2.SandDV1__UOM__c,ProductCode,UnitPrice 
        FROM PricebookEntry WHERE Pricebook2Id =:priceBookId ORDER BY Product2.Name LIMIT 10];  
        
    }
}