/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class DeliveryOrderLineItemService {
    
    /*******************************************************************************************************
* @description :- Method to fetch SandDV1__Delivery_Order_Line_Item__c List from Delivery Order Id.
* @param :- Id.
* @return :- List<SandDV1__Delivery_Order_Line_Item__c>. 
*/
    public static List<SandDV1__Delivery_Order_Line_Item__c> fetchDeliveryOrderLineItemRecords(Id deliveryOrderID) {
        List<Schema.SObjectField> deliveryOLIFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderLineItemFields();
        SecurityUtils.checkRead(SandDV1__Delivery_Order_Line_Item__c.SObjectType, deliveryOLIFieldsList);  
        return [Select Id,SandDV1__Part__c,SandDV1__Part__r.Name,SandDV1__System_DeliveryOrder_Status__c,SandDV1__System_Unfilled_Quantity__c,SandDV1__Part__r.ProductCode,SandDV1__System_SalesOrderLine_Item__c,SandDV1__Part__r.Description,
                SandDV1__Part__r.SandDV1__Price__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Type__c,SandDV1__Delivery_Order__c,SandDV1__Net_Amount__c,SandDV1__Sales_Price__c,SandDV1__Unit_Price__c,
                SandDV1__UOM__c from SandDV1__Delivery_Order_Line_Item__c where SandDV1__Delivery_Order__c =: deliveryOrderID];
    }
    
    /*******************************************************************************************************
* @description :- Method to fetch SandDV1__Delivery_Order_Line_Item__c List from Set of Delivery Order Id.
* @param :- Set<Id>.
* @return :- List<SandDV1__Delivery_Order_Line_Item__c>. 
*/
    public static List<SandDV1__Delivery_Order_Line_Item__c> fetchDeliveryOLIRecords(Set<Id> deliveryOrderID) {
        List<Schema.SObjectField> deliveryOLIFieldsList = FieldAccessibilityUtility.fetchDeliveryOrderLineItemFields2();
        SecurityUtils.checkRead(SandDV1__Delivery_Order_Line_Item__c.SObjectType, deliveryOLIFieldsList);  
        return [Select Id,SandDV1__Part__c,SandDV1__Part__r.Name,SandDV1__System_DeliveryOrder_Status__c,SandDV1__System_Unfilled_Quantity__c,SandDV1__Part__r.ProductCode,SandDV1__System_SalesOrderLine_Item__c,SandDV1__Part__r.Description,
                SandDV1__Part__r.SandDV1__Price__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Type__c,SandDV1__Delivery_Order__c,SandDV1__Net_Amount__c,SandDV1__Sales_Price__c,SandDV1__Unit_Price__c,
                SandDV1__UOM__c from SandDV1__Delivery_Order_Line_Item__c where SandDV1__Delivery_Order__c IN: deliveryOrderID];
    }   
}