/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 07-Jan-2016
* @description This is a service class for Contact.
*/

public with sharing class ContactService {
	
	/*******************************************************************************************************
  * @description .Method to fetch Contacts
  * @param <param name> .accId - Account Id, limitRecords
  * @return <return type> .Contact object
  */
  
  public static List<Contact> fetchContacts(String accId,Integer limitRecords){
      
	List<Schema.SObjectField> contactFieldsList = FieldAccessibilityUtility.fetchContactFields();
    SecurityUtils.checkRead(Contact.SObjectType, contactFieldsList); 
    return [SELECT Id,FirstName,Phone,Email,AccountId,SandDV1__Designation__c FROM Contact WHERE AccountId =:accId LIMIT :limitRecords];
  }
}