/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeConditionService {
    /*******************************************************************************************************
    * @description :- Method to check applicable schemes according to scheme condition.
    * @param :- SandDV1__Scheme_Condition__c schemeCondition, SandDV1__Sales_Order__c salesOrder, SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,
                                        Product2 productObj,Account accountObj
    * @return :- Boolean. 
    */
    
    public static Boolean isConditionApplicable(SandDV1__Scheme_Condition__c schemeCondition, SandDV1__Sales_Order__c salesOrder, SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,
                                        Product2 productObj,Account accountObj) {
        Boolean isConditionApplicable = false;
        
        if(schemeCondition.SandDV1__ObjectApi__c == 'Product2') {
            isConditionApplicable = schemeCondition.SandDV1__Value__c == productObj.get(schemeCondition.SandDV1__FieldApi__c);
        }
        else if(schemeCondition.SandDV1__ObjectApi__c == 'Account') {
          String[] tempStr = new list<String>();
          if(schemeCondition.SandDV1__Value__c.contains(',')) {  
               tempStr = (schemeCondition.SandDV1__Value__c).split(',');
          }
          else {
              tempStr.add(schemeCondition.SandDV1__Value__c);
          }
          for(integer i=0;i<tempStr.size();i++) {
              if(tempStr[i] == accountObj.get(schemeCondition.SandDV1__FieldApi__c)) {
                  isConditionApplicable = true;
              }
          }
        }
        
        else if(schemeCondition.SandDV1__ObjectApi__c == 'SandDV1__Bucket__c') {
            Set<Id> bucketSet = new Set<Id>();
            String[] tempStr = new list<String>();
            if(schemeCondition.SandDV1__System_Value__c.contains(',')) {  
                tempStr = (schemeCondition.SandDV1__System_Value__c).split(',');
            }
            else {
                tempStr.add(schemeCondition.SandDV1__System_Value__c);
            }
            for(integer i=0;i<tempStr.size();i++) {
                bucketSet.add(tempStr[i]);
            }
            for(SandDV1__System_Object_Bucket__c sysObj :ServiceFacade.fetchBucketRecords(bucketSet)) {
                if(sysObj.SandDV1__Account__c == accountObj.Id) {
                    isConditionApplicable = true;
                }
            }
           
        }
        //end
        else if(schemeCondition.SandDV1__ObjectApi__c == 'SandDV1__Sales_Order_Line_Item__c') {
            isConditionApplicable = schemeCondition.SandDV1__Value__c == salesOrderLineItem.get(schemeCondition.SandDV1__FieldApi__c);
        }
        return isConditionApplicable;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch scheme condition records according to scheme id
    * @param :- String scheme id
    * @return :- List<SandDV1__Scheme_Condition__c>
    */
    public static List<SandDV1__Scheme_Condition__c> fetchSchemeConditionRec(String schemeID) {
        List<Schema.SObjectField> visitFieldsList = FieldAccessibilityUtility2.fetchSchemeConditionFields();
        SecurityUtils.checkRead(SandDV1__Scheme_Condition__c.SObjectType, visitFieldsList);
        return [Select Id,Name,SandDV1__Parameter__c,SandDV1__DataType__c,SandDV1__Scheme__c,SandDV1__ObjectApi__c,SandDV1__FieldApi__c,SandDV1__System_Scheme_Type__c,SandDV1__Value__c,SandDV1__System_Value__c 
                from SandDV1__Scheme_Condition__c where SandDV1__Scheme__c =: schemeID];
    }
}