public with sharing class TargetRedirectController {

	public TargetRedirectController(ApexPages.StandardController stdController) {
     
    }


    public PageReference redirect(){
     	   	
		PageReference pageRef = new PageReference('/apex/TargetsMainPageV2');
    	return pageRef;
	}  

  
}