/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 07-Jan-2016
* @description This is a service class for Task.
*/
public with sharing class TaskService {

	/*******************************************************************************************************
	* @description .Method to fetch Task
	* @param <param name> .accId - Account Id
	* @return <return type> .List of Task object
	*/
  
	public static List<Task> fetchTasks(String accId){
		List<Schema.SObjectField> taskFieldsList = FieldAccessibilityUtility.fetchTaskFields();
        SecurityUtils.checkRead(Task.SObjectType, taskFieldsList);  
		return [SELECT AccountId,ActivityDate,Id,IsClosed,Status,Subject,SandDV1__System_Activity_Date__c,Priority FROM Task 
	                WHERE IsClosed = false AND AccountId = :accId ORDER BY ActivityDate ASC NULLS FIRST ];
	}
}