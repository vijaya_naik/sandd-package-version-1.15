/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 
* @description This class is for SalesOrderEditOverridePage
*/

public with sharing class SalesOrderEditOverrideController {

	public String returnURL {get;set;}
    public String soId {get;set;}
    public String soStatus {get;set;}
    public String appStatus {get;set;}

	public SalesOrderEditOverrideController(ApexPages.StandardController con) {
		
        soId = con.getId();
        System.PageReference pr = con.cancel();
        returnURL = pr.getUrl();
        List<Schema.SObjectField> salesOrderFieldList = new List<Schema.SObjectField>{SandDV1__Sales_Order__c.sobjectType.fields.SandDV1__Status__c, SandDV1__Sales_Order__c.sobjectType.fields.SandDV1__Approval_Status__c};
        SecurityUtils.checkRead(SandDV1__Sales_Order__c.SObjectType, salesOrderFieldList);
        SandDV1__Sales_Order__c salesOrder = [SELECT Id,SandDV1__Status__c,SandDV1__Approval_Status__c FROM SandDV1__Sales_Order__c WHERE Id=:soId];
        soStatus = salesOrder.SandDV1__Status__c;
        appStatus = salesOrder.SandDV1__Approval_Status__c;
	}
}