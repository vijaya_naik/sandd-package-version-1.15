/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public  with sharing class BucketViewController {
    
    public List<SandDV1__Bucket__c> AllBuckets {get; set;}
    
    public  BucketViewController() {
    }
    
    //Node class 
    public class Node {
        public Node() {
            children = new List<Node>();
        }
        public Node(Id nodeIdentifier, String nodeName, Id parentIdentifier) {
            nodeId = nodeIdentifier;
            name = nodeName;
            parentId = parentIdentifier;
            children = new List<Node>();
        }
        public Id nodeId {get; set;}
        public string name {get ; set ;}
        public List<Node> children {get; set;}
        public Id parentId {get; set;}
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Active Parent Buckets which are at level 0 (root bucket).
    * @param :- null.
    * @return :- List<SandDV1__Bucket__c>. 
    */
    @RemoteAction
    public static List<SandDV1__Bucket__c> fetchParentBuckets() {
        return BucketService.fetchParentBuckets();
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Active Parent Buckets which are at level 0 (root bucket).
    * @param :- null.
    * @return :- List<SandDV1__Bucket__c>. 
    */
    @RemoteAction
    public static Node getTree(String rootID) {
        List<SandDV1__Bucket__c> allBuckets = BucketService.getAllBuckets(rootID);
        return constructTree(allBuckets);
    }
    
    /*******************************************************************************************************
    * @description :- Method to create node record (as per node class) using bucket list(having parent bucket nd child buckets).
    * @param :- List<SandDV1__Bucket__c> buckets.
    * @return :- Node.
    */
    private static Node constructTree(List<SandDV1__Bucket__c> buckets) {
        Node root = new Node();
        Map<Id,Node> nodes = new Map<Id,Node>();
        SandDV1__Bucket__c tempBucket = null;
        Node parent = null;
        Node tempNode = null;
        for(SandDV1__Bucket__c bucket : buckets) {
            nodes.put(bucket.Id, new Node(bucket.Id, bucket.Name, bucket.SandDV1__Parent__c));
        }
        for(Node nd : nodes.values()) {
            if(nd.parentId != null) {
                parent = nodes.get(nd.parentId);
                parent.children.add(nd);
            }
            else {
                root = nd;
            }
        }
        return root;
    }

}