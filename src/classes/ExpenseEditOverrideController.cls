/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 21-Mar-2016
* @description This class is to override the edit functionality of Expense page.
*/
public with sharing class ExpenseEditOverrideController {

	public String returnURL {get;set;}
    public String eId {get;set;}
    public String eStatus {get;set;}

	public ExpenseEditOverrideController(ApexPages.StandardController con) {
		eId = con.getId();
        System.PageReference pr = con.cancel();
        returnURL = pr.getUrl();
        List<Schema.SObjectField> expenseFieldsList = new List<Schema.SObjectField>{SandDV1__Expense__c.SObjectType.fields.SandDV1__Status__c};
        SecurityUtils.checkRead(SandDV1__Expense__c.SObjectType, expenseFieldsList);  
        SandDV1__Expense__c expense = [SELECT Id, SandDV1__Status__c FROM SandDV1__Expense__c WHERE Id=:eId];
        eStatus = expense.SandDV1__Status__c;
	}
}