/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 03-Nov-2015
* @description This is a service class for Customer360ViewController.
*/
public with sharing class Customer360ViewService {
  

  /*******************************************************************************************************
  * @description .Method to update visit
  * @param <param name> .Visit Object
  * @return <return type> .Visit object
  */
  
  public static SandDV1__Visit__c updateVisit(SandDV1__Visit__c visit){

    try{
      
      List<Schema.SObjectField> visitFieldsList = FieldAccessibilityUtility.fetchVisitFields();
      SecurityUtils.checkUpdate(SandDV1__Visit__c.SObjectType,visitFieldsList);
      update visit;
      return visit;
    }
    catch(Exception e) {

        throw new CustomException(e.getMessage());
    }
    
  }


  /*******************************************************************************************************
  * @description .Method to assign check In/Out
  * @param <param name> .
  * @return <return type> .
  */
  
  public static Customer360ViewDomain.CustomerWrapper assignCheckInCheckOut(List<SandDV1__Visit__c> visitList, 
                              Customer360ViewDomain.CustomerWrapper cusWrap){


        if(visitList[0].SandDV1__Status__c != 'In Progress' && visitList[0].SandDV1__Status__c != 'Completed'){
            cusWrap.isCheckIn = true;

        }
        else if(visitList[0].SandDV1__Status__c == 'In Progress') {

            cusWrap.isCheckOut = true;
        }

    return cusWrap;  
  }


  /*******************************************************************************************************
  * @description .Method to assign values to CustomerWrapper
  * @param <param name> .
  * @return <return type> .
  */
  
  public static Customer360ViewDomain.CustomerWrapper assignComplaintValues(List<SandDV1__Complaint__c> complaintList, 
                                  Customer360ViewDomain.CustomerWrapper cusWrap){
        for(SandDV1__Complaint__c com:complaintList){

          if(com.SandDV1__Status__c == 'Escalated'){

              cusWrap.complaintWrap.escalated += 1;
          }

          if(com.SandDV1__Priority__c == 'High'){

              cusWrap.complaintWrap.high += 1;
          }

          if(com.SandDV1__Priority__c == 'Normal'){

              cusWrap.complaintWrap.medium += 1;
          }

          if(com.SandDV1__Priority__c == 'Low'){

              cusWrap.complaintWrap.low += 1;
          }
      }

    return cusWrap;  
  }

  /*******************************************************************************************************
  * @description .Method to assign values to CustomerWrapper
  * @param <param name> .
  * @return <return type> .
  */
  
  public static Customer360ViewDomain.CustomerWrapper assignActivitieValues(List<Task> openActivityList, 
                                  Customer360ViewDomain.CustomerWrapper cusWrap){
        for(Task tas:openActivityList){

            if(system.today() > tas.ActivityDate){

                cusWrap.openActivityWrap.overDue += 1;
            }

            if(tas.Priority == 'High'){

                cusWrap.openActivityWrap.high += 1;
            }

            if(tas.Priority == 'Normal'){

                cusWrap.openActivityWrap.medium += 1;
            }

            if(tas.Priority == 'Low'){

                cusWrap.openActivityWrap.low += 1;
            }
        }

    return cusWrap;  
  }

}