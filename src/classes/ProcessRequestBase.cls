/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any. The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author ET Marlabs
* @date 18 Aug 2015
* @description This interface is basically to be implemented as a 
*              custom data type having @invocablevaribles so that they can be exposed to 
*              a Lightning Process as parameters
*/

global interface ProcessRequestBase {
   
   
   //Implement this method to return the Id of the record in process
   Id getRecordId();
   
   String getCustomParamList();
   
}