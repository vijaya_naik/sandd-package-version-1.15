/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 08-Mar-2016
* @description This class is for Expense mobile/desktop page
*/
public with sharing class ExpenseController {


    /*******************************************************************************************************
    * @description -method to retrive record types of SandDV1__Expense_Line_Item__c
    * @param accName - 
    * @return List of SandDV1__Expense_Line_Item__c record type names . 
    */
    
    @RemoteAction
    public static List<ExpenseLIRTWrap> fetchExpenseLIRecordTypes(){

        List<ExpenseLIRTWrap> expenseLIRTList = new List<ExpenseLIRTWrap>();
        List<Schema.SObjectField> expenseItemFieldsList = new List<Schema.SObjectField>{SandDV1__Expense_Line_Item__c.SObjectType.fields.Name};
        SecurityUtils.checkRead(SandDV1__Expense_Line_Item__c.SObjectType, expenseItemFieldsList);  
        List<RecordType> recordTypesList = [SELECT Id,Name FROM RecordType WHERE sobjecttype = 'SandDV1__Expense_Line_Item__c'];

        for(RecordType recordType:recordTypesList){

            ExpenseLIRTWrap expenseWrap = new ExpenseLIRTWrap();

            expenseWrap.Id = recordType.Id;
            expenseWrap.Name = recordType.Name;
            expenseLIRTList.add(expenseWrap);
        }
        return expenseLIRTList;
    }

    public class ExpenseLIRTWrap {

        public String Id {get;set;}
        public String Name {get;set;}
    }


    /*******************************************************************************************************
    * @description -Method to fetch visits
    * @param -Visit date
    * @return -List of visits. 
    */
    
    @RemoteAction
    public static List<SandDV1__Visit__c> fetchVisits(String searchVisitDate){

        Date visitDate = Date.valueOf(searchVisitDate); 

        return ServiceFacade.fetchVisits(visitDate);

    }


    /*******************************************************************************************************
    * @description -Method to save expense
    * @param -
    * @return -
    */
    
    @RemoteAction
    public static SandDV1__Expense__c saveExpense(SandDV1__Expense__c expense,List<SandDV1__Expense_Line_Item__c> expenseLineItemList){

        Savepoint sp = Database.setSavepoint();
        try {

            List<Schema.SObjectField> expenseFieldsList = FieldAccessibilityUtility2.fetchExpenseFields();
            SecurityUtils.checkInsert(SandDV1__Expense__c.SObjectType, expenseFieldsList);
            SecurityUtils.checkUpdate(SandDV1__Expense__c.SObjectType, expenseFieldsList);
            Database.UpsertResult sr = Database.upsert(expense,false);
            if(sr.isSuccess()){
                
                for(SandDV1__Expense_Line_Item__c expLI:expenseLineItemList){

                    if(expLI.SandDV1__Expense__c == null){
                        expLI.SandDV1__Expense__c = sr.getId();
                    }
                }
                
                List<Schema.SObjectField> expenseLIFieldsList = FieldAccessibilityUtility2.fetchExpenseLineItemFields();
                SecurityUtils.checkInsert(SandDV1__Expense_Line_Item__c.SObjectType,expenseLIFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Expense_Line_Item__c.SObjectType,expenseLIFieldsList);
                upsert expenseLineItemList;
                return expense;
            }
            else {

                throw new CustomException('Error while inserting Expense');
            }
        }
        catch(Exception e) {

            Database.rollback(sp);
            throw new CustomException(e.getMessage());
        }

    }

    /*******************************************************************************************************
    * @description -Method to fetch expense
    * @param -
    * @return -
    */
    
    @RemoteAction
    public static SandDV1__Expense__c fetchExpense(String expenseId){
        List<Schema.SObjectField> expenseFieldsList = FieldAccessibilityUtility.fetchExpenseFields();
        SecurityUtils.checkRead(SandDV1__Expense__c.SObjectType, expenseFieldsList);   
        return [SELECT Id,Name,SandDV1__Status__c,SandDV1__Date__c,(SELECT Id,SandDV1__Amount__c,SandDV1__Date__c,SandDV1__Expense__c,SandDV1__Visit__c,SandDV1__Visit__r.Name,SandDV1__Visit__r.SandDV1__Account__r.Name,RecordTypeId,RecordType.Name,RecordType.Id FROM SandDV1__Expense_Line_Items__r) FROM SandDV1__Expense__c WHERE Id =: expenseId LIMIT 1];
    }

}