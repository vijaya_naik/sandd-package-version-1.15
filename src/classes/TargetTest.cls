/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan (ET Marlabs).
* @date August 2015.
* @description This is test class.
*/

@isTest
private class TargetTest {
    
    @isTest static void testTagetSharing() {

        List<Target__Share> insertTargetShareList = new List<Target__Share>();
        List<SandDV1__Period__c> periodList = new List<SandDV1__Period__c>();
        List<SandDV1__Target__c> targetList = new List<SandDV1__Target__c>();
        Map<Integer,SandDV1__Period__c> periodMap = new Map<Integer,SandDV1__Period__c>();

         //Initalize Batch Class
        TargetSharingRecalculationBatch targetsharingcls = new TargetSharingRecalculationBatch();
 
        //create dummy user record
        User dummyUser = InitializeTest.createUser('standarduser1a@testorg.com');
        insert dummyUser;
        System.assertEquals(dummyUser.UserName,'standarduser1a@testorg.com');

        //create dummy user record
        User dummyUser1 = InitializeTest.createUser('standarduser1b@testorg.com');
        insert dummyUser1;

        User dummyUser2 = InitializeTest.createUser('standarduser1c@testorg.com');
        insert dummyUser2;
        System.assertEquals(dummyUser2.UserName,'standarduser1c@testorg.com');

        //create dummy period 
        for (Integer i=0;i<=2;i++) {
            Integer enddate = i+1;
            SandDV1__Period__c dummyperiod = InitializeTest.createTargetPeriod(i,true);
            System.assertEquals(dummyperiod.SandDV1__End_Date__c,system.today()+enddate);
            periodList.add(dummyperiod ) ;
        }
        insert periodList;
        
        for(integer i=0;i<periodList.size();i++) {
            periodMap.put(i,periodList[i]);
        }
        
        //create dummy target records
        for (Integer i=0;i<=2;i++) {
            String status ;
            Id assignedto =null;
            if(i<1) {
                status = Label.Draft;
                assignedto = dummyUser2.Id;
            }
            else if(i==1) {
                status = Label.Accepted;
                assignedto = dummyUser1.Id;
            }
            else if(i==2) {
                status = Label.Assigned;
                assignedto = dummyUser.Id;
            }

            SandDV1__Target__c dummytarget = InitializeTest.createTarget(periodMap.get(i).Id,true,
                status,assignedto);
            targetList.add(dummytarget);
        }
        insert targetList;

        for (SandDV1__Target__c t : targetList) {
            if(t.SandDV1__Status__c == Label.Assigned) {
                t.SandDV1__Status__c = Label.Accepted;
            }
            if(t.SandDV1__Status__c == Label.Draft) {
                t.SandDV1__Status__c = Label.Assigned;
            } 
            if(t.SandDV1__Status__c == Label.Accepted) {
               t.SandDV1__Assigned_To__c = dummyUser2.Id;
            }
            if(t.SandDV1__Status__c != 'Draft' && t.SandDV1__Assigned_To__c != null) {
                Target__Share targetshare = InitializeTest.createTargetShare(t.Id,t.SandDV1__Assigned_To__c,'Read');
                insertTargetShareList.add(targetshare);
            }
        }   
           
        update targetList;
        insert insertTargetShareList;
        Test.startTest();
        
        // Invoke the Batch class
        String jobId = Database.executeBatch(targetsharingcls);
        Test.stopTest();

          
    }
    
    
}