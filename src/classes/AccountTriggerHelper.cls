/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Helper Class for AccountTrigger.
*/
public with sharing class AccountTriggerHelper {
	
	/*-- SINGLETON PATTERN --*/
    private static AccountTriggerHelper instance;
    public static AccountTriggerHelper getInstance() {
        if (instance == null) {
            instance = new AccountTriggerHelper();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description : Method to execute before insertion of account. Check custom setting value and update
    				 BLock Order Creation in account.
    * @param : List<Account> newAccounts
    * @return : null
    */

	public void onBeforeInsert(List<Account> newAccounts) {

		SandDV1__BlockOrderCreation__c blockOrderCreation = SandDV1__BlockOrderCreation__c.getValues('BlockOrderCreation');

		if(blockOrderCreation != null){
			for(Account acc:newAccounts){

				acc.SandDV1__Block_Order_Creation__c = blockOrderCreation.SandDV1__Value__c;
			}
			
		}
	}
}