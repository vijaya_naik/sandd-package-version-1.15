/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeController {

    public SchemeController(ApexPages.StandardController controller) {}

    public SchemeController() {}
    
     //Method return minimum size ofSchemeCriteriaValueSize list size for pagination 
    public static String getSchemeCriteriaValueSize (){
        return JSON.serialize(ConstantsClass.SchemeCriteriaValueSize );
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Type Values from Type field in SandDV1__Scheme__c
    * @param :- null.
    * @return :- List<String>.
    */ 
    @RemoteAction
    public static List<String> selectSchemeType() {
        List<String> schemeTypeList = new List<String>();
        SecurityUtils.checkRead(SandDV1__Scheme__c.sobjectType, new List<Schema.SObjectField>{SandDV1__Scheme__c.SObjectType.fields.SandDV1__Type__c});
        Schema.DescribeFieldResult fieldResult = SandDV1__Scheme__c.SandDV1__Type__c.getDescribe();
           List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();   
           for(Schema.PicklistEntry pL : pickListValues) {
              schemeTypeList.add(pL.getValue());
           }       
        return schemeTypeList;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Criteria
    *  @param :- null.
    * @return :- List<SchemeDomain.criteriaWrapper>.
    */
    @RemoteAction
    public static List<SchemeDomain.criteriaWrapper> getCriteriaList() {
        List<SchemeDomain.criteriaWrapper> CriteriaList = getCriteria();
        return CriteriaList;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Criteria using selected criteria.
    *  @param :- String //selectedcriteriavalue.
    * @return :- SchemeDomain.criteriaWrapper.
    */
    @RemoteAction
    public static SchemeDomain.criteriaWrapper getValue(String selectedcriteriavalue) {
        SchemeDomain.criteriaWrapper newwrap = new SchemeDomain.criteriaWrapper();
        for(SchemeDomain.criteriaWrapper cw : getCriteria()){
            if(cw.criteriavalue == selectedcriteriavalue){
                newwrap = cw;
            }
        }
        return newwrap;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch values(from product,Account etc object depending upon criteria selected).
    *  @param :- SchemeDomain.criteriaWrapper cwrap,String textValue.
    * @return :- List<SchemeDomain.criteriaValueWrapper>.
    */

    @RemoteAction
    public static List<SchemeDomain.criteriaValueWrapper> fetchCriteriaValue(SchemeDomain.criteriaWrapper cwrap,String textValue) {
        List<sobject> sobjectList = new List<sobject>();
        Set<String> valueSet = new Set<String>();
        List<SchemeDomain.criteriaValueWrapper> criteriaValueList = new List<SchemeDomain.criteriaValueWrapper>();
        
        if(cwrap.isPickList == true) {
            List<String> lstPickvals=new List<String>();
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(cwrap.objectApi );//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
            SecurityUtils.checkRead(sobject_type, new List<Schema.SObjectField>{field_map.get(cwrap.fieldApi)});
            List<Schema.PicklistEntry> pick_list_values = field_map.get(cwrap.fieldApi).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
            for (Schema.PicklistEntry pL : pick_list_values) { //for all values in the picklist list
              if(textValue != null) {
                  if((pL.getValue()).startsWithIgnoreCase(textValue)) {
                        SchemeDomain.criteriaValueWrapper val = new SchemeDomain.criteriaValueWrapper();
                        val.valueName = String.valueof(pL.getValue());
                        val.valueId = String.valueof(pL.getValue());
                        val.datatype = cwrap.returnType;
                        criteriaValueList.add(val);
                  }
              }
              else {
                    SchemeDomain.criteriaValueWrapper val = new SchemeDomain.criteriaValueWrapper();
                    val.valueName = String.valueof(pL.getValue());
                    val.valueId = String.valueof(pL.getValue());
                    val.datatype = cwrap.returnType;
                    criteriaValueList.add(val);
              }
           } 
        }
        else {
            textValue = textValue+'%';
            String query ;
            if(cwrap.activeApi != '' && cwrap.activeApi != null) {
                query = 'Select  Id,' + cwrap.fieldApi + ' from ' + cwrap.objectApi + '  Where '  + cwrap.fieldApi + ' like: XXX AND ' + cwrap.activeApi + '=' + cwrap.activeApiValue + ' order By ' + cwrap.fieldApi;
            }
            else {
                query = 'Select  Id,' + cwrap.fieldApi + ' from ' + cwrap.objectApi + '  Where '  + cwrap.fieldApi + ' like: XXX order By ' + cwrap.fieldApi;
            }
            sobjectList = Database.query(generateQueryString('textValue', query));
            for(sobject so :sobjectList) {
                if(!valueSet.contains(String.valueof((String)so.get(cwrap.fieldApi)))) {
                SchemeDomain.criteriaValueWrapper val = new SchemeDomain.criteriaValueWrapper();
                        val.valueName = String.valueof((String)so.get(cwrap.fieldApi));
                        val.datatype = cwrap.returnType;
                        valueSet.add(val.valueName);
                    if(cwrap.fieldApi == 'Name') {
                        val.valueId = String.valueof((Id)so.get('Id'));
                    }
                    else{
                        val.valueId = String.valueof((String)so.get(cwrap.fieldApi));
                    }
                criteriaValueList.add(val);
                }
            }
        }
        return criteriaValueList;
    }

    public static String generateQueryString(String textValue, String soqlString) {
        return soqlString.replace('XXX',textValue);
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benfit Values
    * @param :- null.
    * @return :- List<String>
    */
    @RemoteAction
    public static List<String> getSchemeBenefitTypePicklist() {
        List<String> schemeBenefitTypePicklist= new List<String>();
        SecurityUtils.checkRead(SandDV1__Scheme_Benefit__c.sobjectType, new List<Schema.SObjectField>{SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Type__c});
        Schema.DescribeFieldResult fieldResult = SandDV1__Scheme_Benefit__c.SandDV1__Type__c.getDescribe();
           List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();   
           for(Schema.PicklistEntry pL : pickListValues)
           {
              schemeBenefitTypePicklist.add(pL.getValue());
           }       
        return schemeBenefitTypePicklist;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benfit Levels
    * @param :- null.
    * @return :- List<String>
    */
    @RemoteAction
    public static List<String> getSchemeBenefitLevels() {
        List<String> schemeBenefitTypePicklist= new List<String>();
        SecurityUtils.checkRead(SandDV1__Scheme_Benefit__c.sobjectType, new List<Schema.SObjectField>{SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Level__c});
        Schema.DescribeFieldResult fieldResult = SandDV1__Scheme_Benefit__c.SandDV1__Level__c.getDescribe();
           List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();   
           for(Schema.PicklistEntry pL : pickListValues)
           {
              schemeBenefitTypePicklist.add(pL.getValue());
           }       
        return schemeBenefitTypePicklist;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Information of Scheme Criteria and Value
    * @param :- null.
    * @return :- SandDV1__Scheme_Condition__c
    */
    @RemoteAction
    public static SandDV1__Scheme_Condition__c fetchSchemeCriteriaInfo() {
        SandDV1__Scheme_Condition__c scondition = new SandDV1__Scheme_Condition__c();
        return scondition;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch information of Scheme Assignment and value
    * @param :- null.
    * @return :- List<SandDV1__Scheme_Condition__c>
    */
    @RemoteAction
    public static List<SandDV1__Scheme_Condition__c> fetchAssignmentCriteriaInfo() {
        List<SandDV1__Scheme_Condition__c> sconditionList = new List<SandDV1__Scheme_Condition__c>();
        SandDV1__Scheme_Condition__c sc = new SandDV1__Scheme_Condition__c();
        sconditionList.add(sc);
        return sconditionList;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch information of Scheme Assignment and value
    * @param :- null.
    * @return :- SchemeDomain.schemeWrapper
    */
    @RemoteAction
    public static SchemeDomain.schemeWrapper fetchSchemeInfo() {
        SandDV1__Scheme__c s = new SandDV1__Scheme__c();
        SchemeDomain.schemeWrapper scheme = new SchemeDomain.schemeWrapper();
        scheme.schemeObj = s;
        scheme.validFromDate = String.valueof(System.today());
        scheme.expireOnDate = String.valueof(System.today());
        scheme.schemeType = 'Select Type';
        scheme.IsActive = false;
        return scheme;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch products using product name.
    * @param :- String prodName
    * @return :- List<Product2>.
    */
    @RemoteAction
    public static List<Product2> fetchProductList(String prodName) {
        return ServiceFacade.fetchProductList(prodName);
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme records.
    * @param :- String schemeId.
    * @return :- SchemeDomain.schemeWrapperAll.
    */
    @RemoteAction 
    public static SchemeDomain.schemeWrapperAll fetchAllSchemeRecords(String schemeId){
        Map<String,SchemeDomain.criteriaWrapper> criteriamap = new Map<String,SchemeDomain.criteriaWrapper>();
        Set<Id> benfitSet = new Set<Id>();
        SchemeDomain.schemeWrapperAll newSchemeWrap = new SchemeDomain.schemeWrapperAll();

        if(schemeId == '' || schemeId == null) {
            newSchemeWrap.schemeRecord = fetchSchemeInfo();
            newSchemeWrap.sCriteriaRecord = fetchSchemeCriteriaInfo();
            newSchemeWrap.sAssignmentRecord = fetchAssignmentCriteriaInfo();
        }
        else{
            newSchemeWrap = SchemeService.fetchAllSchemeRecords(schemeId);
            
        }
        return newSchemeWrap;
    }

    /*******************************************************************************************************
    * @description :- Method to Save Scheme,SchemeCondition,Scheme Benefit Range,Scheme Benefit records.
    * @param :- SchemeDomain.schemeWrapper schemeRecord,SandDV1__Scheme_Condition__c schemeConditionRecord,
                                List<SandDV1__Scheme_Condition__c> assignmentCriteriaList,List<SchemeDomain.benfitRangeWrapper> benfitRangeList,
                                List<SchemeDomain.benfitWrapper> benefitList,List<SchemeDomain.benfitRangeWrapper> deleteschemeBenefitList,List<SandDV1__Scheme_Condition__c> deletedAssignmentScheme,
                                SandDV1__Scheme_Condition__c deleteSchemeCriteria.
    * @return :- String .
    */
    @RemoteAction 
    public static String Save(SchemeDomain.schemeWrapper schemeRecord,SandDV1__Scheme_Condition__c schemeConditionRecord,
                                List<SandDV1__Scheme_Condition__c> assignmentCriteriaList,List<SchemeDomain.benfitRangeWrapper> benfitRangeList,
                                List<SchemeDomain.benfitWrapper> benefitList,List<SchemeDomain.benfitRangeWrapper> deleteschemeBenefitList,
                                List<SandDV1__Scheme_Condition__c> deletedAssignmentScheme,SandDV1__Scheme_Condition__c deleteSchemeCriteria) {
       
       return SchemeService.SaveScheme(schemeRecord,schemeConditionRecord,assignmentCriteriaList,benfitRangeList,benefitList,deleteschemeBenefitList,deletedAssignmentScheme,deleteSchemeCriteria);
    }

    /*******************************************************************************************************
    * @description :- Method to fetch criteria values from custom setting and put in wrapper class.
    * @param :- null.
    * @return :- List<SchemeDomain.criteriaWrapper>.
    */
    public static List<SchemeDomain.criteriaWrapper> getCriteria() {
        List<SchemeDomain.criteriaWrapper> criteriaList = new List<SchemeDomain.criteriaWrapper>();
     
        for(Scheme_Criteria_Values__mdt meta : ConfigurationService.fetchSchemeCriteriaMetadate()){
            SchemeDomain.criteriaWrapper wrap = new SchemeDomain.criteriaWrapper();
            wrap.objectApi = meta.SandDV1__ObjectApi__c;
            wrap.fieldApi = meta.SandDV1__FieldApi__c;
            wrap.returnType = meta.SandDV1__ReturnType__c;
            wrap.criteriavalue = meta.SandDV1__CriteriaValue__c;
            wrap.IsAssignment = meta.SandDV1__IsAssignment__c;
            wrap.activeApi = meta.SandDV1__ActiveAPI__c;
            wrap.activeApiValue = meta.SandDV1__ActiveApiValue__c;
            wrap.isPickList = meta.SandDV1__IsPickList__c;
            criteriaList.add(wrap);
        }
        return criteriaList;
    }
}