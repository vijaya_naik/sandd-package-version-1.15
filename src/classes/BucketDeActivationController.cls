/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/21/2015..
* @description This class is used to deactive bucket using custom button.
*/

public with sharing class BucketDeActivationController {
  
  public String bucketId {get;set;}
  public String systemFutureCall {get;set;}
  public String bucketActive {get;set;}

  public BucketDeActivationController(ApexPages.StandardController stdController) {
     
      bucketId = stdController.getId();
      List<SandDV1__Bucket__c> bucketList = BucketService.fetchBucketById(bucketId);
      if(!bucketList.isEmpty()){
          //systemFutureCall = bucketList[0].SandDV1__System_Future_Call__c;
          //bucketActive = bucketList[0].SandDV1__Active__c; 
          systemFutureCall = bucketList[0].SandDV1__System_Future_Call__c ? 'true' : 'false';
          bucketActive = bucketList[0].SandDV1__Active__c ? 'true' : 'false';
      }
  }
  
  
  /*******************************************************************************************************
  * @description :- method is used to call deactivation method from bucket service class and return page to current bucket id..
  * @param :- null.
  * @return :- Boolean. 
  */
  @RemoteAction
  public static Boolean deActivationCall(String bucketId) {

      Set<Id> bucketSet = new Set<Id>();
      bucketSet.add(bucketId);
      BucketService.executeDeActiveBucket(bucketSet);
      return true;
  }

}