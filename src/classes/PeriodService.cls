/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 25-Nov-2015
* @description This is a service class for Period
*/
public with sharing class PeriodService {
	
	/*******************************************************************************************************
    * @description .Method to fetch Periods based on a string
    * @param <param name> startWith
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriods(String startWith){
		
        List<Schema.SObjectField> periodFieldsList = FieldAccessibilityUtility.fetchPeriodFields2();
        SecurityUtils.checkRead(SandDV1__Period__c.SObjectType, periodFieldsList);  
        String startsWithValue = startWith + '%';
        return [SELECT Id, Name,SandDV1__System_Start_and_End_Date__c,SandDV1__Start_Date__c,SandDV1__End_Date__c FROM SandDV1__Period__c WHERE Name LIKE :startsWithValue];
    }


    /*******************************************************************************************************
    * @description .Method to fetch Periods
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriods(){
		List<Schema.SObjectField> periodFieldsList = FieldAccessibilityUtility.fetchPeriodFields4();
        SecurityUtils.checkRead(SandDV1__Period__c.SObjectType, periodFieldsList); 
        return [SELECT Id, Name,SandDV1__System_Start_and_End_Date__c,SandDV1__End_Date__c,SandDV1__Start_Date__c,CreatedDate FROM SandDV1__Period__c 
        WHERE SandDV1__Active__c = true AND SandDV1__End_Date__c > Today ORDER BY CreatedDate]; 
    }


    /*******************************************************************************************************
    * @description .Method to fetch Periods using master Id
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriodsByMasterId(String masterId){
		List<Schema.SObjectField> periodFieldsList = FieldAccessibilityUtility.fetchPeriodFields3();
        SecurityUtils.checkRead(SandDV1__Period__c.SObjectType, periodFieldsList); 
        return [SELECT Id, Name,SandDV1__System_Start_and_End_Date__c,SandDV1__Master_Period__c,SandDV1__Start_Date__c,SandDV1__End_Date__c,SandDV1__Active__c FROM SandDV1__Period__c 
        WHERE SandDV1__Master_Period__c = :masterId AND SandDV1__Active__c = true];
    }


    /*******************************************************************************************************
    * @description .Method to fetch Periods by Id
    * @param <param name> 
    * @return <return type> .List of periods
    */
    
    public static List<SandDV1__Period__c> fetchPeriodsById(String periodId){
		List<Schema.SObjectField> periodFieldsList = FieldAccessibilityUtility.fetchPeriodFields2();
        SecurityUtils.checkRead(SandDV1__Period__c.SObjectType, periodFieldsList); 
        return [SELECT Id, Name,SandDV1__System_Start_and_End_Date__c,SandDV1__End_Date__c,SandDV1__Start_Date__c,CreatedDate FROM SandDV1__Period__c 
        WHERE Id =:periodId]; 
    }

    /*******************************************************************************************************
    * @description .Method to put periods into map
    * @param <param name> 
    * @return <return type> .Map of period
    */
    
    public static Map<Id,SandDV1__Period__c> periodListToMap(String periodId){

    	Map<Id,SandDV1__Period__c> periodMap = new Map<Id,SandDV1__Period__c> ();
        for(SandDV1__Period__c p : fetchPeriodsById(periodId)){
                periodMap.put(p.Id,p);
        }

        return periodMap;
    }


    /************************************************************************************************
    *@description: This methods returns a map of all sub periods related to master periods
    *@param: Set<Id>
    *@return: Map<Id, List<SandDV1__Period__c>>
    */

    public static Map<Id, List<SandDV1__Period__c>> getSubPeriodsMap(Set<Id> masterPeriodsIdSet){
        Map<Id, List<SandDV1__Period__c>> subPeriodsMap = new Map<Id, List<SandDV1__Period__c>>();

        for(SandDV1__Period__c period :getSubPeriodsList(masterPeriodsIdSet)){
            if(subPeriodsMap.containsKey(period.SandDV1__Master_Period__c)){
                subPeriodsMap.get(period.SandDV1__Master_Period__c).add(period);
            }
            else{
                subPeriodsMap.put(period.SandDV1__Master_Period__c, new List<SandDV1__Period__c>{period});
            }
        }

        return subPeriodsMap;
    }


    /************************************************************************************************
    *@description: This methods returns a list of all sub periods related to master periods
    *@param: Set<Id>
    *@return: List<SandDV1__Period__c>
    */

    public static List<SandDV1__Period__c> getSubPeriodsList(Set<Id> masterPeriodsIdSet){
        List<Schema.SObjectField> periodFieldsList = FieldAccessibilityUtility.fetchPeriodFields2();
        SecurityUtils.checkRead(SandDV1__Period__c.SObjectType, periodFieldsList); 
        return [SELECT Id, SandDV1__Master_Period__c, SandDV1__Start_Date__c, SandDV1__End_Date__c
            FROM SandDV1__Period__c
            WHERE SandDV1__Master_Period__c IN : masterPeriodsIdSet];
    }
}