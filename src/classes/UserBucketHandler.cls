/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author Ankita Trehan (ET Marlabs)
* @date 
* @description This class if for this
*/

public with sharing class UserBucketHandler {
    
    /*******************************************************************************************************
    * @description :- method to update UserBucket Fields on before insert/upadte Fields like systemActive,recordtype,
                        activtaion and deactivation date.
    * @param :- List<SandDV1__User_Bucket_Member__c>,String .
    * @return :- void. 
    */
    public static void updateuserBucketField(List<SandDV1__User_Bucket_Member__c> systeUserBuckList,String triggerAction) {
        try {
            
            Map<Id,Set<Id>> bucketUserMap = new Map<Id,Set<Id>>();
            Id activeRecordTypeID = BucketService.fetchRecordTypeID('Active_Member','SandDV1__User_Bucket_Member__c');
            Id deactiveRecordTypeID = BucketService.fetchRecordTypeID('DeActive_Member','SandDV1__User_Bucket_Member__c');
           
            for(SandDV1__User_Bucket_Member__c newsysuser :systeUserBuckList) {
                
                if(triggerAction == 'Insert') {
                    if(bucketUserMap.containskey(newsysuser.SandDV1__Bucket__c)) {
                        bucketUserMap.get(newsysuser.SandDV1__Bucket__c).add(newsysuser.SandDV1__User__c);
                    }
                    else {
                        Set<Id> userSet = new Set<Id>();
                        userSet.add(newsysuser.SandDV1__User__c);
                        bucketUserMap.put(newsysuser.SandDV1__Bucket__c,userSet);
                    }
                    newsysuser.SandDV1__System_Active__c = true;
                    if(activeRecordTypeID != null) {
                        newsysuser.RecordTypeID = activeRecordTypeID;
                    }
                }

               if(newsysuser.SandDV1__Active_From__c != null && newsysuser.SandDV1__Deactivated_On__c == null) {
                    newsysuser.SandDV1__System_User_Active__c = string.valueof(newsysuser.SandDV1__User__c) + string.valueof(newsysuser.SandDV1__Bucket__c);
                }

                else if(newsysuser.SandDV1__Active_From__c != null && newsysuser.SandDV1__Deactivated_On__c != null) {
                    newsysuser.SandDV1__System_Active__c = false;
                    if(deactiveRecordTypeID != null) {
                        newsysuser.RecordTypeID = deactiveRecordTypeID;
                    }
                    newsysuser.SandDV1__System_User_Active__c = string.valueof(newsysuser.SandDV1__User__c) + string.valueof(newsysuser.SandDV1__Bucket__c) + string.valueof(newsysuser.SandDV1__Deactivated_On__c);
                }

                if(newsysuser.SandDV1__System_Active__c == true && newsysuser.SandDV1__Active_From__c == null) {
                    newsysuser.SandDV1__Active_From__c = system.now();
                }

                if(newsysuser.SandDV1__System_Active__c == false && newsysuser.SandDV1__Deactivated_On__c == null) {
                    newsysuser.SandDV1__Deactivated_On__c = system.now();
                    if(deactiveRecordTypeID != null) {
                        newsysuser.RecordTypeID = deactiveRecordTypeID;
                    }
                }
            }

            if(!bucketUserMap.isempty() && triggerAction == 'Insert') {
                //if user already present above in buckets then don't allow user in  below buckets.
                BucketService.checkuserAlreadyInGroup(bucketUserMap,systeUserBuckList);
                //if user is already present in above bucket then deactivate user in below bucket.
                BucketService.deactiveuserinchildbucket(bucketUserMap);
            }

        }
        catch(Exception e) {
            for (SandDV1__User_Bucket_Member__c newUB : systeUserBuckList) {
                newUB.addError(+System.Label.TriggerError +':-' +e);
            }
        }
    }

    /*******************************************************************************************************
    * @description:- on insert update of user bucket perform operation on group members (remove or create).
    * @param:- List<SandDV1__User_Bucket_Member__c>,String.
    * @return:- void. 
    */ 
    public static void onInsertUpdate(List<SandDV1__User_Bucket_Member__c> systeUserBuckList,String triggerAction) {
        String bucketID = null;
        Set<Id> userBucketIDSet = new Set<Id>();
        Set<Id> bucketIdSet = new Set<Id>();
        
        try {   

            if(triggerAction == 'Update') {
                updateuserBucketField(systeUserBuckList,'Update');
            }

            for(SandDV1__User_Bucket_Member__c newsysuser :systeUserBuckList) {
                if(newsysuser.SandDV1__System_Active__c == true) {
                    userBucketIDSet.add(newsysuser.ID);
                    bucketIdSet.add(newsysuser.SandDV1__Bucket__c);
                }
                else if(newsysuser.SandDV1__System_Active__c == false && newsysuser.SandDV1__System_Code_DeActivation__c == false) {
                    userBucketIDSet.add(newsysuser.ID);
                    bucketIdSet.add(newsysuser.SandDV1__Bucket__c);
                }               
            }

            if(!userBucketIDSet.isempty()) {
                
                if(!bucketIdSet.isEmpty()) {
                    /*before calling future method (userswithgroupassociation) block bucket hierarchy
                    (top level bucket).*/
                    BucketService.blockhierarchy(bucketIdSet,true);
                }
                String sessionID = UserInfo.getSessionId();
                //future method to create remove group members
                userswithgroupassociation(userBucketIDSet,triggerAction,sessionID);
            }    
        }
        catch(Exception e) {
            userBucketIDSet.clear();
            for (SandDV1__User_Bucket_Member__c newUB : systeUserBuckList) {
                newUB.addError(+System.Label.TriggerError +':-' +e);
            }
        }
    }

    /*******************************************************************************************************
    * @description :- on insert update of user bucket perform operation on group members (remove or create) according to bucket.
    * @param :- Set<Id> ,String.
    * @return:- void . 
    */
    @Future(callout=true)
    //method to fetch users related with group via bucket and then call create or remove groupmembers 
    public static void userswithgroupassociation(Set<Id> userBucketIDSet,String triggerAction,String sessionID) {
        Set<Id> bucketIdSet = new Set<Id>();
        String bucketID = null;
        Map<Id,Set<Id>> userBucketMap = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> userGroupMap = new Map<Id,Set<Id>>();
        List<SandDV1__User_Bucket_Member__c> userBucketList = new List<SandDV1__User_Bucket_Member__c>();

        try {
            for(SandDV1__User_Bucket_Member__c newsysuser :BucketService.fetchUserBucketRecord(userBucketIDSet)) {
                userBucketList.add(newsysuser);
                bucketIdSet.add(newsysuser.SandDV1__Bucket__c);
            }

            if(!bucketIdSet.isEmpty()) {
                //fetch top level bucket from bucket id using parent id and system root id.
                bucketID = BucketService.hierarchyBucketID(bucketIdSet);
            }

            if(bucketID != null) {
                //unlock bucket hierarchy after future call.
                BucketService.callHttpmethodtoUpdateBucket(bucketID,sessionID,false);
            }

            //fetch active users linked with Bucket
            if(!userBucketList.isempty()) {
               userBucketMap = BucketService.searchUserswithBucket(userBucketList);
            }

            /*if(!userBucketMap.isempty() && triggerAction == 'Insert'){
               userGroupMap = BucketService.searchGroupId(userBucketMap);
            }*/

            //fetch child groups and users on which further operations will done(insert or remove from group)
            if(!userBucketMap.isempty() /*&& triggerAction == 'Update'*/) {
               userGroupMap = BucketService.fetchchildgroup(userBucketMap);
            }
            
            

            //create group members
            if(!userGroupMap.isempty() && triggerAction == 'Insert') {
                BucketService.creategroupmembers(userGroupMap);
            }

            //remove group members
            if(!userGroupMap.isempty() && triggerAction == 'Update') {
                BucketService.removegroupmembers(userGroupMap);
            } 
        }
        catch(Exception e) {
            throw new CustomException(e);
        }
    }
    
    /*******************************************************************************************************
    * @description:- method to restrict user's to delete userbucket records .
    * @param :- List<SandDV1__User_Bucket_Member__c> .
    * @return :- void. 
    */
    public static void onDelete(List<SandDV1__User_Bucket_Member__c> systeUserBuckList) {
        for(SandDV1__User_Bucket_Member__c userbucket : systeUserBuckList) {
            userbucket.adderror(+system.Label.Can_tDeleteRecordMessage);
        }
    }
    
    /*******************************************************************************************************
    * @description:- method is used to check bucket hierarchy is locked or not.
        on insert or update of user bucket check bucket at top level have system future call = true or false.
        If system future call is true then means bucket hierachy is locked.
    * @param <param name>:- List<SandDV1__User_Bucket_Member__c>.
    * @return <return type> :-List<SandDV1__User_Bucket_Member__c>. 
    */
    public static List<SandDV1__User_Bucket_Member__c> checkHierarchyBlock(List<SandDV1__User_Bucket_Member__c> systeUserBuckList) {
        Map<Id,Boolean> bucketRootMap = new Map<Id,Boolean>();
        List<SandDV1__User_Bucket_Member__c> newUserBucketList = new List<SandDV1__User_Bucket_Member__c>();

        try {
            //bucketRootMap will have root bucket id and future status (checkbox).
            bucketRootMap = BucketService.fetchRootIdFutureStatus(systeUserBuckList);

            for(SandDV1__User_Bucket_Member__c userbucket :systeUserBuckList) {
                if(bucketRootMap.containskey(userbucket.SandDV1__Bucket__c) && bucketRootMap.get(userbucket.SandDV1__Bucket__c) == false) {
                    newUserBucketList.add(userbucket); 
                }
                else if(bucketRootMap.containskey(userbucket.SandDV1__Bucket__c) && bucketRootMap.get(userbucket.SandDV1__Bucket__c) == true) {
                    userbucket.addError(+System.Label.Error_Message_For_Future_Call);
                }
            }
             
            if(newUserBucketList.isempty()) {
                for (SandDV1__User_Bucket_Member__c newUB : systeUserBuckList) {
                    newUB.addError(+System.Label.TriggerError);
                } 
            }
            return newUserBucketList;
        }
        catch(Exception e) {
            newUserBucketList.clear();
            for (SandDV1__User_Bucket_Member__c newUB : systeUserBuckList) {
                newUB.addError(+System.Label.TriggerError +':-' +e);
            }
            return newUserBucketList;
        }
    }
}