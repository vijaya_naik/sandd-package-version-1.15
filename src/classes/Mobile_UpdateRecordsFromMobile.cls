@RestResource(urlmapping='/Mobile_UpdateRecordsFromMobile/*')
global with sharing class Mobile_UpdateRecordsFromMobile {
    
    /** Wrapper Class to Parse the data from Mobile**/
    global class DataFromMobile{        
        public SandDV1__Sales_Order__c orderRecord                          {get;set;}       
        public List<SandDV1__Sales_Order_Line_Item__c> orderLineItemRecord  {get;set;}        
    }
    
    global class SalesForceToMobile{                                 
        public List<SandDV1__Sales_Order__c> salesOrders                       {get;set;}                      
        public List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItems     {get;set;}  
        
        public  SalesForceToMobile(){
            salesOrders = new List<SandDV1__Sales_Order__c>();
            salesOrderLineItems = new List<SandDV1__Sales_Order_Line_Item__c>();
        }       
        
    }
    
    
    @HttpPost
    global static String getRecords(){
        return 'Success';
    }
}