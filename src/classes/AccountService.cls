/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a Service class for Account
*/

public with sharing class AccountService {
    

    /*******************************************************************************************************
    * @description method to retrive accounts based on provided string
    * @param accName - account name to query
    * @return List of account . 
    */
    
    public static List<Account> fetchAccounts(String accName){
		List<Schema.SObjectField> accountFieldsList = FieldAccessibilityUtility.fetchAccountFields2();
        SecurityUtils.checkRead(Account.SObjectType, accountFieldsList); 
        String startsWith = accName + '%';
        return [SELECT Id,Name,SandDV1__IsActive__c FROM Account WHERE SandDV1__IsActive__c =:true AND Name LIKE :startsWith ORDER BY Name];
    }

    /*******************************************************************************************************
    * @description method to retrive account based on Id
    * @param accId - account Id to query
    * @return List of accounts 
    */
    
    public static List<Account> fetchAccountById(String accountId){
		List<Schema.SObjectField> accountFieldsList = FieldAccessibilityUtility.fetchAccountFields3();
        SecurityUtils.checkRead(Account.SObjectType, accountFieldsList); 
        return [SELECT Id,Name,SandDV1__Credit_Limit__c,SandDV1__Block_Order_Creation__c,AccountNumber,CreatedDate,Type,SandDV1__Account_Category__c FROM Account WHERE Id =:accountId];
    }

    /*******************************************************************************************************
    * @description method to retrive accounts 
    * @param 
    * @return List of account . 
    */
    
    public static List<Account> fetchAccounts(){
		List<Schema.SObjectField> accountFieldsList = FieldAccessibilityUtility.fetchAccountFields2();
        SecurityUtils.checkRead(Account.SObjectType, accountFieldsList); 
        return [SELECT Id,Name,SandDV1__IsActive__c FROM Account WHERE SandDV1__IsActive__c =:true ORDER BY Name LIMIT 10];
    }
}