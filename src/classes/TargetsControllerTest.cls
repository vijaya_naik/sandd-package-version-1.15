/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 31-Aug-2015
* @description This is a Test class of TargetsController
*/

@isTest(seeAllData = false)
private class TargetsControllerTest {

    static UserRole testRole;
    static UserRole testSubRole;
    static User sysAdmin;
    static User testUser;
    static User testSubUser;
    static SandDV1__Period__c period;
    static SandDV1__Period__c subPeriod;

    static SandDV1__Target__c target;
    static SandDV1__Target__c targetChild;

    static SandDV1__Target_Line_Item__c targetLI;
    static SandDV1__Target_Line_Item__c targetLIChild;


    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    
    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        System.assertequals(testRole.name,'Parent role');
        insert testRole;

        testSubRole = Initial_Test_Data.createRole('Child role', testRole.Id);
        insert testSubRole;

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        testSubUser = Initial_Test_Data.createUser('Gold','Berg','System Administrator',testSubRole.Id);
        insert testSubUser;
        System.runAs(sysAdmin){
        period = Initial_Test_Data.createPeriod('Quarter',null,20);
        insert period;

        subPeriod = Initial_Test_Data.createPeriod('Sub 1',period.Id,10);
        insert subPeriod;

        target = Initial_Test_Data.createTarget('Accepted', testUser.Id, period.Id);
        insert target;
        
        targetLI = Initial_Test_Data.createTargetLI(target.Id, subPeriod.Id);
        insert targetLI;
        }

    }

    /*******************************************************************************************************
    * @description This method will retrive the target based on the Target Id
    * @param targetID - Target Id
    * @return Target Object
    */
    
    static SandDV1__Target__c retriveTarget(String targetID){

        SandDV1__Target__c targetTemp=[SELECT Id,SandDV1__Status__c FROM SandDV1__Target__c WHERE Id=:targetID limit 1];
        return targetTemp;
    }


    /*******************************************************************************************************
    * @description This method will test the TargetsController class
    * @param null
    * @return null
    */

    @isTest static void test_method_one() {

        init();
        
        System.runAs(testUser){
            Test.startTest();

            TargetsController targetCtrl = new TargetsController();
            TargetsController.gettargetViewSize();
            TargetsController.allowTargetCreation();
            TargetsController.getAllTargets();
            TargetsController.getPeriods(period.Name);
            TargetsController.fetchPeriods();
            TargetsController.getAspectValues();
            TargetsController.fetchTargetInfo(null, period.Id); //This will create new target
            TargetsController.fetchTargetInfo(target.Id, period.Id); 

            targetChild = Initial_Test_Data.createTarget('Draft', testSubUser.Id, period.Id);
            targetChild.SandDV1__Parent__c = target.Id;
            insert targetChild;

            targetLIChild = Initial_Test_Data.createTargetLI(targetChild.Id, subPeriod.Id);
            insert targetLIChild;

            TargetsController.fetchTargetInfo(targetChild.Id, period.Id); 
            TargetsController.fetchTargetInfo(target.Id, period.Id);
            SandDV1__Target__c tar = retriveTarget(target.Id);
            System.assertEquals(tar.SandDV1__Status__c,'Accepted');

            targetChild.SandDV1__Status__c = 'Accepted';
            update targetChild;
            TargetsController.fetchTargetInfo(target.Id, period.Id);

            targetChild.SandDV1__Status__c = 'To be Revised';
            update targetChild;
            TargetsController.fetchTargetInfo(target.Id, period.Id);

            targetChild.SandDV1__Status__c = 'Suspended';
            update targetChild;
            TargetsController.fetchTargetInfo(target.Id, period.Id);

            targetChild.SandDV1__Status__c = 'Assigned';
            update targetChild;
            TargetsController.fetchTargetInfo(target.Id, period.Id);

            target.SandDV1__Status__c = 'Assigned';
            targetChild.SandDV1__Status__c = 'Assigned';
            update target;
            update targetChild;
            TargetsController.fetchTargetInfo(target.Id, period.Id);
            tar = retriveTarget(target.Id);
            System.assertEquals(tar.SandDV1__Status__c,'Assigned');

            target.SandDV1__Status__c = 'To be Revised';
            update target;
            TargetsController.fetchTargetInfo(target.Id, period.Id);
            tar = retriveTarget(target.Id);
            System.assertEquals(tar.SandDV1__Status__c,'To be Revised');

            target.SandDV1__Status__c = 'Suspended';
            update target;
            TargetsController.fetchTargetInfo(target.Id, period.Id);
            tar = retriveTarget(target.Id);
            System.assertEquals(tar.SandDV1__Status__c,'Suspended');


            SandDV1__Target__c targetToInsert = Initial_Test_Data.createTarget('Draft', testUser.Id, period.Id);
            SandDV1__Target_Line_Item__c targetLIToInsert = Initial_Test_Data.createTargetLI(null, subPeriod.Id);

            List<TargetsDomain.PeriodTLIWrapper> periodTLIList = new List<TargetsDomain.PeriodTLIWrapper>();
            TargetsDomain.PeriodTLIWrapper periodTLI = new TargetsDomain.PeriodTLIWrapper();
            periodTLI.tliObj = targetLIToInsert;
            periodTLIList.add(periodTLI);

            TargetsController.saveTarget(targetToInsert, periodTLIList, true, false, false, false); //draft
            TargetsController.checkExistingTarget(period.Id,'Revenue');
            TargetsController.saveTarget(targetToInsert, periodTLIList, false, true, false, false); //assigned
            TargetsController.saveTarget(targetToInsert, periodTLIList, false, false, false, true); //Accepted
            TargetsController.saveTarget(targetToInsert, periodTLIList, false, false, true, false); //Revise

            SandDV1__Target__c targetToInsertExcp = Initial_Test_Data.createTarget('Drafti', testUser.Id, period.Id);
            
            List<TargetsDomain.TargetInfoWrapper> childWrapList = new List<TargetsDomain.TargetInfoWrapper>();
            TargetsDomain.TargetInfoWrapper childWrap= new TargetsDomain.TargetInfoWrapper();
            childWrap.targetObj = targetToInsert;
            childWrap.periodTliWrapList = periodTLIList;
            childWrapList.add(childWrap);

            TargetsController.saveAllTargets(childWrapList, true, false); //draft
            TargetsController.saveAllTargets(childWrapList, false, true); //Assigned


            List<TargetsDomain.TargetInfoWrapper> childWrapListExcp = new List<TargetsDomain.TargetInfoWrapper>();
            TargetsDomain.TargetInfoWrapper childWrapExcp = new TargetsDomain.TargetInfoWrapper();
            childWrapExcp.targetObj = targetToInsertExcp;
            childWrapExcp.periodTliWrapList = periodTLIList;
            childWrapListExcp.add(childWrapExcp);


            TargetsDomain.TargetInfoWrapper childWrapDraft = new TargetsDomain.TargetInfoWrapper();
            childWrapDraft = TargetsController.saveChildTarget(childWrap,true, false, false); //draft status
            System.AssertEquals(childWrapDraft.isDraft, true);

            TargetsDomain.TargetInfoWrapper childWrapAssigned = new TargetsDomain.TargetInfoWrapper();
            childWrapAssigned = TargetsController.saveChildTarget(childWrap,false, true, false); //Assigned status
            System.AssertEquals(childWrapAssigned.isAssigned, true);

            TargetsDomain.TargetInfoWrapper childWrapSuspend = new TargetsDomain.TargetInfoWrapper();
            childWrapSuspend = TargetsController.saveChildTarget(childWrap,false, false, true); //Suspend status
            System.AssertEquals(childWrapSuspend.isSuspended, true);

            SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
            try {

                TargetsController.saveAllTargets(childWrapList, false, true);

            }
            catch(Exception e){

                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            
            try {

                TargetsController.saveTarget(targetToInsert, periodTLIList, true, false, false, false);

            }
            catch(Exception e){

                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            
            TargetsController.deleteTarget(target.Id);
            Test.stopTest();
        } 
    }
    
    
}