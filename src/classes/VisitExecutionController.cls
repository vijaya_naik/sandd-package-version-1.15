public with sharing class VisitExecutionController {
    
    public VisitExecutionController() {}

    /*@remoteAction
    public static List<Holiday> getPublicHolidays(integer selectedMonth, integer selectedYear) {

        Integer totalDaysInMonth    = Date.daysInMonth(selectedYear, selectedMonth);
        Date visitStartDate         = Date.newinstance(selectedYear, selectedMonth, 1);
        Date visitEndDate           = Date.newinstance(selectedYear, selectedMonth, totalDaysInMonth);

        return VisitPlanningService.getHolidays(visitStartDate, visitEndDate);
    }*/

    @remoteAction
    public static List<SandDV1__Visit__c> getParticularDayVisits(integer selectedDate, integer selectedMonth, integer selectedYear) {
		List<Schema.SObjectField> visitFieldsList = FieldAccessibilityUtility.fetchVisitDayFields();
        SecurityUtils.checkRead(SandDV1__Visit__c.SObjectType, visitFieldsList);
        Date visitDate = Date.newinstance(selectedYear, selectedMonth, selectedDate);
        return new list<SandDV1__Visit__c>([select Name, SandDV1__Status__c, SandDV1__Checkin_DateTime__c, SandDV1__Checkout_DateTime__c, SandDV1__Account__c, SandDV1__Account__r.Name, SandDV1__Account__r.Phone, SandDV1__Account__r.BillingStreet, SandDV1__Account__r.BillingCity, SandDV1__Account__r.BillingState, SandDV1__Account__r.BillingCountry, SandDV1__Account__r.BillingPostalCode, SandDV1__Account__r.Location__Latitude__s, SandDV1__Account__r.Location__Longitude__s from SandDV1__Visit__c where SandDV1__Visit_Plan_Date__c =: visitDate AND SandDV1__Visit_Plan__r.SandDV1__User__c =: UserInfo.getUserId() AND SandDV1__Visit_Plan__r.SandDV1__Status__c = 'Approved' order by SandDV1__Account__r.Name]);
    }    
}