/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/21/2015.
* @description This class is used as Bucket Service.
*/
public with sharing class SystemObjectBucketService{
    public SystemObjectBucketService() {
      
    }

    /*******************************************************************************************************
    * @description :- Method fetch Active System Object Bucket list related with bucket
    * @param :- Set<Id>. // bucket id set.
    * @return :- List<SandDV1__System_Object_Bucket__c>. 
    */
    public static List<SandDV1__System_Object_Bucket__c> fetchBucketRecords(Set<Id> bucketIdSet) {
        
        List<Schema.SObjectField> objBucketFieldsList = FieldAccessibilityUtility.fetchSysObjBucket();
        SecurityUtils.checkRead(SandDV1__System_Object_Bucket__c.SObjectType, objBucketFieldsList);   
        return [Select Id,SandDV1__Bucket__c,SandDV1__Account__c from SandDV1__System_Object_Bucket__c where 
                    SandDV1__Bucket__c IN :bucketIdSet AND SandDV1__Active_From__c !=: null AND SandDV1__Deactivated_On__c=:null];
    }
}