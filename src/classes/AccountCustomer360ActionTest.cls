/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 08-Dec-2015
* @description This is a Test class for AccountCustomer360ActionController
*/

@isTest(seeAllData = false)
private class AccountCustomer360ActionTest {

    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Account testAccount;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        System.runAs(sysAdmin){
            
            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount;

        }
    }

    @isTest static void test_method_one() {
        
        init();
        System.runAs(testUser){

            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testUser);
            AccountCustomer360ActionController accObj = new AccountCustomer360ActionController(sc);
            System.assert(accObj <> null);
            Test.stopTest(); 
        }
    }
    
    
}