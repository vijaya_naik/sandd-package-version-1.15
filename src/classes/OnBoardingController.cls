/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class OnBoardingController {
    public static String  OnBoardingDraftStatus {get; set;}
    public static String  OnBoardingPendingApprovalStatus {get; set;}
    public static String  OnBoardingNeedApprovalStatus {get; set;}
    public static String  OnBoardingApprovedStatus {get; set;}
    public static String  OnBoardingDraftStatusColor {get; set;}
    public static String  OnBoardingPendingApprovalStatusColor {get; set;}
    public static String  OnBoardingNeedApprovalStatusColor {get; set;}
    public static String  OnBoardingApprovedStatusColor {get; set;}
    
    public OnBoardingController() {
        OnBoardingDraftStatus = ConfigurationService.OnBoardingDraftStatus;
        OnBoardingNeedApprovalStatus = ConfigurationService.OnBoardingNeedApprovalStatus ;
        OnBoardingPendingApprovalStatus = ConfigurationService.OnBoardingPendingApprovalStatus;
        OnBoardingApprovedStatus = ConfigurationService.OnBoardingApprovedStatus ;
        OnBoardingDraftStatusColor = ConfigurationService.OnBoardingDraftStatusColor;
        OnBoardingPendingApprovalStatusColor = ConfigurationService.OnBoardingPendingApprovalStatusColor;
        OnBoardingNeedApprovalStatusColor = ConfigurationService.OnBoardingNeedApprovalStatusColor ;
        OnBoardingApprovedStatusColor = ConfigurationService.OnBoardingApprovedStatusColor ;
    }
     
    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__OnBoarding_Document__c Records of account and onboarding setting records from custom setting.
    * @param :- String AccountId.
    * @return :- list<OnBoardingDomain.DocumentWrapper>.
    */
    @RemoteAction
    public static list<OnBoardingDomain.DocumentWrapper> getOnBoardingDocuments(String AccountId,String AccountStatus) {
        OnBoardingDraftStatus = ConfigurationService.OnBoardingDraftStatus;
        list<OnBoardingDomain.DocumentWrapper> OnBoardingDocList = new list<OnBoardingDomain.DocumentWrapper>();
        list<SandDV1__OnBoarding_Document__c> ExistingList = new list<SandDV1__OnBoarding_Document__c>();
        Map<String,String> statusColorMap = new Map<String,String>();
        Set<Id> settingIdSet = new Set<Id>();
        Set<Id> accountIdSet= new Set<Id>();
        accountIdSet.add(AccountId);
        ExistingList = ServiceFacade.fetchAccountDocs(accountIdSet);
        
        for(SandDV1__OnBoardingStatusColor__c statuscolor : ServiceFacade.fetchOnBoardingStatusColor()) {
            statusColorMap.put(statuscolor.Name,statuscolor.SandDV1__Color_Code__c);
        }
        
        for(SandDV1__OnBoarding_Document__c existingdoc: ExistingList) {
            settingIdSet.add(existingdoc.SandDV1__System_Custom_Setting_ID__c);
        }
        
        for(SandDV1__OnBoardingDocumentSetting__c docSett : ServiceFacade.fetchOnBoardingDocSetting()) {
            
            if(!settingIdSet.contains(docSett.Id)) {
                OnBoardingDomain.DocumentWrapper wrap = new OnBoardingDomain.DocumentWrapper();
                SandDV1__OnBoarding_Document__c newdoc = new SandDV1__OnBoarding_Document__c();
                newdoc.Name = docSett.Name;
                newdoc.SandDV1__Comments__c = '';
                newdoc.SandDV1__Is_Mandatory__c = docSett.SandDV1__Is_Mandatory__c;
                newdoc.SandDV1__Description__c = docSett.SandDV1__Description__c;
                //newdoc.SandDV1__Status__c = 'Draft';
                newdoc.SandDV1__Status__c =  OnBoardingDraftStatus;
                newdoc.SandDV1__System_Custom_Setting_ID__c = docSett.Id;
                newdoc.Id = null;
                newdoc.SandDV1__AccountId__c = AccountId;
                newdoc.SandDV1__Approval_Process_Needed__c = docSett.SandDV1__Approval_Process_Needed__c;
                newdoc.SandDV1__Type__c = docSett.SandDV1__Type__c; 
                wrap.statusColor = statusColorMap.get(newdoc.SandDV1__Status__c);
                wrap.boardingDocObj = newdoc;
                wrap.IsSetting = true;
                if(AccountStatus == '1') {
                    wrap.IsAccountActive= true;
                }
                else {
                    wrap.IsAccountActive= false;
                }
                
                OnBoardingDocList.add(wrap);
            }         
        }
        
        for(SandDV1__OnBoarding_Document__c existingdoc:ExistingList) {
            OnBoardingDomain.DocumentWrapper wrap = new OnBoardingDomain.DocumentWrapper();
            wrap.boardingDocObj = existingdoc;
            wrap.statusColor = statusColorMap.get(existingdoc.SandDV1__Status__c);
            wrap.boardingDocObj.SandDV1__Is_Mandatory__c  = existingdoc.SandDV1__Is_Mandatory__c ;
            if(existingdoc.SandDV1__System_Custom_Setting_ID__c != '' && existingdoc.SandDV1__System_Custom_Setting_ID__c  != null) {
                wrap.IsSetting = true;
            }
            else{
                wrap.IsSetting = false;
            }
            if(AccountStatus == '1') {
                    wrap.IsAccountActive= true;
            }
            else {
                wrap.IsAccountActive= false;
            }
            OnBoardingDocList.add(wrap);
            settingIdSet.add(existingdoc.SandDV1__System_Custom_Setting_ID__c);
        }
        
        return OnBoardingDocList;
    }
    
    /*******************************************************************************************************
    * @description :- Method to save attachment and document with account id
    * @param :- SandDV1__OnBoarding_Document__c docObj,String postContent,String fName.
    * @return :- String .
    */
    @RemoteAction
    public static String doUploadAttachment(SandDV1__OnBoarding_Document__c docObj,String attachmentBody, String attachmentName) {
        String docId = OnBoardingService.doUploadAttachment(docObj,attachmentBody,attachmentName) ;
        return docId;
    }

    /*******************************************************************************************************
    * @description :- Method to delete attachment.
    * @param :- Attachment .
    * @return :- void .
    */
    @RemoteAction
    public static void deleteAttachment(Attachment attachObj) {
        if(attachObj!=null) {
            OnBoardingService.deleteAttachment(attachObj);
        }
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch attachments using parent id.
    * @param :- String parentID.
    * @return :- List<OnBoardingDomain.AttachmentWrapper>.
    */
    @RemoteAction
    public static List<OnBoardingDomain.AttachmentWrapper> fetchAttachments(SandDV1__OnBoarding_Document__c docObj) {
         List<OnBoardingDomain.AttachmentWrapper> attachmentList = new  List<OnBoardingDomain.AttachmentWrapper>();
         String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
         for(Attachment attach :AttachmentService.fetchAttachment (docObj.Id)) {
             OnBoardingDomain.AttachmentWrapper attachWrap = new OnBoardingDomain.AttachmentWrapper();
                 attachWrap.attachObj = attach;
                 if(docObj.SandDV1__Status__c == ConfigurationService.OnBoardingApprovedStatus && docObj.SandDV1__Approval_Process_Needed__c == true) {
                     attachWrap.IsLocked = true;
                 }
                 else {
                     attachWrap.IsLocked = false;
                 }
                 attachWrap.attachUrl = sfdcBaseURL +  system.Label.AttachmentUrl + attach.Id;
             attachmentList.add(attachWrap);
         }    
         return attachmentList;
    }
    
     /*******************************************************************************************************
    * @description :- Method to submit SandDV1__OnBoardingdocument__c record for approval.
    * @param :- String docID.
    * @return :- void .
    */
    @RemoteAction
    public static OnBoardingDomain.OnBoardingMessageInfo submitForApproval(String docID) {
        OnBoardingDomain.OnBoardingMessageInfo mesageWrap = new OnBoardingDomain.OnBoardingMessageInfo();
        try{
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(docID);
            Approval.ProcessResult result = Approval.process(app);
            mesageWrap.boardingId = docID;
            mesageWrap.messagetype = 'Success';
            mesageWrap.boardingMessage = 'ApprovalSubmitted';
        }
        catch(Exception e) {
            mesageWrap.boardingId = docID;
            mesageWrap.messagetype = 'Error';
            mesageWrap.boardingMessage = String.valueof(e);
        }
        return mesageWrap;
    }    

    
}