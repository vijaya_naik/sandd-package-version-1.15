/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 30-May-2016
* @description 
*/
public with sharing class NewSchemeEditMainController {
	public String returnURL {get;set;}
    public String sId {get;set;}
    public String schemeApplied {get;set;} 

	public NewSchemeEditMainController(ApexPages.StandardController con) {
		sId = con.getId();
        System.PageReference pr = con.cancel();
        returnURL = pr.getUrl();
        List<Schema.SObjectField> schemeFieldsList = new List<Schema.SObjectField>{SandDV1__Scheme__c.SObjectType.fields.SandDV1__Scheme_Applied__c};
        SecurityUtils.checkRead(SandDV1__Scheme__c.SObjectType, schemeFieldsList);  
        SandDV1__Scheme__c expense = [SELECT Id,SandDV1__Scheme_Applied__c FROM SandDV1__Scheme__c WHERE Id=:sId];
        schemeApplied = expense.SandDV1__Scheme_Applied__c ? 'true' : 'false';
	}
}