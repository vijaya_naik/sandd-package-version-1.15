/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeBenefitSelectionController {
    /*******************************************************************************************************
    * @description :- Method to fetch applicable scheme records .
    * @param :- SandDV1__Sales_Order__c salesOrder, List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemList
    * @return :- List<SchemeDomain.SchemeBenefitsWrapperV1>
    */
    
    public static List<SchemeDomain.SchemeBenefitsWrapperV1> getApplicableSchemeBenefits(SandDV1__Sales_Order__c salesOrder, List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemList){
        List<SandDV1__Scheme__c> activeSchemes = SchemeService.getAllActiveSchemes();
        List<SchemeDomain.ApplicableSchemesWrapper> applicableSchemesWrapperList = SchemeService.getApplicableSchemes(activeSchemes, salesOrder, salesOrderLineItemList);
        

        Set<String> schemeIdSet = new Set<String>();
        for(SchemeDomain.ApplicableSchemesWrapper schemeWrapper :applicableSchemesWrapperList){
            for(SandDV1__Scheme__c scheme :schemeWrapper.schemesList){
                schemeIdSet.add(scheme.Id);
            }
        }

        Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap = SchemeBenefitRangeService.getSchemeBenefitRangeMap(schemeIdSet);

        List<SchemeDomain.SchemeBenefitRangeWrapper> applicableSchemeBenefitRangeList = SchemeBenefitRangeService.getApplicableSchemeBenefitRange(applicableSchemesWrapperList, schemeBenefitRangeMap);

        List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList = new List<SandDV1__Scheme_Benefit_Range__c>();
        for(SchemeDomain.SchemeBenefitRangeWrapper schmeBenefitRangeWrapper :applicableSchemeBenefitRangeList){
            schemeBenefitRangeList.addAll(schmeBenefitRangeWrapper.schemeBenefitRangeList);
        }
        
        Map<String, List<SandDV1__Scheme_Benefit__c>> schemeBenefitsMap = SchemeBenefitService.getSchemeBenefitsMap(schemeBenefitRangeList);
        
        return SchemeBenefitService.getSchemeBenefitsList(applicableSchemeBenefitRangeList, schemeBenefitsMap);
    }
}