/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 28-July-2016
* @description This class for DeliveryOrderClosePage
*/

public with sharing class DeliveryOrderCloseController {

    public String orderClosed {get;set;}
    public String deliveryId {get;set;}
    
    public DeliveryOrderCloseController(ApexPages.StandardController stdController) {
     
     deliveryId = stdController.getId();
     
    }
    public void closeDeliveryOrder(){
        
        List<SandDV1__Delivery_Order__c> deliveryOrderList = DeliveryOrderService.fetchDeliveryOrderRecord(deliveryId);
         if(!deliveryOrderList.isEmpty()){
             if(deliveryOrderList[0].SandDV1__Status__c != 'Close'){

                 DeliveryOrderService.changeDeliveryOrderStatus(deliveryOrderList[0],'Close');
                 orderClosed = 'true';
             }
             else {
                 
                 orderClosed = 'false';
             }
         }
    }
}