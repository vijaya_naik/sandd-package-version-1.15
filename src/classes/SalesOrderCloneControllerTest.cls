/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author ET Marlabs
* @date 21-Dec-2015
* @description This is a controller for salesOrderClonePage
*/
@isTest(seeAllData = false)
private class SalesOrderCloneControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Product2 testProduct0;
    static Product2 testProduct;
    static Product2 testProduct2;
    static Pricebook2 testPricebook;
    static PricebookEntry testPriceEntry;
    static PricebookEntry testPriceEntry2;
    static PricebookEntry testPriceEntry3;
    static PricebookEntry testPriceEntry4;
    static Account testAccount;
    static SandDV1__Shipping_Address__c testShippingAddress;
    static SandDV1__Sales_Order__c testSalesOrder;
    static SandDV1__Sales_Order__c testSalesOrder2;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLI0;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLI;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLIDel;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLIDel2;


    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    
    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        testUser.ManagerId = sysAdmin.Id;
        insert testUser;
        System.assertEquals(testUser.ManagerId,sysAdmin.Id);

        
        System.runAs(sysAdmin){
            
            testProduct0 = Initial_Test_Data.createProduct('Test Product0','test0001');
            insert testProduct0;


            testProduct = Initial_Test_Data.createProduct('Test Product','test001');
            insert testProduct;

            testProduct2 = Initial_Test_Data.createProduct('Test Product 2','test002');
            insert testProduct2;

            testPricebook = Initial_Test_Data.createPriceBook('Test pricebook');
            insert testPricebook;
            System.assertEquals(testPricebook.Name,'Test pricebook');

            Id stdpb = Test.getStandardPricebookId();

            testPriceEntry = Initial_Test_Data.createPriceBookEntry(stdpb, testProduct.Id);
            insert testPriceEntry;


            testPriceEntry2 = Initial_Test_Data.createPriceBookEntry(testPricebook.Id, testProduct.Id);
            insert testPriceEntry2;

            testPriceEntry3 = Initial_Test_Data.createPriceBookEntry(stdpb, testProduct2.Id);
            insert testPriceEntry3;

            testPriceEntry4 = Initial_Test_Data.createPriceBookEntry(testPricebook.Id, testProduct2.Id);
            insert testPriceEntry4;

            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount;

            testShippingAddress = Initial_Test_Data.createShippingAddress('Test Shipping',testAccount.Id );
            insert testShippingAddress;

            testSalesOrder = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            insert testSalesOrder;

            testSalesOrderLI0 = Initial_Test_Data.createSalesOrderLI(testSalesOrder.Id,testProduct0.Id,testPricebook.Id );
            insert testSalesOrderLI0;

            testSalesOrderLI = Initial_Test_Data.createSalesOrderLI(testSalesOrder.Id,testProduct.Id,testPricebook.Id );
            insert testSalesOrderLI;

            testSalesOrder2 = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            insert testSalesOrder2;


        }

    }

    @isTest static void test_method_one() {

        init();
        
        System.runAs(testUser){
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testSalesOrder);
            SalesOrderCloneController controller = new SalesOrderCloneController(sc);
            controller.cloneSalesOrder();

            Test.stopTest();
        } 
    }

    @isTest static void test_method_two() {

        init();
        
        System.runAs(testUser){
            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testSalesOrder2);
            SalesOrderCloneController controller = new SalesOrderCloneController(sc);
            controller.cloneSalesOrder();

            Test.stopTest();
        } 
    }

}