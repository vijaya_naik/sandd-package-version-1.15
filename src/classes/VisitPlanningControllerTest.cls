/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Sep 2015
* @description This class if for providing testdata to VisitPlanningController test class(VisitPlanTest).
*/

@isTest(seeAllData=false)
private class VisitPlanningControllerTest {
    
    /*******************************************************************************************************
* @description: Setting up test data for the test class.
*  
*/
    @testSetup static void visitDataInit() {
        
        list<Account> accList                     = new list<Account>();
        list<SandDV1__period__c> periodList                = new list<SandDV1__Period__c>();
        list<SandDV1__Route__c> routeList                  = new list<SandDV1__Route__c>();
        list<SandDV1__Route_Account__c> routeAccountList   = new list<SandDV1__Route_Account__c>();
        Date todayDate                            = Date.today();
        
        User adminUser = VisitPlanTestData.testAdminUser();
        insert adminUser;
        
        User standardUser = VisitPlanTestData.testStandardUser(adminUser.Id);
        insert standardUser;
        system.assertEquals(standardUser.ManagerID,adminUser.Id);
        
        for(integer i=0; i < 200; i++) {
            
            accList.add(VisitPlanTestData.testAccount(i));
        }
        
        insert accList;
        
        for(integer i=0; i < 10; i++) {
            
            Boolean b       = true;//(Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate.addDays(i);
            Date endDate    = startDate.addDays(7);
            periodList.add(VisitPlanTestData.testPeriod(b, startDate, endDate));
        }
        insert periodList;
        
        for(integer i=0; i < 10; i++) {
            
            Boolean b = (Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate.addDays(i);
            Date endDate    = startDate.addDays(1 * i);
            routeList.add(VisitPlanTestData.testroute(b));
        }
        
        insert routeList;
        
        for(integer i=0; i < 10; i++) {
            
            Boolean b = (Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate.addDays(i);
            Date endDate    = startDate.addDays(1 * i);
            routeAccountList.add(VisitPlanTestData.testRouteAccount(routeList[i].Id, accList[i].Id, b));
        }
        
        insert routeAccountList;
        
        new VisitPlanningController(new ApexPages.StandardController(new SandDV1__Visit_Plan__c()));
    }
    
    /*******************************************************************************************************
* @description: Setting up Holiday test data for the test class.
*  
*/
    @testSetup static void visitDataInit2() {
        
        Date todayDate                            = Date.today();
        Holiday holidayTest    = VisitPlanTestData.testHoliday(todayDate);
        Holiday holidayTest2   = VisitPlanTestData.testHoliday(todayDate.addDays(4));
        Holiday holidayTest3   = VisitPlanTestData.testHoliday(todayDate);
        Holiday holidayTest4   = VisitPlanTestData.testHoliday(todayDate.addDays(6));
        Holiday [] holidayList = new list<Holiday> {holidayTest, holidayTest2, holidayTest3, holidayTest4};
        system.assertEquals(holidayList[0],holidayTest);
            insert holidayList;
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Period picklist.
*  
*/
    private static testMethod void retreiveActivePeriodsTest() {
        
        test.startTest();
        list<SandDV1__Period__c> periodList   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true]; 
        list<SandDV1__Period__c> periodValues = VisitPlanningController.retreivePeriods();
        system.assertEquals(periodList, periodValues);
        test.stopTest();
    }
    
     /*******************************************************************************************************
* @description: Testing the functionality to populate Period picklistwith VisitPlan.
*  
*/
    private static testMethod void retreiveActivePeriodsVisitTest() {
        
        Profile p =[select Id from Profile where Name = 'Standard User'];
        User standardUser = [select Id from User where profileId =: p.Id limit 1];
       
        test.startTest();
       
        system.runAs(standardUser) {
            Id periodId = [select id from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1].Id;
        SandDV1__Visit_Plan__c visit = VisitPlanTestData.testVisitPlan(periodId, userInfo.getUserId()); 
            insert visit;
        list<SandDV1__Period__c> periodList   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true and Id !=: periodId]; 
        list<SandDV1__Period__c> periodValues = VisitPlanningController.retreivePeriods();
            system.assertEquals(periodList, periodValues);
        }
        test.stopTest();
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Period picklist.
*  
*/  
    private static testMethod void retreiveActiveRoutesTest() {
        
        list<SandDV1__Route__c> routeList   = [select Id, Name from SandDV1__Route__c where SandDV1__Active__c = true]; 
        list<SandDV1__Route__c> routeValues = VisitPlanningController.retreiveRoutes();
        system.assertEquals(routeList, routeValues);
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Period picklist.
*  
*/  
    private static testMethod void retreiveAccountCategoryTest() {
        
        list<selectOption> periodvalues = VisitPlanningController.retreiveAccountCategory();
        List<SandDV1__Period__c> subPeriod = VisitPlanningController.retreiveSubPeriods([select id from SandDV1__Period__c limit 1].Id);
        system.assertEquals(subPeriod.size(), 0);
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate selected Route related Accounts.
*  
*/ 
    private static testMethod void retreiveRouteAccountsTest() {
        
        map<id,string> customerMap = new map<id, string>();
        map<id,string> customerResMap = new map<id, string>();
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true AND SandDV1__Start_Date__c=: Date.today() AND SandDV1__End_Date__c =: Date.today().addDays(7) limit 1];
        SandDV1__Route__c routeInstance     = [select Id, Name from SandDV1__Route__c where SandDV1__Active__c = true limit 1];
        
        for(SandDV1__Route_Account__c routeAcc : [select SandDV1__Account__c, SandDV1__Account__r.Name from SandDV1__Route_Account__c where SandDV1__Route__c =: routeInstance.Id and SandDV1__Active__c = true]) {
            customerMap.put(routeAcc.SandDV1__Account__c, routeAcc.SandDV1__Account__r.Name);
        }
        
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningController.retreiveAccounts(periodInstance.Id, routeInstance.Id, null);
        
        for(VisitPlanningModel.CustomerWrapper cusWrap : resWrap.customerValues) {
            customerResMap.put(cusWrap.customerId, cusWrap.customerName);
        }
        system.assertEquals(customerMap, customerResMap);
        system.assertEquals(customerResMap.keySet().containsAll(customerMap.keySet()), true);
        system.assertEquals(customerResMap.Values(), customerMap.Values());
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to retrieve all Accounts(no Route is selected) in the org.
*  
*/
    private static testMethod void retreiveAllAccountsTest() {
        
        map<id,string> customerMap = new map<id, string>();
        map<id,string> customerResMap = new map<id, string>();
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1];
        
        for(Account acc : [select id, Name from Account]) {
            customerMap.put(acc.Id, acc.Name);
        }
        
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningController.retreiveAccounts(periodInstance.Id, Label.Select_Route, null);
        
        for(VisitPlanningModel.CustomerWrapper cusWrap : resWrap.customerValues) {
            customerResMap.put(cusWrap.customerId, cusWrap.customerName);
        }
        system.assertEquals(customerMap, customerResMap);
        system.assertEquals(customerResMap.keySet().containsAll(customerMap.keySet()), true);
        List<string> customerRes = new list<String>();
        customerRes.addAll(customerResMap.Values());
        customerRes.sort();
        List<string> customerMap1 = new list<String>();
        customerMap1.addAll(customerMap.Values());
        customerMap1.sort();
        system.assertEquals(customerRes, customerMap1);
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Saved to Draft(Planning).
*  
*/  
    private static testMethod void saveVisitPlanASDraft() {
        
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c  from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1];
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningController.retreiveAccounts(periodInstance.Id, Label.Select_Route, null);
        
        for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
            for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                if(res.customerName == 'Test Visit Plan Account0') {
                    if(date.parse(visitWrap.dateValue) == Date.today()) {
                        visitWrap.IsVisitPlanned = true;
                    } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                        visitWrap.IsVisitPlanned = true;
                    }
                }
            }
        }
        system.assertEquals(periodInstance.SandDV1__Active__c, true);
        system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
        SandDV1__Visit_Plan__c boolRes = VisitPlanningController.saveVisitPlan(resWrap, periodInstance.Id, 'Planning');
        
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Submitted to Manager, 
*               but Manager for the running is not defined.
*/
    private static testMethod void saveVisitPlanAsSubmittedWithoutManager() {
        
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1];
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningController.retreiveAccounts(periodInstance.Id, Label.Select_Route, null);
        
        for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
            for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                if(res.customerName == 'Test Visit Plan Account0') {
                    if(date.parse(visitWrap.dateValue) == Date.today()) {
                        visitWrap.IsVisitPlanned = true;
                    } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                        visitWrap.IsVisitPlanned = true;
                    }
                }
            }
        }
        system.assertEquals(periodInstance.SandDV1__Active__c, true);
        system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
        
        SandDV1__Visit_Plan__c boolRes;
        User u    = [sELECT Id,IsActive FROM User WHERE ManagerId = null And IsActive =true LIMIT 1];
        system.runAs(u) {
            try {
                boolRes = VisitPlanningController.saveVisitPlan(resWrap, periodInstance.Id, 'Submitted');
            } catch(Exception e)  {
                system.assertEquals(e.getMessage(), Label.Manager_Not_Defined);
            }
            //system.assertEquals(boolRes, false);
        }
    }
    
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Submitted to Manager, 
*               but Manager for the running is not defined.
*/
    private static testMethod void saveVisitPlanAsSubmittedWithManager() {
        
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1];
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningController.retreiveAccounts(periodInstance.Id, Label.Select_Route, null);
        
        for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
            for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                if(res.customerName == 'Test Visit Plan Account0') {
                    if(date.parse(visitWrap.dateValue) == Date.today()) {
                        visitWrap.IsVisitPlanned = true;
                    } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                        visitWrap.IsVisitPlanned = true;
                    }
                }
            }
        }
        system.assertEquals(periodInstance.SandDV1__Active__c, true);
        system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
        
        SandDV1__Visit_Plan__c boolRes;
        profile p = [select Id from profile where Name = 'Standard User'];
        User u    = [sELECT Id,IsActive FROM User WHERE profileId =: p.Id And IsActive =true LIMIT 1];
        system.runAs(u) {
            try {
                boolRes = VisitPlanningController.saveVisitPlan(resWrap, periodInstance.Id, 'Submitted');
            } catch(Exception e)  {
                system.assertEquals(e.getMessage(), Label.Manager_Not_Defined);
            }
            //system.assertEquals(boolRes, false);
        }
    }
}