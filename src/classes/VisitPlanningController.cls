/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Aug 2015
* @description This class is controller for VisitPlanning Page.
*/

public with sharing class VisitPlanningController {
    
    public VisitPlanningController(ApexPages.StandardController con) {  }
    
    private static Map<Integer, String> monthsMap = new Map<Integer, String>{1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'};
    private static Map<Integer, String> weekMap   = new Map<Integer, String>{1 => 'Sunday', 2 => 'Monday', 3 => 'Tuesday', 4 => 'Wednesday', 5 => 'Thursday', 6 => 'Friday', 7 => 'Saturday'};    
            
            /*******************************************************************************************************
* @description: Populate Period picklist in the VisitPlanning Page.
* @return list<SandDV1__Period__c> - returns list of Period. 
*/
            @remoteAction
            public static List<SandDV1__Period__c> retreivePeriods() {
                
                Set<Id> visitAllocatedPeriod   = new Set<Id>();
                List<SandDV1__Period__c> periodList     = new List<SandDV1__Period__c>();
                
                for(SandDV1__Visit_Plan__c visitPlan : VisitPlanningService.getCurrentUserVisitPlan(UserInfo.getUserId())) {  
                    visitAllocatedPeriod.add(visitPlan.SandDV1__Period__c);
                }
                
                if(!visitAllocatedPeriod.isEmpty()) {
                    for(SandDV1__Period__c period : VisitPlanningService.getActivePeriodInfo(null)) {
                        if(!visitAllocatedPeriod.contains(period.Id)) {
                            periodList.add(period);
                        }
                    }
                } else {
                    periodList = VisitPlanningService.getActivePeriodInfo(null);
                }
                return periodList;
            }
    
    /*******************************************************************************************************
* @description: Populate Sub Period picklist in the VisitPlanning Page.
* @return list<SandDV1__Period__c> - returns list of Period. 
*/
    @remoteAction
    public static List<SandDV1__Period__c> retreiveSubPeriods(Id periodId) {
        
        return(VisitPlanningService.getActiveSubPeriodInfo(periodId));
    }
    
    
    /*******************************************************************************************************
* @description: Populate Route picklist in the VisitPlanning Page.
* @return list<SandDV1__Route__c> - returns list of Period. 
*/
    @remoteAction
    public static List<SandDV1__Route__c> retreiveRoutes() {
        
        return(VisitPlanningService.getRouteInfo());
    }


     /*******************************************************************************************************
* @description: To return Account category picklist values.
* @return list<selectOption> - returns list of Account Category Values. 
*/
    @remoteAction
    public static List<selectOption> retreiveAccountCategory() {
        
        return(VisitPlanningService.retreivePicklistValues('Account', 'Account_Category__c')); 
    }
    
    /*******************************************************************************************************
* @description: Get Accounts related to particular route.
* @param: PeriodId - Selected Period Value.
* @param: routeId  - Selected route Value
* @param: AccountCategory - Selected Account Category.
* @return VisitPlanningModel.ResponseWrapper - Sending data in VisitPlanningModel.ResponseWrapper wrapper format. 
*/
    @remoteAction
    public static VisitPlanningModel.VisitPlanResponseWrapper retreiveAccounts(String periodId, String routeId, String accountCategory) {
        
        VisitPlanningModel.VisitPlanResponseWrapper visitPlanResponseWrapperInstance;
        List<sObject> sobjectList;
        SandDV1__Period__c periodInstance;
        List<Date> allDatesOfPeriod                                              =  new List<Date>();
        List<Integer> dateStringList                                             =  new List<Integer>();
        Map<Date, Boolean> dateValueMap                                          =  new Map<Date, Boolean>();
        List<VisitPlanningModel.CustomerWrapper> customerList                    =  new List<VisitPlanningModel.CustomerWrapper>();
        List<VisitPlanningModel.TotalVisitPlannedWrapper> totalVisitPlannedList  =  new List<VisitPlanningModel.TotalVisitPlannedWrapper>();
        List<VisitPlanningModel.VisitPlanWrapper> visitPlanList                  =  new List<VisitPlanningModel.VisitPlanWrapper>();
        Set<String> monthStringList                                              =  new Set<String>(); //set
        List<Integer> daysInMonthList                                            =  new List<Integer>();
        Map<Date, List<String>> holidaysMap                                      =  new Map<Date, list<String>>();
        Set<String> weekendHolidaySet                                            =  new Set<String>();
        List<VisitPlanningModel.VisitTargetWrapper> totalVisitTargetList         =  new List<VisitPlanningModel.VisitTargetWrapper>();
        Integer visitTargetVal                                                   =  0;
        
        if(periodId != Label.Select_Period) {
            
            periodInstance = new SandDV1__Period__c();
            periodInstance = VisitPlanningService.getActivePeriodInfo(periodId)[0];
            
            if(routeId != Label.Select_Route) {
                sobjectList = new List<SandDV1__Route_Account__c>();
                sobjectList = VisitPlanningService.getRouteAccountInfo(routeId, accountCategory); 
            } else { 
                sobjectList = new List<Account>();
                sobjectList = VisitPlanningService.getAccountInfo(accountCategory);
            }
            
            allDatesOfPeriod = VisitPlanningService.getDatesBetween(periodInstance.SandDV1__Start_Date__c, periodInstance.SandDV1__End_Date__c); //Calling Date method.

            try {
            visitTargetVal   = Integer.valueOf(ConfigurationService.fetchVisitPlanDefaultTarget('Default').values()[0].SandDV1__Number_Of_Visits__c);
            } catch(Exception e) {
                if(e.getMessage() == 'List index out of bounds: 0' || e.getMessage() == 'Attempt to de-reference a null object') {

                    SandDV1__Visit_Plan_Default_Target__c visitPlanCSInstance = new SandDV1__Visit_Plan_Default_Target__c();
                    visitPlanCSInstance.put('Name','Default');
                    visitPlanCSInstance.put('SandDV1__Number_Of_Visits__c',5);
                    insert visitPlanCSInstance;
                    visitTargetVal = Integer.valueOf(visitPlanCSInstance.get('SandDV1__Number_Of_Visits__c'));
                } else {
                    
                    throw new CustomException(String.format(Label.Custom_Exception_Msg, new List<String>{e.getStackTraceString(), String.valueOf(e.getLineNumber()), e.getMessage()}));
                    //('Exception from :'+ + e.getStackTraceString() +', in the line no: '+ e.getLineNumber() +' with the reason: '+ e.getMessage());
                }
            } 

            
            for(Holiday h : VisitPlanningService.getHolidays(periodInstance.SandDV1__Start_Date__c, periodInstance.SandDV1__End_Date__c)) {
                if(holidaysMap.containsKey(h.ActivityDate)) {
                    holidaysMap.get(h.ActivityDate).add(h.Name);
                } else {
                    holidaysMap.put(h.ActivityDate, new list<String>());
                    holidaysMap.get(h.ActivityDate).add(h.Name);
                }
            }
            
            BusinessHours businessHoursIns = VisitPlanningService.getWeekend();
            if(businessHoursIns != null) {
                if(businessHoursIns.SundayStartTime == null && businessHoursIns.SundayEndTime == null) WeekendHolidaySet.add(weekMap.get(1));
                if(businessHoursIns.MondayStartTime == null && businessHoursIns.MondayEndTime == null) WeekendHolidaySet.add(weekMap.get(2));
                if(businessHoursIns.TuesdayStartTime == null && businessHoursIns.TuesdayEndTime == null) WeekendHolidaySet.add(weekMap.get(3));
                if(businessHoursIns.WednesdayStartTime == null && businessHoursIns.WednesdayEndTime == null) WeekendHolidaySet.add(weekMap.get(4));
                if(businessHoursIns.ThursdayStartTime == null && businessHoursIns.ThursdayEndTime == null) WeekendHolidaySet.add(weekMap.get(5));
                if(businessHoursIns.FridayStartTime == null && businessHoursIns.FridayEndTime == null) WeekendHolidaySet.add(weekMap.get(6));
                if(businessHoursIns.SaturdayStartTime == null && businessHoursIns.SaturdayEndTime == null) WeekendHolidaySet.add(weekMap.get(7));
            }
            
            integer counter = 0;
            integer oldMonthVal;
            integer oldYearVal;
            integer newMonthVal;
            integer newYearVal;
            
            for(Date d : allDatesOfPeriod) {
                
                String datetoString = d.format(); 
                dateStringList.add(d.day());
                newMonthVal = d.Month();
                newYearVal  = d.year();
                
                Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
                String dayOfWeek = dt.format('EEEE');
                if(WeekendHolidaySet.contains(dayOfWeek)) {
                    if(holidaysMap.containsKey(d)) {
                        holidaysMap.get(d).add(dayOfWeek);
                    } else {
                        holidaysMap.put(d, new list<String>());
                        holidaysMap.get(d).add(dayOfWeek);
                    }
                }
                
                if(newMonthVal != oldMonthVal) {
                    if(counter != 0)  daysInMonthList.add(counter);
                    Counter = 1;
                    monthStringList.add(monthsMap.get(newMonthVal)+' - '+newYearVal);
                    oldMonthVal = newMonthVal;
                    oldYearVal  = newYearVal;
                } else {
                    counter++;
                }
                Boolean holidayFlag  = holidaysMap.containsKey(d) ? true : false;
                list<String> holidayReason = new list<String>();
                holidayReason = holidayFlag ? holidaysMap.get(d) : null;
                totalVisitPlannedList.add(new VisitPlanningModel.TotalVisitPlannedWrapper(counter, 0, holidayFlag));
                totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, visitTargetVal, holidayFlag));

                //if(!holidayFlag) {
                   
                //    totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, visitTargetVal, holidayFlag));
                //} else {

                //    totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, 0, holidayFlag));
                //}
                VisitPlanList.add(new VisitPlanningModel.VisitPlanWrapper(false, holidayFlag, holidayReason, datetoString,null)); 
            }
            daysInMonthList.add(counter);
            string sObjectTypeValue = string.valueOf(sobjectList.getSobjectType());
            
            for(sObject objInstance : sobjectList) {
                
                if(sObjectTypeValue == 'Account') {
                    Account acc = new Account();
                    acc = (Account)objInstance;
                    
                    VisitPlanningModel.CustomerWrapper customerInfo = new VisitPlanningModel.CustomerWrapper(acc.Name, acc.Id, acc.SandDV1__Account_Category__c, acc.SandDV1__Credit_Limit__c);
                    customerInfo.visitPlanWrapperList         =  VisitPlanList.clone();
                    customerList.add(customerInfo);
                    
                } else if(sObjectTypeValue == 'SandDV1__Route_Account__c') {
                    SandDV1__Route_Account__c routeAccount = new SandDV1__Route_Account__c();
                    routeAccount = (SandDV1__Route_Account__c)objInstance;
                    VisitPlanningModel.CustomerWrapper customerInfo = new VisitPlanningModel.CustomerWrapper(routeAccount.SandDV1__Account__r.Name, routeAccount.SandDV1__Account__c, routeAccount.SandDV1__Account__r.SandDV1__Account_Category__c, routeAccount.SandDV1__Account__r.SandDV1__Credit_Limit__c);
                    customerInfo.visitPlanWrapperList         =  VisitPlanList.clone();
                    customerList.add(customerInfo);
                } 
            }
        }
        
        visitPlanResponseWrapperInstance                                     =  new VisitPlanningModel.VisitPlanResponseWrapper();
        visitPlanResponseWrapperInstance.periodValues.tableName              =  'Days';
        visitPlanResponseWrapperInstance.periodValues.dayOfMonth             =  dateStringList;
        visitPlanResponseWrapperInstance.periodValues.monthWithYear          =  new List<String>(monthStringList);
        visitPlanResponseWrapperInstance.periodValues.numberOfDaysInMonth    =  daysInMonthList;
        visitPlanResponseWrapperInstance.totalVisitValues                    =  totalVisitPlannedList;
        visitPlanResponseWrapperInstance.visitTargetValues                   =  totalVisitTargetList;
        visitPlanResponseWrapperInstance.customerValues                      =  customerList;
        
        return visitPlanResponseWrapperInstance;
    }
    
    
    /*******************************************************************************************************
* @description: Save VisitPlan data based on the checked values.
* @param: visitPlanRes - JSON format of VisitPlan Data.
* @param: periodId  - Selected PeriodId.
* @param: Status - Selected Status.
* @return SandDV1__Visit_Plan__c - Inserted VisitPlanData. 
*/
    @remoteAction
    public static SandDV1__Visit_Plan__c saveVisitPlan(VisitPlanningModel.VisitPlanResponseWrapper visitPlanRes, String periodId, String status) {
        
        VisitPlanningModel.VisitPlanResponseWrapper res = visitPlanRes;
        //= (VisitPlanningModel.VisitPlanResponseWrapper) JSON.deserialize(visitPlanRes, VisitPlanningModel.VisitPlanResponseWrapper.class);
        
        
        Id currentUserId                                 = userinfo.getUserId();
        List<SandDV1__Visit__c> VisitList                         = new List<SandDV1__Visit__c>();
        
        SandDV1__Visit_Plan__c visitPlanInstance                  = new SandDV1__Visit_Plan__c();
        visitPlanInstance.SandDV1__Period__c                      = periodId;
        visitPlanInstance.SandDV1__User__c                        = currentUserId;
        visitPlanInstance.SandDV1__Status__c                      = 'Planning & Inprogress'; //(status == 'Planning' ? 'Planning & Inprogress' : status); 
        visitPlanInstance.SandDV1__Number_Of_Minimum_Visits__c    = res.visitTargetValues[0].checkedRowCount;
               

        Id userManagerId                = VisitPlanningService.getuserDetails(currentUserId).ManagerId;
        Id visitRecordTypeId            = VisitPlanningService.getSobjectRecordTypeInfo('SandDV1__Visit__c', 'Check In').getRecordTypeId();
        
        if(userManagerId == null && Status == 'Submitted') {

            throw new VisitPlanningException(Label.Manager_Not_Defined);
            }  else {
            
            List<Schema.SObjectField> VisitPlanSobjectFieldsList = FieldAccessibilityUtility.fetchVisitPlanFields2();
            SecurityUtils.checkInsert(SandDV1__Visit_Plan__c.SObjectType, VisitPlanSobjectFieldsList);
            insert visitPlanInstance;
            
            for(VisitPlanningModel.CustomerWrapper cusWrap : res.customerValues) {
                
                for(VisitPlanningModel.VisitPlanWrapper visitWrap : cusWrap.visitPlanWrapperList) {
                    
                    if(visitWrap.IsVisitPlanned) {
                        SandDV1__Visit__c visitInstance            =  new SandDV1__Visit__c();
                        visitInstance.put('SandDV1__Account__c', cusWrap.customerId);
                        visitInstance.put('SandDV1__Visit_Plan__c', visitPlanInstance.id);
                        visitInstance.put('SandDV1__Visit_Plan_Date__c', Date.parse(visitWrap.dateValue));
                        visitInstance.put('recordTypeId', visitRecordTypeId);
                        visitInstance.put('SandDV1__Type__c', 'Planned');
                        VisitList.add(visitInstance);
                    }
                }
            }
            
            List<Schema.SObjectField> VisitSobjectFieldsList = FieldAccessibilityUtility.fetchPlannedVisitFields2();
            SecurityUtils.checkInsert(SandDV1__Visit__c.SObjectType, VisitSobjectFieldsList);
            insert VisitList;
            
            if(status != 'Planning') {
                visitPlanInstance.SandDV1__Status__c     = status;
                
                SecurityUtils.checkUpdate(SandDV1__Visit_Plan__c.SObjectType, new List<String>{'SandDV1__Status__c'});
                update visitPlanInstance;
            }

        }
        return visitPlanInstance;
    }  
}