/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 25th August 2015 .
* @description This class is used for Sharing Service.
*/

public with sharing class RecordSharing {
    
    /*
    * @description :- method is used to fetch object type from object id.
    * @param:- Map<Id,Set<Id>>.
    * @return :- string. 
    */
    public static string fetchobjecttype(Id objectID) {
        Schema.SObjectType objType;
        String shareobjectType ;

            objType = objectID.getSobjectType();
            shareobjectType = String.valueof(objType);
            if(shareobjectType.contains('__c')) {
                shareobjectType.split('__c');
                String[] splitarray = (shareobjectType).split('__c');
                shareobjectType = splitarray[0]+'__Share';
            }
            else {
                shareobjectType = shareobjectType + 'Share';
            }
        return shareobjectType;
    }

    /*******************************************************************************************************
    * @description :- This Method is used to assign sharing rule.
    * @param :- List<SandDV1__System_Object_Bucket__c>,Map<Id,SandDV1__Bucket__c>,Boolean.
    * @return :-  void.
    */
    public static void assignSharing(List<SandDV1__System_Object_Bucket__c> objectBucketList,Map<Id,SandDV1__Bucket__c> bucketMap,
                                    Boolean IsObjectBucket) {
        List<sobject> sharingList = new List<sobject>();
        Map<String, Schema.SObjectType> tokens = Schema.getGlobalDescribe();
        for(SandDV1__System_Object_Bucket__c objbucket : objectBucketList) {
            String shareobjectType ;
            String parentID = '';
            String accesslevel = '';
            shareobjectType = fetchobjecttype(objbucket.SandDV1__Parent_Id__c);
            if(tokens.containsKey(shareobjectType)) {
                SObject testShare = tokens.get(shareobjectType).newSObject();
            if(!shareobjectType.contains('__Share') && shareobjectType.contains('Share')) {
                String[] splitobjectarray = (shareobjectType).split('Share');
                parentID = splitobjectarray[0]+'Id';
                accesslevel = splitobjectarray[0]+'AccessLevel';
            }
            else {
                parentID = 'ParentId';
                accesslevel = 'AccessLevel';
            }
            if(shareobjectType == 'AccountShare') {
                testShare.put('OpportunityAccessLevel',bucketMap.get(objbucket.SandDV1__Bucket__c).SandDV1__Access_Level__c);
            }
            testShare.put(parentID,objbucket.SandDV1__Parent_Id__c);
            testShare.put(accesslevel,bucketMap.get(objbucket.SandDV1__Bucket__c).SandDV1__Access_Level__c);
            //testShare.put('RowCause','BucketShare');
            testShare.put('UserOrGroupId',bucketMap.get(objbucket.SandDV1__Bucket__c).SandDV1__System_Group_ID__c);
            
            Schema.SObjectType objType = tokens.get(shareobjectType);
            List<Schema.SObjectField> shareObjectFieldsList = new List<Schema.SObjectField>();
            if(tokens.get(shareobjectType).getDescribe().fields.getMap().get('ParentId') <> null){
                shareObjectFieldsList.add(tokens.get(shareobjectType).getDescribe().fields.getMap().get('ParentId'));
            }
            if(tokens.get(shareobjectType).getDescribe().fields.getMap().get('AccessLevel') <> null){    
                shareObjectFieldsList.add(tokens.get(shareobjectType).getDescribe().fields.getMap().get('AccessLevel'));
            }
            if(tokens.get(shareobjectType).getDescribe().fields.getMap().get('UserOrGroupId') <> null){    
                shareObjectFieldsList.add(tokens.get(shareobjectType).getDescribe().fields.getMap().get('UserOrGroupId'));
            }
            
            SecurityUtils.checkInsert(objType, shareObjectFieldsList);
            sharingList.add(testShare);
            }
        }

        if(!sharingList.isEmpty()) {
            List<Database.SaveResult> srList = Database.insert(sharingList,false);
            objectBucketField(srList,objectBucketList,IsObjectBucket);
        }
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch share record id and put in System object bucket record..
    * @param :- List<Database.SaveResult>,List<SandDV1__System_Object_Bucket__c>,Boolean.
    * @return :- void. 
    */
    public static void objectBucketField(List<Database.SaveResult> srList,
                                        List<SandDV1__System_Object_Bucket__c> objectBucketList,Boolean IsObjectBucket) {
        Map<Integer,String> shareMap = new Map<Integer,String>();
        
        for (integer i=0; i<srList.size();i++) {
            if(srList[i].isSuccess()) {
                shareMap.put(i,srList[i].getId());
            }
            else {
                for(Database.Error err : srList[i].getErrors()) {                 
                    
                }
            }
        }
        for (integer i=0; i<objectBucketList.size();i++) {
            if(shareMap.containskey(i) && shareMap.get(i) != null && shareMap.get(i) != '') {
                objectBucketList[i].SandDV1__System_Record_Share_Id__c = shareMap.get(i);
            }
            else {
                objectBucketList[i].SandDV1__System_Record_Share_Id__c = '';
            }
        }
        if(IsObjectBucket == false) {
            if(!objectBucketList.isEmpty()) {
                List<Schema.SObjectField> systemObjectBucketFieldsList = FieldAccessibilityUtility.fetchSysObjBucket2();
                SecurityUtils.checkUpdate(SandDV1__System_Object_Bucket__c.SObjectType,systemObjectBucketFieldsList);
                update objectBucketList;
            }
        }
    }
    
    /*******************************************************************************************************
    * @description :- Method to delete Sharing Records on insert of deactivation date of SandDV1__System_Object_Bucket__c 
                       object and even on update of Accesslevel in SandDV1__Bucket__c object 
                       (Because Share Record cannot be updated only insert or delete).
    * @param :- List<SandDV1__System_Object_Bucket__c>.
    * @return :- void . 
    */
    public static void deleteShare(List<SandDV1__System_Object_Bucket__c> newObjects) {
        List<sobject> deleteList = new List<sobject>();
        Map<String, Schema.SObjectType> tokens = Schema.getGlobalDescribe();
        for(SandDV1__System_Object_Bucket__c objectBucket : newObjects) {
            Id objectParentID = objectBucket.SandDV1__Parent_Id__c;
            String shareobjectType = fetchobjecttype(objectParentID);
            if(objectBucket.SandDV1__System_Record_Share_Id__c != null && objectBucket.SandDV1__System_Record_Share_Id__c != '') {
                SObject so = tokens.get(shareobjectType).newSObject();
                so.Id = objectBucket.SandDV1__System_Record_Share_Id__c;
                objectBucket.SandDV1__System_Record_Share_Id__c = '';
                
                Schema.SObjectType objType = tokens.get(shareobjectType);
                SecurityUtils.checkObjectIsDeletable(objType);
                deleteList.add(so);
            }
        }
        if(!deleteList.isempty()) {
            delete deleteList;
        }
    }
}