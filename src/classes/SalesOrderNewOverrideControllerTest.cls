/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Test class of SalesOrderNewOverrideController
*/

@isTest(seeAllData = false)
private class SalesOrderNewOverrideControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Account testAccount;
    static SandDV1__FieldIdsforURLHack__c fieldHackCustom;
    static SandDV1__Sales_Order__c testSalesOrder;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        System.runAs(sysAdmin){
            
            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount;

            fieldHackCustom = new SandDV1__FieldIdsforURLHack__c();
            fieldHackCustom.Name = 'salesOrderBuyer';
            fieldHackCustom.SandDV1__Field_Id__c = 'CF00N28000005r6YZ_lkid';
            fieldHackCustom.SandDV1__Field_Name__c = 'SandDV1__Buyer__c';
            fieldHackCustom.SandDV1__Object_Name__c = 'SandDV1__Sales_Order__c';
            insert fieldHackCustom;

            testSalesOrder = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',null);
            System.assertEquals(fieldHackCustom.SandDV1__Object_Name__c,'SandDV1__Sales_Order__c');
        }
    }
    @isTest static void test_method_one() {
        
        init();

        System.runAs(testUser){
            Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(testSalesOrder);
            ApexPages.currentPage().getParameters().put('retURL','/'+testAccount.Id);
            ApexPages.currentPage().getParameters().put('CF00N28000005r6YZ_lkid',testAccount.Id);
            SalesOrderNewOverrideController trc = new SalesOrderNewOverrideController(sc); 

            Test.stopTest();  
        }
    }
    
}