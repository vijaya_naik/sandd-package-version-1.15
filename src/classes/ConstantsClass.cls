/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class ConstantsClass {


    public static final String TYPE_SUBPERIOD = 'Sub Period';
    public static final String TYPE_PERIOD = 'Period';
    public static final Integer TargetViewSize = 10;
    public static final Integer SchemeCriteriaValueSize = 10;
    public static Boolean Is_Bucket_Update = true;
    
    /*******************************************************************************************************
    * @description :- Method to store Aspect values
    * @param :- 
    * @return :- Map<String,String> 
    */
    public static Map<String,String> getAspectValues() {
        Map<String,String> aspectMap = new Map<String,String>();
        aspectMap.put('Number of Orders','Number of Orders');
        aspectMap.put('Quantity','Quantity');

        return aspectMap;

    }

    /*******************************************************************************************************
    * @description :- Method to store TargetStatus values
    * @param :- 
    * @return :- Map<String,String> 
    */
    public static Map<String,String> getTargetStatusValues() {
        Map<String,String> statusMap = new Map<String,String>();
           statusMap.put('isAssigned','Assigned');
           statusMap.put('isRevised','To be Revised');
           statusMap.put('isDraft','Draft');
           statusMap.put('isAccepted','Accepted');
           statusMap.put('isSuspended','Suspended');
        return statusMap;
    }


    /*******************************************************************************************************
    * @description : method to store Sales Order status values
    * @param : 
    * @return 
    */

    public static Map<String,String> getSalesOrderStatusValues() {
        Map<String,String> statusMap = new Map<String,String>();
           statusMap.put('isDraft','Draft');
           statusMap.put('isOpen','Open');
        return statusMap;
    }

    public static final Boolean AllowTargetAcceptance = true;

    public static final Boolean AllowAcceptedTargetsEdit = false;

    public static final Boolean AllowTargetCreation = true; //HierarchicalCustomMetadata.fetchToggleValue('ToggleSettings');

    public static final Boolean AllowSandBagging = false;

    public static final Boolean AllowLowerTargets = false;

    public static final Integer DigitLimit = 12;
}