/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
@isTest
global with sharing class BucketingTest {
    static User dummyuser;
    static User dummyuser1;
    static User dummychilduser;
    static Group dummyParentGroup;
    static Group dummyChildGroup;
    static SandDV1__Bucket__c bucket;
    static SandDV1__Bucket__c dummybucket;
    static SandDV1__Bucket__c dummyparentbucket;
    static SandDV1__User_Bucket_Member__c dummychildmember;
    static String sessionID;
    static List<SandDV1__User_Bucket_Member__c> userBucketList = new List<SandDV1__User_Bucket_Member__c>();
    static List<SandDV1__System_Object_Bucket__c> objectBucketList = new List<SandDV1__System_Object_Bucket__c>();

    static void init() {
        sessionID = UserInfo.getSessionId();
        dummyuser = InitializeTest.createUser('abtestsandd@gmail.com');
        insert dummyuser;
        dummyuser1 = InitializeTest.createUser('abtestsandd1@gmail.com');
        insert dummyuser1;
        dummychilduser = InitializeTest.createUser('chilssandd@gmail.com');
        insert dummychilduser;
        System.runAs(dummyuser)
        {
            dummyparentbucket = InitializeTest.createBucket(true,null,false);
            dummyparentbucket.Name = 'ParentBucket';
            insert dummyparentbucket;
            Id groupId = [Select Id,DeveloperName,Type from Group where DeveloperName =: dummyparentbucket.Id AND Type=:'Regular' limit 1].Id;
            dummyparentbucket.SandDV1__System_RootId__c = dummyparentbucket.Id;
            dummyparentbucket.SandDV1__System_Route_Path__c = '/'+dummyparentbucket.Id;
            dummyparentbucket.SandDV1__System_Level_Index__c = 0;
            dummyparentbucket.SandDV1__System_Group_ID__c = groupId;
            update dummyparentbucket;
            System.assertEquals(dummyparentbucket.SandDV1__System_Level_Index__c,0);
            
            SandDV1__User_Bucket_Member__c dummyparentmember = InitializeTest.createUserBucketMember(dummyuser.Id,dummyparentbucket.Id,true);
            userBucketList.add(dummyparentmember);
            System.assertEquals(dummyparentmember.SandDV1__User__c,dummyuser.Id);
            SandDV1__User_Bucket_Member__c dummyparentmember1 = InitializeTest.createUserBucketMember(dummyuser1.Id,dummyparentbucket.Id,true);
            userBucketList.add(dummyparentmember1);
            insert userBucketList;
        }
    }

    static testMethod void createChildBucketandMembersTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.runAs(dummyuser)
        {
            ConstantsClass.Is_Bucket_Update = true;
            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            update dummyparentbucket; 
            dummybucket = InitializeTest.createBucket(true,dummyparentbucket.Id,false);
            dummybucket.Name = 'ChildBucket';
            insert dummybucket;
            System.assertEquals(dummybucket.Name,'ChildBucket');
            
            Id groupChildId = [Select Id,DeveloperName,Type from Group where DeveloperName =: dummybucket.Id AND Type=:'Regular' limit 1].Id;
            dummybucket.SandDV1__System_RootId__c = dummyparentbucket.Id;
            //dummybucket.SandDV1__System_Group_ID__c = dummygroup.Id;
            dummybucket.SandDV1__System_Route_Path__c = dummyparentbucket.SandDV1__System_Route_Path__c +'/'+dummybucket.Id;
            dummybucket.SandDV1__System_Level_Index__c = 1;
            dummybucket.SandDV1__System_Group_ID__c = groupChildId;
            update dummybucket;
            dummychildmember = InitializeTest.createUserBucketMember(dummychilduser.Id,dummybucket.Id,true);
            dummybucket.SandDV1__Parent__c = null;
            dummybucket.SandDV1__System_RootId__c = dummybucket.Id;
            dummybucket.SandDV1__System_Group_ID__c = groupChildId;
            dummybucket.SandDV1__System_Route_Path__c = '/'+dummybucket.Id;
            dummybucket.SandDV1__System_Level_Index__c = 0;
            update dummybucket;
        }
    }

    static testMethod void createAndUpdateBucketTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.runAs(dummyuser)
        {
            ConstantsClass.Is_Bucket_Update = true;
            dummybucket = InitializeTest.createBucket(true,null,false);
            dummybucket.Name = 'newParentBucket';
            insert dummybucket;
            Id groupdummybucketId = [Select Id,DeveloperName,Type from Group where DeveloperName =: dummybucket.Id AND Type=:'Regular' limit 1].Id;
            dummybucket.SandDV1__System_RootId__c = dummybucket.Id;
            //dummybucket.SandDV1__System_Group_ID__c = dummygroup.Id;
            dummybucket.SandDV1__System_Route_Path__c = '/'+dummybucket.Id;
            dummybucket.SandDV1__System_Level_Index__c = 0;
            dummybucket.SandDV1__System_Group_ID__c = groupdummybucketId;
            update dummybucket;
            dummychildmember = InitializeTest.createUserBucketMember(dummychilduser.Id,dummybucket.Id,true);
            //insert dummychildmember;
            dummybucket.SandDV1__Parent__c = dummyparentbucket.Id;
            dummybucket.SandDV1__System_RootId__c = dummyparentbucket.SandDV1__System_RootId__c;
            dummybucket.SandDV1__System_Group_ID__c = groupdummybucketId;
            dummybucket.SandDV1__System_Route_Path__c = dummyparentbucket.SandDV1__System_Route_Path__c +'/'+dummybucket.Id;
            dummybucket.SandDV1__System_Level_Index__c = 1;
            update dummybucket;
            System.assertEquals(dummybucket.SandDV1__System_Level_Index__c,1);
        }
    }

    static testMethod void deActicvateMemberWhenHierarchyLockedTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            system.debug('dummyparentbucket......'+dummyparentbucket);
            try {
            for(integer i=0; i<userBucketList.size();i++){
                if(i==0){
                    userBucketList[i].SandDV1__System_Active__c = false;
                }
            }
            update userBucketList;
            }
            catch(Exception e) {
                System.debug('e......'+e);
            }
        }
    }

    static testMethod void deActicvateMemberWhenHierarchyUnLockedTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            update dummyparentbucket;
            for(integer i=0; i<userBucketList.size();i++){
                if(i==0){
                    userBucketList[i].SandDV1__System_Active__c = false;
                }
            }
            update userBucketList;
            //dummyparentmember.SandDV1__System_Active__c = false;
            //update dummyparentmember;
        }
    }

    static testMethod void deleteUserMemberTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            try {
            List<SandDV1__User_Bucket_Member__c> deleteList = new List<SandDV1__User_Bucket_Member__c>();
            for(integer i=0; i<userBucketList.size();i++){
            if(i==0){
                deleteList.add(userBucketList[i]);
            }
            }
                delete deleteList;
            }
            Catch(Exception e) {
              System.debug('Error:'+e);
            }
        
        }
    }
    static testMethod void deleteBucketTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            try {
                delete dummyparentbucket;
            }
            Catch(Exception e) {
              System.debug('Error:'+e);
            }
        
        }
    }

    static testMethod void ExceptionTest(){
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            try {
                //dummyparentbucket.SandDV1__System_Future_Call__c = false;
                dummyparentbucket.SandDV1__System_Group_ID__c = null;
                dummyparentbucket.SandDV1__System_RootId__c =dummyuser.Id;
                dummyparentbucket.SandDV1__System_Route_Path__c = 'AAAAAAAAAAAAAAAAAA';
                dummyparentbucket.SandDV1__System_Level_Index__c = null ;
                //update dummyparentbucket;
                System.assertEquals(dummyparentbucket.SandDV1__System_RootId__c,dummyuser.Id);
                
                SandDV1__User_Bucket_Member__c dummymember = InitializeTest.createUserBucketMember(dummyuser.Id,dummyparentbucket.Id,true);
                //insert dummymember;
            }
            catch(Exception e) {
                System.debug('e...'+e);
            }
        }
    }

    static testMethod void BucketDeActivationControllerTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {
            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            update dummyparentbucket;
            ConstantsClass.Is_Bucket_Update = true;
            dummybucket = InitializeTest.createBucket(true,dummyparentbucket.Id,false);
            dummybucket.Name = 'ChildBucket';
            insert dummybucket;
            Id groupChildId = [Select Id,DeveloperName,Type from Group where DeveloperName =: dummybucket.Id AND Type=:'Regular' limit 1].Id;

            dummybucket.SandDV1__System_RootId__c = dummyparentbucket.Id;
            //dummybucket.SandDV1__System_Group_ID__c = dummygroup.Id;
            dummybucket.SandDV1__System_Route_Path__c = dummyparentbucket.SandDV1__System_Route_Path__c +'/'+dummybucket.Id;
            dummybucket.SandDV1__System_Level_Index__c = 1;
            dummybucket.SandDV1__System_Group_ID__c = groupChildId;
            update dummybucket;
            System.assertEquals(dummybucket.SandDV1__System_Group_ID__c,groupChildId);
            
            ApexPages.CurrentPage().getparameters().put('Id',dummyparentbucket.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(dummyparentbucket);
            BucketDeActivationController e = new BucketDeActivationController(sc);
            BucketDeActivationController.deActivationCall(e.bucketId);
    
            
        }
    }

    static testMethod void createObjectBucketTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {   
            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            update dummyparentbucket;
            Account acc = InitializeTest.createAccount();
            insert acc;
            SandDV1__System_Object_Bucket__c objectbucket = InitializeTest.createObjectBucket('test1',acc.Id,dummyparentbucket.Id);
            objectbucket.SandDV1__Active_From__c = system.now();
            insert objectbucket;
            Id shareId = [Select Id,AccountId,UserorGroupId from AccountShare where AccountId =:acc.Id
                   AND UserorGroupId =:dummyparentbucket.SandDV1__System_Group_ID__c].Id;
            
            objectbucket.SandDV1__Deactivated_On__c = system.now();
            objectbucket.SandDV1__System_Record_Share_Id__c = shareId;
            update objectbucket;
            System.assertEquals(objectbucket.SandDV1__System_Record_Share_Id__c,shareId);
        }
    }

    static testMethod void createObjectBucketTest1() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {   
            List<SandDV1__System_Object_Bucket__c> objectList = new List<SandDV1__System_Object_Bucket__c>();
            List<SandDV1__System_Object_Bucket__c> objectList1 = new List<SandDV1__System_Object_Bucket__c>();

            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            dummyparentbucket.SandDV1__Access_Level__c = 'Read';
            update dummyparentbucket;
            SandDV1__period__c period = InitializeTest.createPeriod(true);
            insert period;
            SandDV1__Target__c target= InitializeTest.createTarget(period.Id,true,Label.Draft,dummyuser.Id);
            insert target;
            SandDV1__System_Object_Bucket__c objectbucket = InitializeTest.createObjectBucket('test1',target.Id,dummyparentbucket.Id);
            objectbucket.SandDV1__Active_From__c = system.now();
             objectBucketList.add(objectbucket);
            SandDV1__System_Object_Bucket__c objectbucket1 = InitializeTest.createObjectBucket('test2',period.Id,dummyparentbucket.Id);
            objectbucket1.SandDV1__Active_From__c = system.now();
            objectBucketList.add(objectbucket1);
            try {
            insert objectBucketList;
            }
            catch(Exception e) {
                System.debug('e.........'+e);
            }

            for(integer i= 0;i<objectBucketList.size();i++) {
                if(i==1) {
                objectBucketList[i].SandDV1__Deactivated_On__c = system.now();
                objectBucketList[i].SandDV1__System_Record_Share_Id__c = '';
                objectList.add(objectBucketList[i]);
                }
                if(i==0) {
                objectBucketList[i].SandDV1__Parent_Id__c = '';
                objectBucketList[i].SandDV1__Deactivated_On__c = system.now();
                objectList1.add(objectBucketList[i]);
                }
            }
            
            try {
                update objectList;
                update objectList1;
            }
            catch(Exception e) {
            }
            dummyparentbucket.SandDV1__Access_Level__c = 'Edit';
            update dummyparentbucket;
        }
    }

    static testMethod void createObjectBucketTest2() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {   try {
            dummyparentbucket.SandDV1__System_Future_Call__c = false;
            update dummyparentbucket;
            System.assertEquals(dummyparentbucket.SandDV1__System_Future_Call__c,false);
            SandDV1__System_Object_Bucket__c objectbucket = InitializeTest.createObjectBucket('test1','',dummyparentbucket.Id);
            objectbucket.SandDV1__Active_From__c = system.now();
            insert objectbucket;
            }
            catch(Exception e) {

            }
        }
    }

   
    global with sharing class Bucket_Test implements HttpCalloutMock {
        global httpResponse respond(HTTPrequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;
        }
    }

    static testMethod void processBuilderBucketAssignor() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        System.runAs(dummyuser)
        {   Account accObj = InitializeTest.createAccount();
            accObj.NumberOfEmployees = 400;
            insert accObj;
            RecordToBucketAssignor.BucketAssignmentRequest rec = new RecordToBucketAssignor.BucketAssignmentRequest();
            rec.recordId = accObj.Id;
            rec.uniqueBucketId = dummyparentbucket.Id;
            rec.customParameterList = null;
            System.assertEquals(rec.uniqueBucketId,dummyparentbucket.Id);
        }
    }

    static testMethod void BucketViewControllerTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        System.assert(dummyuser <> null);
        Test.startTest();
        BucketViewController controller = new BucketViewController();
        BucketViewController.fetchParentBuckets();
        BucketViewController.getTree(dummyparentbucket.Id);
        Test.stopTest();
    }

    /*static testMethod void BucketMemberDeactivationControllerTest() {
        Test.setMock(HttpcalloutMock.class, new Bucket_Test());
        init();
        Test.startTest();
        dummyparentbucket.SandDV1__System_Future_Call__c = false;
        update dummyparentbucket;
        dummychildmember = InitializeTest.createUserBucketMember(dummychilduser.Id,dummyparentbucket.Id,true);
        insert dummychildmember;
        ApexPages.StandardController sc = new ApexPages.standardController(dummychildmember);
        BucketMemberDeactivationController controller = new BucketMemberDeactivationController(sc);
        //BucketMemberDeactivationController.deactivateBucketMember(controller.bucketMemberId);
        Test.stopTest();
    }*/
    
    
}