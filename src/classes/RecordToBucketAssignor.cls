/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date Jul 2015
* @description This class if for this
*/

global with sharing class RecordToBucketAssignor extends ProcessClass{
    /*******************************************************************************************************
    * @description :- Method is used to assign record to bucket. It will create system object bucket record.
    * @param :- List<BucketAssignmentRequest> requests.
    * @return :- void.
    */
    
    @InvocableMethod(label='RecordsToBucketAssignor' description='Assign a record to a Bucket')
    global static void assignRecordsToBucket(List<BucketAssignmentRequest> requests) {
        
        List<SObject> sObjectList = new List<SObject>();
        //sObjectList = ProcessClass.getFilteredRecords(generateProcessInfoList(requests), 'AccountToBucketAssignor');
        sObjectList = ProcessClass.getFilteredRecords(requests, 'BucketAssignmentFilterable');
        //Call the Method to create the object Bucket records for Accounts.
        BucketService.createObjectBucketRecords(requests, sObjectList);
    }

    /**
    * @author ET Marlabs
    * @date 18 Aug 2015
    * @description This class if for this
    */
    
    global class BucketAssignmentRequest implements ProcessRequestBase {
        @InvocableVariable(label='Record Id' description='Id of Account' required=true)
        global Id recordId;

        @InvocableVariable(label='Bucket Id' description='Unique Id of the Buket to which the Accounts to be Assigned' required=true)
        global String uniqueBucketId;

        @InvocableVariable(label='Custom Parameter List' description='String of custom data that can be passed for the Custom filter methods' required=false)
        global String customParameterList;
        
        global Id getRecordId() {
            return recordId;        
        }
        
        global String getCustomParamList() {
            return customParameterList;
        }
    }
   
}