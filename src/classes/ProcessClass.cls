/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author ET Marlabs
* @date 18 Aug 2015
* @description This class to be extended for Processes by the Product Dev Team
*/

public virtual with sharing class ProcessClass {
    public ProcessClass() {
        
    }

    private static List<Filterable> filterInstancesList = new List<Filterable>();
    
    /*******************************************************************************************************
    * @description .The method to apply custom defined filters on the records and returns the  
                    list of filtered records.
    * @param:- List<ProcessRequestBase> requestList, String processClassName.
    * @return List<SObject>. 
    */
    
    public static List<SObject> getFilteredRecords(List<ProcessRequestBase> requestList, String processClassName) {
        
        List<SObject> customFilteredRecordsList = new List<SObject>();
        Set<SObject> customFilteredRecordsSet = new Set<SObject>();
        filterInstancesList = ConfigurationService.fetchFilters(processClassName);
        
        
        if(filterInstancesList.size() == 0) {
            for(ProcessRequestBase request : requestList) {
                
                
                Type t = Type.forName(request.getRecordId().getsobjecttype().getDescribe().getName());
                SObject sObj = (SObject)t.newInstance();
                sObj.put('Id', request.getRecordId());
                customFilteredRecordsList.add(sObj);
            }   
        }
        else {
            for(Filterable filterInstance : filterInstancesList) {
                customFilteredRecordsSet.addAll(filterInstance.filter(requestList));
            }
            customFilteredRecordsList.addAll(customFilteredRecordsSet); 
        }
        
      
        return customFilteredRecordsList;
    }
}