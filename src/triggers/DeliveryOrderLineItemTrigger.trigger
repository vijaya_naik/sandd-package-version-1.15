trigger DeliveryOrderLineItemTrigger on Delivery_Order_Line_Item__c (before insert,before update,after insert,after update,before delete) {
    
    //Initilization 
    DeliveryTriggerHandler deliveryOrderHelper = DeliveryTriggerHandler.getInstance();


    // Before Insert/Update
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore) {
        deliveryOrderHelper.onBeforeInsertUpdate(Trigger.new);
    }
    
    
    // After Insert
    if(Trigger.isInsert && Trigger.isAfter) {
        deliveryOrderHelper.onAfterInsert(Trigger.new);
    }
    
    // After Update
    if(Trigger.isUpdate && Trigger.isAfter) {
        deliveryOrderHelper.onAfterUpdate(Trigger.new,Trigger.oldMap);
    }
    
    // Before Delete
    if(Trigger.isDelete && Trigger.isBefore) {
        deliveryOrderHelper.onBeforeDelete(Trigger.old,true);
    }
    

}