/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Trigger on Account.
*/

trigger AccountTrigger on Account (before insert,before update,after insert) {

    /*--Instantiate the helper--*/
    AccountTriggerHelper helper = AccountTriggerHelper.getInstance();
    OnBoardingAccountTriggerHelper boardinghelper = OnBoardingAccountTriggerHelper.getInstance();
    //Check for the events
    if(Trigger.isBefore && Trigger.isInsert){

        helper.onBeforeInsert(Trigger.new); 
        //boardinghelper.onBeforeInsert(Trigger.new,); 
    }
    if(Trigger.isBefore && Trigger.isUpdate){

        boardinghelper.onBeforeInsert(Trigger.new,Trigger.oldMap); 
    }
    if(Trigger.isAfter && Trigger.isInsert){

        boardinghelper.onBeforeInsert(Trigger.new,null); 
    }
}