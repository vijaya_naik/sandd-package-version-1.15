/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs)
* @date 8/12/2015
* @description This trigger 
*/
trigger UserBucketTrigger on User_Bucket_Member__c (After insert,before insert,before update,before delete) {

    List<User_Bucket_Member__c> userBucketList = new List<User_Bucket_Member__c>();

    if(!Trigger.isDelete){
        /*checkHierarchyBlock method is used to check bucket hierarchy is locked or not.If locked then show message
          otherwise insert or update user bucket member records.*/
        userBucketList = UserBucketHandler.checkHierarchyBlock(trigger.new);
    }

    if(Trigger.isInsert && Trigger.isAfter) {
        if(!userBucketList.isempty()) {
            UserBucketHandler.onInsertUpdate(userBucketList,'Insert');
        }
    }

    if(Trigger.isInsert && Trigger.isBefore) {
        if(!userBucketList.isempty()) {
            UserBucketHandler.updateuserBucketField(userBucketList,'Insert');
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
        if(!userBucketList.isempty()) {
            UserBucketHandler.onInsertUpdate(userBucketList,'Update');
        }
    }

    if(Trigger.isDelete && Trigger.isBefore) {
        UserBucketHandler.onDelete(trigger.old);
    }
  
}