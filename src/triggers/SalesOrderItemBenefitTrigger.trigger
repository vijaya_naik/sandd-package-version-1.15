trigger SalesOrderItemBenefitTrigger on Sales_Order_Item_Benefit__c (before insert,after delete,after insert, before update, before delete) {
    if(Trigger.isAfter && Trigger.isInsert){
        SalesOrderItemBenefitTriggerHandler.onAfterInsert(Trigger.new);
    }


    if(Trigger.isBefore && Trigger.isDelete){
        SalesOrderItemBenefitTriggerHandler.onBeforeDelete(Trigger.old);
    }
    if(Trigger.isAfter && Trigger.isDelete){
        SalesOrderItemBenefitTriggerHandler.onAfterDelete(Trigger.old);
    }



    if(Trigger.isBefore && Trigger.isInsert){
        SalesOrderItemBenefitTriggerHandler.onBeforeInsert(Trigger.new);
    }


    if(Trigger.isBefore && Trigger.isUpdate){
        SalesOrderItemBenefitTriggerHandler.onBeforeUpdate(Trigger.new);
    }
}