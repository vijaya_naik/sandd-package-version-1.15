/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
trigger DeliveryOrderTrigger on Delivery_Order__c (before delete,before insert,after update) {

    //Initilization 
    DeliveryTriggerHandler deliveryOrderHelper = DeliveryTriggerHandler.getInstance();

    // Before Delete
    if(Trigger.isDelete && Trigger.isBefore) {
        deliveryOrderHelper.onDeleteofDeliveryOrder(Trigger.old);
    }

    //Before Insert 
    if(Trigger.isInsert && Trigger.isBefore) {
        deliveryOrderHelper.onBeforeInsert(Trigger.new);
    }

    //After Update 
    if(Trigger.isUpdate && Trigger.isAfter) {
        deliveryOrderHelper.onUpdateDeliveryOrder(Trigger.newMap,Trigger.oldMap);
    }
}