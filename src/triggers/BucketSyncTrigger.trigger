/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/18/2015
* @description This trigger 
*/

trigger BucketSyncTrigger on Bucket__c (before insert,after insert,after update,after delete) {
    
    // Before Insert
    if(Trigger.isInsert && Trigger.isBefore) {
        BucketTriggerHandler.onBeforeInsert(Trigger.new);
    }
    
    // After Insert
    if(Trigger.isInsert && Trigger.isAfter && ConstantsClass.Is_Bucket_Update  == true) {
        BucketTriggerHandler.OnAfterInsert(Trigger.new);
    }
    
    // Before Update
    if(Trigger.isUpdate && Trigger.isAfter) {
        BucketTriggerHandler.onUpdate(Trigger.NewMap,Trigger.OldMap,Trigger.new);
    }
    // After Delete
    if(Trigger.isDelete && Trigger.isAfter) {
        BucketTriggerHandler.onDelete(Trigger.old);
    }
}