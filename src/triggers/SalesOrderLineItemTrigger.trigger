/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
trigger SalesOrderLineItemTrigger on Sales_Order_Line_Item__c (after Update,before Delete,before Update) {
  
    //Initilization 
    SalesorderLineItemTriggerHandler salesOrderLineItemHelper = SalesorderLineItemTriggerHandler.getInstance();
    
    // Before Update
    if(Trigger.isUpdate && Trigger.isBefore) {
        salesOrderLineItemHelper.onBeforeUpdate(Trigger.new,Trigger.oldMap);
    }
    
    // After Update
    if(Trigger.isUpdate && Trigger.isAfter) {
        salesOrderLineItemHelper.onAfterUpdate(Trigger.new,Trigger.oldMap);
    }

    // Before Delete
    if(Trigger.isDelete && Trigger.isBefore) {
        salesOrderLineItemHelper.onDelete(Trigger.old);
    }
}