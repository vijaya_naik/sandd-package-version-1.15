<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>No. of Order Line Items</label>
    <values>
        <field>ActiveAPI__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ActiveApiValue__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CriteriaValue__c</field>
        <value xsi:type="xsd:string">No. of Order Line Items</value>
    </values>
    <values>
        <field>FieldApi__c</field>
        <value xsi:type="xsd:string">SandDV1__System_No_of_Sales_Order__c</value>
    </values>
    <values>
        <field>IsAssignment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsPickList__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>ObjectApi__c</field>
        <value xsi:type="xsd:string">SandDV1__Sales_Order__c</value>
    </values>
    <values>
        <field>ReturnType__c</field>
        <value xsi:type="xsd:string">Integer</value>
    </values>
    <values>
        <field>SchemeType__c</field>
        <value xsi:type="xsd:string">Number</value>
    </values>
</CustomMetadata>
