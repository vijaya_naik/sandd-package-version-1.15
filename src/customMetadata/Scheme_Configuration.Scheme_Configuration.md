<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Scheme Configuration</label>
    <values>
        <field>Scheme_Active_Status__c</field>
        <value xsi:type="xsd:string">Active</value>
    </values>
    <values>
        <field>Scheme_Assignment_Type__c</field>
        <value xsi:type="xsd:string">Assignment</value>
    </values>
    <values>
        <field>Scheme_Criteria_Type__c</field>
        <value xsi:type="xsd:string">Criteria</value>
    </values>
    <values>
        <field>Scheme_InActive_Status__c</field>
        <value xsi:type="xsd:string">In-Active</value>
    </values>
    <values>
        <field>Scheme_Type_Amount__c</field>
        <value xsi:type="xsd:string">Amount</value>
    </values>
    <values>
        <field>Scheme_Type_FOC__c</field>
        <value xsi:type="xsd:string">FOC</value>
    </values>
    <values>
        <field>Scheme_Type_Percentage__c</field>
        <value xsi:type="xsd:string">Percentage</value>
    </values>
</CustomMetadata>
