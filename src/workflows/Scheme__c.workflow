<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Scheme_is_going_to_Expire</fullName>
        <description>Scheme is going to Expire.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Scheme_Folder/Scheme_is_going_to_expire</template>
    </alerts>
    <fieldUpdates>
        <fullName>Scheme_InActive</fullName>
        <field>Status__c</field>
        <literalValue>In-Active</literalValue>
        <name>Scheme InActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Scheme is going to deactivate</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Scheme__c.Expires_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Scheme__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Scheme_is_going_to_Expire</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Scheme__c.Expires_On__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Scheme_InActive</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Scheme__c.Expires_On__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
