<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_Mail_For_Change_in_Accepted_Target</fullName>
        <description>Notification Mail For Change in Accepted Target</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Target_Setting/Target_Changes_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_Mail_For_Re_Assignment_of_Target_when_submitted_For_Revision</fullName>
        <description>Notification Mail For Re-Assignment of Target when submitted For Revision</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Target_Setting/Target_Notification_After_Reassignment_as_part_of_Target_Revision</template>
    </alerts>
    <alerts>
        <fullName>Notification_Mail_For_Target_Acceptance</fullName>
        <description>Notification Mail For Target Acceptance</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Target_Setting/Target_Acceptance_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_Mail_For_Target_Assigned</fullName>
        <description>Notification Mail For Target Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Target_Setting/Target_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_Mail_for_Target_Revision</fullName>
        <description>Notification Mail for Target Revision</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Target_Setting/Target_Revision_Notification</template>
    </alerts>
</Workflow>
