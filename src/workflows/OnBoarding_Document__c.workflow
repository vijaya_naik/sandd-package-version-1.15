<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_OnBoardingDocument_Status</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update OnBoardingDocument Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OnBoardingDocument_StatusReject</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update OnBoardingDocument StatusReject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OnBoardingDocument_StatusSubmitte</fullName>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Update OnBoardingDocument StatusSubmitte</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
