<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Discount</fullName>
        <field>Discount__c</field>
        <formula>0</formula>
        <name>Update Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Price Book</fullName>
        <actions>
            <name>Update_Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Type__c = &apos;FOC&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
