<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_System_Start_and_End_Date_Field</fullName>
        <description>Concatenates start date and end date in a single field in &apos;Start Date - End Date&apos; format for display purposes on custom pages though tool tips etc.</description>
        <field>System_Start_and_End_Date__c</field>
        <formula>Text(DAY(Start_Date__c))&amp;&apos;/&apos;&amp; Text(MONTH(Start_Date__c))&amp;&apos;/&apos;&amp; Text(YEAR(Start_Date__c))&amp; &apos; - &apos; &amp;Text(DAY(End_Date__c))&amp;&apos;/&apos;&amp; Text(MONTH(End_Date__c))&amp;&apos;/&apos;&amp; Text(YEAR(End_Date__c))</formula>
        <name>Update System Start and End Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Concatenate Period Start and End Date</fullName>
        <actions>
            <name>Update_System_Start_and_End_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The rule to fire actions whenever a Period record is created or updated.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
