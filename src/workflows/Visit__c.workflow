<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_RT_to_Check_Out</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Check_Out</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RT to Check Out.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RT_to_Visited</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Visited</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RT to Visited</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Record Type with Check Out</fullName>
        <actions>
            <name>Update_RT_to_Check_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Visit__c.Checkin_Location__Latitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.Checkin_Location__Longitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Check In</value>
        </criteriaItems>
        <description>Populate Record Type field with Check Out</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Record Type with Check Out%2E</fullName>
        <actions>
            <name>Update_RT_to_Check_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Visit__c.Checkin_Location__Latitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.Checkin_Location__Longitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Check In</value>
        </criteriaItems>
        <description>Populate Record type field with Check Out.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Record Type with Visited</fullName>
        <actions>
            <name>Update_RT_to_Visited</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Visit__c.Checkout_Location__Latitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.Checkout_Location__Longitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Check Out</value>
        </criteriaItems>
        <description>Populate Record Type field with Visited</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Record Type with Visited%2E</fullName>
        <actions>
            <name>Update_RT_to_Visited</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Visit__c.Checkout_Location__Latitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.Checkout_Location__Longitude__s</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Visit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Check Out</value>
        </criteriaItems>
        <description>Populate Record Type with Visited.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
