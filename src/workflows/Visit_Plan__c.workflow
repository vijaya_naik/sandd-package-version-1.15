<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approve_Tour_Plan</fullName>
        <description>Tour Plan Approved.</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approve Tour Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_Tour_Plan</fullName>
        <description>Tour Plan Rejected.</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Tour Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Visit_Plan_Status_to_Submitted</fullName>
        <description>Once the user submits for approval, change the Visit Plan status to Submitted.</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Visit Plan Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
